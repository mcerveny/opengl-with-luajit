--[[ simple_texture.lua

 The quickest and easiest scene that:
  - draws something 
  - plays sounds
  - takes input
]]

simple_texture = {}
simple_texture.__index = simple_texture

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function simple_texture.new(...)
    local self = setmetatable({}, simple_texture)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_texture:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_texture:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
end

vs=[[
#version 100
attribute vec2 vP;
varying vec3 vfC;
void main()
{
    vfC=vec3(vP,0.);
    gl_Position=vec4(vP,0.,1.);
}
]]

fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform sampler2D tex;

void main()
{
    vec2 tc = vec2(.5)+.5*vfC.xy;
    vec3 col = texture2D(tex, tc).xyz;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_texture:loadTextures(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
    --gl.glTexParameteri( GL.GL_TEXTURE_2D, GL.GL_DEPTH_TEXTURE_MODE, GL.GL_INTENSITY ); --deprecated, out in 3.1
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end

function simple_texture:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    v = ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
    vl = gl.glGetAttribLocation(self.prog,"vP")
    vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
    gl.glBindVertexArray(0)

    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory(self.dataDir.."/fonts")
    glfont:initGL()

    self.texID = 0
    self:loadTextures('expl_spritesheet1.data', 900, 900)
end

function simple_texture:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)
end

function simple_texture:render()
    gl.glUseProgram(self.prog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "tex")
    gl.glUniform1i(stex_loc, 0)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    glfont:render_string(m, p, col, "simple_texture")
end

function simple_texture:timestep(absTime, dt)
    self.t = absTime
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function simple_texture:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then
            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end
        end
    end
end

function simple_texture:on_button_pressed(i)
    self.t_last = self.t
    if i == lr_input.JOYPAD_A then
        snd.playSound("pop_drip.wav")
    elseif i == lr_input.JOYPAD_B then
        snd.playSound("Jump5.wav")
    elseif i == lr_input.JOYPAD_X then
        snd.playSound("Pickup_Coin7.wav")
    elseif i == lr_input.JOYPAD_Y then
        snd.playSound("test.wav")
    end
end

return simple_texture
