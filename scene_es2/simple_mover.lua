--[[ simple_mover.lua

    Takes controller input and moves a square around the viewport.
]]

simple_mover = {}
simple_mover.__index = simple_mover

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function simple_mover.new(...)
    local self = setmetatable({}, simple_mover)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_mover:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_mover:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0

    self.movers = {}
end

vs=[[
#version 100
uniform vec2 uOffset;
attribute vec2 vP;
varying vec3 vfC;
void main()
{
    vfC=vec3(vP,0.);
    gl_Position=vec4(vP+uOffset,0.,1.);
}
]]

fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_mover:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local s = .2
    v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    vl = gl.glGetAttribLocation(self.prog,"vP")
    vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)

    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory(self.dataDir.."/fonts")
    glfont:initGL()
end

function simple_mover:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_mover:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    for i=1,#self.movers do
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
        local off = self.movers[i][1]
        gl.glUniform2f(uoff_loc, off[1], off[2])

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end


    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    glfont:render_string(m, p, col, "simple_mover "..tostring(#self.movers))
end

function simple_mover:timestep(absTime, dt)
    self.t = absTime
    for j=1,#self.movers do
        local vel = self.movers[j][2]
        for i=1,2 do
            self.movers[j][1][i] = math.min(math.max(-1,self.movers[j][1][i] + vel[i]),1)
        end
    end
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function simple_mover:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then

            -- Create mover if not already there
            if not self.movers[j] then
                local offset = {0,0}
                local vel = {0,0}
                self.movers[j] = {offset,vel}
            end

            local v = .01
            local vel = self.movers[j][2]
            vel[1] = 0
            vel[2] = 0

            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end

            if js[lr_input.JOYPAD_LEFT] ~= 0 then
                vel[1] = -v
            elseif js[lr_input.JOYPAD_RIGHT] ~= 0 then
                vel[1] = v
            end
            if js[lr_input.JOYPAD_UP] ~= 0 then
                vel[2] = v
            elseif js[lr_input.JOYPAD_DOWN] ~= 0 then
                vel[2] = -v
            end
        end
    end
end

function simple_mover:on_button_pressed(i)
    if i == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
    elseif i == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
    end
end

return simple_mover
