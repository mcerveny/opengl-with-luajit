--[[ simple_quads.lua

    Draws a bunch of rotating quads.
]]

simple_quads = {}
simple_quads.__index = simple_quads

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function simple_quads.new(...)
    local self = setmetatable({}, simple_quads)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_quads:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_quads:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 16*16*4
    self.offsets = {}
    for i=1,self.n_quads do
        local off = {-1+2*math.random(), -1+2*math.random()}
        table.insert(self.offsets,off)
    end
end

vs=[[
#version 100
uniform float uTime;
uniform vec2 uOffset;
attribute vec2 vP;
varying vec3 vfC;

void main()
{
    float rot = uTime;
    mat2 m = mat2(cos(rot), -sin(rot), sin(rot), cos(rot));
    vfC = vec3(vP,0.);
    vec2 pt = m * vP;
    gl_Position = vec4(pt+uOffset,0.,1.);
}
]]

fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_quads:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local s = .05
    v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    vl = gl.glGetAttribLocation(self.prog,"vP")
    vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)

    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory(self.dataDir.."/fonts")
    glfont:initGL()
end

function simple_quads:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_quads:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local ut_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(ut_loc, self.t)

    -- TODO: pseudo-instancing
    for i=1,self.n_quads do
        local off = self.offsets[i]
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
        gl.glUniform2f(uoff_loc, off[1], off[2])

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    glfont:render_string(m, p, col, "simple_quads "..self.n_quads)
    mm.glh_translate(m, 30, 30, 0)
end

function simple_quads:timestep(absTime, dt)
    self.t = absTime
end

local last_state = {}
for i=1,14 do last_state[i]=0 end
function simple_quads:update_retropad(js)
    --TODO: pass in last frame's state as well?
    for i=1,14 do
        if last_state[i] == 0 and js[i] == 1 then
            self:on_button_pressed(i)
        end
        last_state[i] = js[i]
    end
end

function simple_quads:on_button_pressed(i)
    if i == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
    elseif i == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
    end
end

return simple_quads
