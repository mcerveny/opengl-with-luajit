--[[ scene_selector.lua

 The quickest and easiest scene that:
  - draws something 
  - plays sounds
  - takes input
]]

scene_selector = {}
scene_selector.__index = scene_selector

local ffi = require "ffi"
local sf = require "util.shaderfunctions"
local SDL = require "lib/sdl" -- For joystick list

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function scene_selector.new(...)
    local self = setmetatable({}, scene_selector)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local scenedir = 'scene_es2'
local scene_modules = {}
local scene_idx = 1

-- We really shouldn't be doing this...
-- Getting a list of files using the shell is a bad idea.
-- The alternative is building LuaFilesystem.
local use_shell_hacks_to_get_scene_list = true
if use_shell_hacks_to_get_scene_list then
    require("util.shell_hacks")
    scene_modules = {}
    local files = scandir_portable_sorta(scenedir)
    --print(ffi.os, #files)
    for k,v in pairs(files) do
        local s = v:gsub(".lua", "")
        --print(k,v,s)
        if s ~= 'scene_selector' then
            table.insert(scene_modules, s)
        end
    end
end

function scene_selector:getSelectedSceneName()
    return scene_modules[scene_idx]
end

function scene_selector:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function scene_selector:init()
    self.buttons = {}
end

function scene_selector:initGL()
    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory(self.dataDir.."/fonts")
    glfont:initGL()
end

function scene_selector:exitGL()
    glfont:exitGL()
end

function scene_selector:render()
    gl.glClearColor(.2,.2,.2,1)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    glfont:render_string(m, p, col, "Scene Selector")

    mm.glh_translate(m, 0, 200, 0)
    local str = tostring(scene_idx)..'/'..tostring(#scene_modules)..'  '..scene_modules[scene_idx]
    glfont:render_string(m, p, col, str)

    self:renderJoysticksText()
end

function scene_selector:renderJoysticksText()
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .25
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 900, 0)
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    local col = {1, 1, 0}

    local nj = SDL.SDL_NumJoysticks()
    for i=0,nj-1 do
        local jname = SDL.SDL_JoystickNameForIndex(i)
        local jp = SDL.SDL_JoystickOpen(i)
        local nb = SDL.SDL_JoystickNumButtons(jp)
        local na = SDL.SDL_JoystickNumAxes(jp)
        local str = tostring(i)..') '..ffi.string(jname)..nb..' buttons, '..na..' axes'

        glfont:render_string(m, p, col, str)
        mm.glh_translate(m, 0, 140, 0)

        -- Attempt to give some more info with guids
        if false then
            --local guid = SDL.SDL_JoystickGetDeviceGUID(i)
            local guid = SDL.SDL_JoystickGetGUID(jp)
            local guidbuf = ffi.new("char[64]")
            SDL.SDL_JoystickGetGUIDString(guid, guidbuf, 64)
            mm.glh_translate(m, 120, 0, 0)
            glfont:render_string(m, p, col, ffi.string(guidbuf))
            mm.glh_translate(m, -120, 140, 0)
        end
    end
end

function scene_selector:timestep(absTime, dt)
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function scene_selector:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then
            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end
        end
    end
end

function scene_selector:on_button_pressed(i)
    if i == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        -- TODO select scene
    elseif i == lr_input.JOYPAD_DOWN then
        snd.playSound("pop_drip.wav")
        scene_idx = scene_idx + 1
        scene_idx = math.min(scene_idx, #scene_modules)
    elseif i == lr_input.JOYPAD_UP then
        snd.playSound("pop_drip.wav")
        scene_idx = scene_idx - 1
        scene_idx = math.max(scene_idx, 1)
    end
end

return scene_selector
