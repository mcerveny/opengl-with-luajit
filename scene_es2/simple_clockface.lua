--[[ simple_clockface.lua

    A simple example of animation.

    The time is set in the timestep function and stored in a
    module-scoped variable. This variable is referenced during
    drawing to rotate the three hands of a stopwatch.

    The largest hand ticks once per second and completes a rotation
    after one minute. The middle hand completes one smooth rotation
    per second. The smallest hand rotates 10 times per second.

    This scene can be useful to check that the internal time functions
    are returning values consistent with wall clock time. 
]]
simple_clockface = {}
simple_clockface.__index = simple_clockface

function simple_clockface.new(...)
    local self = setmetatable({}, simple_clockface)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_clockface:init()
    self.vbos = {}
    self.prog = 0
    self.absoluteTime = 0 -- Hold time here for drawing
end

--local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv   = ffi.typeof('GLint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 100

attribute vec4 vPosition;

uniform mat4 mvmtx;
uniform float aspect;

void main()
{
    vec4 pos = mvmtx * vPosition;
    pos.x /= aspect;
    gl_Position = pos;
}
]]

local basic_frag = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform vec3 color;

void main()
{
    gl_FragColor = vec4(color, 1.0);
}
]]

function simple_clockface:init_tri_attributes()
    local verts = glFloatv(3*3, {
        -.1,0,0,
        .1,0,0,
        0,1,0,
        })

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
end

function simple_clockface:resizeViewport(win_w, win_h)
    self.win_w,self.win_h = win_w,win_h
end

function simple_clockface:initGL()
    self.prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:init_tri_attributes()
end

function simple_clockface:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_clockface:render()
    gl.glClearColor(0,0,0,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    gl.glDisable(GL.GL_DEPTH_TEST)

    local umv_loc = gl.glGetUniformLocation(self.prog, "mvmtx")
    local uasp_loc = gl.glGetUniformLocation(self.prog, "aspect")
    local ucol_loc = gl.glGetUniformLocation(self.prog, "color")
    gl.glUseProgram(self.prog)
    gl.glUniform1f(uasp_loc, self.win_w/self.win_h)
    
    -- if not self.absoluteTime then return end
    local time = self.absoluteTime
    if self.startTime then time = time - self.startTime end

    local m01= {}
    mm.make_identity_matrix(m01)
    local rotations01 = .1*time
    rotations01 = math.floor(rotations01*10)/60
    mm.glh_rotate(m01, 360*rotations01, 0,0,-1)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m01))
    gl.glUniform3f(ucol_loc, 0,0,1)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,3)

    local m = {}
    mm.make_identity_matrix(m)
    mm.glh_rotate(m, 360*time, 0,0,-1)
    mm.glh_scale(m, .5,.5,.5)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glUniform3f(ucol_loc, 1,0,0)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,3)

    local m10 = {}
    mm.make_identity_matrix(m10)
    local rotations10 = 10*time
    mm.glh_rotate(m10, 360*rotations10, 0,0,-1)
    mm.glh_scale(m10, .25,.25,.25)
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m10))
    gl.glUniform3f(ucol_loc, 0,1,0)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,3)

    gl.glUseProgram(0) 
end

function simple_clockface:timestep(absTime, dt)
    if not self.startTime then self.startTime = absTime end
    self.absoluteTime = absTime
end

return simple_clockface
