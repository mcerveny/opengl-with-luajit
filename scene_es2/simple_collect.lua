--[[ simple_collect.lua

    Each player collects targets on screen. Count is kept for each.
]]

simple_collect = {}
simple_collect.__index = simple_collect

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function simple_collect.new(...)
    local self = setmetatable({}, simple_collect)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_collect:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_collect:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 16*16*4
    self.offsets = {}
    for i=1,self.n_quads do
        local off = {-1+2*math.random(), -1+2*math.random()}
        table.insert(self.offsets,off)
    end

    self.movers = {}
end

vs=[[
#version 100
uniform float uTime;
uniform vec2 uOffset;
attribute vec2 vP;
varying vec3 vfC;

void main()
{
    float rot = uTime;
    mat2 m = mat2(cos(rot), -sin(rot), sin(rot), cos(rot));
    vfC = vec3(vP,0.);
    vec2 pt = m * vP;
    gl_Position = vec4(pt+uOffset,0.,1.);
}
]]

fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_collect:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    local s = .05
    v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    vl = gl.glGetAttribLocation(self.prog,"vP")
    vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)

    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory(self.dataDir.."/fonts")
    glfont:initGL()
end

function simple_collect:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_collect:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    -- Targets
    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)

    local ut_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(ut_loc, self.t)

    -- TODO: pseudo-instancing
    for i=1,self.n_quads do
        local off = self.offsets[i]
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
        gl.glUniform2f(uoff_loc, off[1], off[2])

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end

    -- Collectors
    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)
    local ut_loc = gl.glGetUniformLocation(self.prog, "uTime")
    gl.glUniform1f(ut_loc, 0)
    
    for i=1,#self.movers do
        local off = self.movers[i][1]
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
        gl.glUniform2f(uoff_loc, off[1], off[2])

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end


    -- Score/fps overlay
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    local str = ''
    for i=1,#self.movers do
        str = str..tostring(self.movers[i].score)..'  '
    end
    glfont:render_string(m, p, col, str)

    if self.fps then
        mm.glh_translate(m, 0, 120, 0)
        glfont:render_string(m, p, col, 'simple_collect '..self.fps.." fps")
    end
end

function simple_collect:timestep(absTime, dt)
    self.t = absTime
    for j=1,#self.movers do
        local vel = self.movers[j][2]
        for i=1,2 do
            self.movers[j][1][i] = math.min(math.max(-1,self.movers[j][1][i] + vel[i]),1)
        end

        -- Check hits
        local rad2 = .1*.1
        for i=1,self.n_quads do
            local mov = self.movers[j]
            if not mov then return end
            local off = mov[1]
            if not off then return end
            local target = self.offsets[i]
            local dx,dy = target[1]-off[1], target[2]-off[2]
            local dist2 = dx*dx+dy*dy
            if dist2 < rad2 then
                --TODO: remove them from the list
                self.offsets[i][1] = -99
                self.offsets[i][2] = -99
                snd.playSound("pop_drip.wav")
                self.movers[j].score = self.movers[j].score + 1
            end
        end
    end
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function simple_collect:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then

            -- Create mover if not already there
            if not self.movers[j] then
                local offset = {0,0}
                local vel = {0,0}
                self.movers[j] = {offset,vel,['score']=0}
            end

            local v = .01
            local vel = self.movers[j][2]
            vel[1] = 0
            vel[2] = 0

            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end

            if js[lr_input.JOYPAD_LEFT] ~= 0 then
                vel[1] = -v
            elseif js[lr_input.JOYPAD_RIGHT] ~= 0 then
                vel[1] = v
            end
            if js[lr_input.JOYPAD_UP] ~= 0 then
                vel[2] = v
            elseif js[lr_input.JOYPAD_DOWN] ~= 0 then
                vel[2] = -v
            end
        end
    end
end

function simple_collect:on_button_pressed(i)
    if i == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self.t_last = self.t
    elseif i == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self.t_last = self.t
    elseif i == lr_input.JOYPAD_START then
        -- Reset
        self.offsets = {}
        for i=1,self.n_quads do
            local off = {-1+2*math.random(), -1+2*math.random()}
            table.insert(self.offsets,off)
        end
    end
end

return simple_collect
