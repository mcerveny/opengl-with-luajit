--[[ frogs_eating_flies.lua

    Each player collects targets on screen. Count is kept for each.
]]

frogs_eating_flies = {}
frogs_eating_flies.__index = frogs_eating_flies

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function frogs_eating_flies.new(...)
    local self = setmetatable({}, frogs_eating_flies)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function frogs_eating_flies:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function frogs_eating_flies:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}

    self.n_quads = 16*4
    self.offsets = {}
    self.velocities = {}
    self.accels = {}
    for i=1,self.n_quads do
        local off = {-1+2*math.random(), -1+2*math.random()}
        table.insert(self.offsets,off)
        local v = {0,0}
        table.insert(self.velocities,v)
        local a = {0,0}
        table.insert(self.accels,a)
    end

    self.movers = {}
end

function frogs_eating_flies:loadTexture(texfilename, w, h)
    if self.dataDir then texfilename = self.dataDir .. "/images/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
    --gl.glTexParameteri( GL.GL_TEXTURE_2D, GL.GL_DEPTH_TEXTURE_MODE, GL.GL_INTENSITY ); --deprecated, out in 3.1
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
                  w, h, 0,
                  GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    return texID
end

targetVs=[[
#version 100

attribute vec2 aPos;
attribute vec2 aTex;

uniform float uRot;
uniform vec2 uOffset;
uniform vec2 texScale;
uniform vec2 texOffset;

varying vec2 vfC;

void main()
{
    vfC = aTex * texScale+texOffset;

    float rot = uRot;
    mat2 m = mat2(cos(rot), -sin(rot), sin(rot), cos(rot));
    vec2 pt = m * aPos;
    gl_Position = vec4(pt+uOffset,0.,1.);
}
]]

targetFs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D tex;
varying vec2 vfC;
void main()
{
    vec2 tc = vfC;
    vec4 col = texture2D(tex, tc);
    gl_FragColor = col;
}
]]

function frogs_eating_flies:initTargets()
    self.prog = sf.make_shader_from_source({vsrc=targetVs,fsrc=targetFs})

    -- Vertex positions
    local s = .05
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    local vl = gl.glGetAttribLocation(self.prog, "aPos")
    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
    table.insert(self.vbos, vvbo)

    -- Texture coordinates
    local t = ffi.typeof('GLfloat[?]')(12,{0,0,1,0,1,1,0,0,1,1,0,1,})
    local tl = gl.glGetAttribLocation(self.prog, "aTex")
    local tvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1, tvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, tvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(t), t, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(tl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(tl)
    table.insert(self.vbos, tvbo)
end

collectorVs=[[
#version 100

uniform vec2 uOffset;

attribute vec2 aPos;
attribute vec2 aTex;

varying vec2 vfC;

void main()
{
    float rot = 0.;
    mat2 m = mat2(cos(rot), -sin(rot), sin(rot), cos(rot));
    vfC = aTex;
    vec2 pt = m * aPos;
    gl_Position = vec4(pt+uOffset,0.,1.);
}
]]

collectorFs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;

uniform float uBlue;
uniform sampler2D tex;

void main()
{
    vec2 tc = vfC;
    tc.y = 1.-tc.y;
    vec4 col = texture2D(tex, tc);
    gl_FragColor = col;
}
]]

tongueVs=[[
#version 100

uniform vec3 uOffset;

attribute vec2 aPos;
attribute vec2 aTex;

varying vec2 vfC;

void main()
{
    vfC = aTex;
    vec2 pt = aPos;
    if (pt.y > 0.)
    {
        pt.y += uOffset.z;
    }
    gl_Position = vec4(pt+uOffset.xy,0.,1.);
}
]]

tongueFs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec2 vfC;

uniform float uBlue;
uniform sampler2D tex;

void main()
{
    vec2 tc = vfC;
    tc.y = 1.-tc.y;
    vec4 col = texture2D(tex, tc);
    gl_FragColor = col;
}
]]

function frogs_eating_flies:initCollectors()
    self.progColl = sf.make_shader_from_source({
        vsrc=collectorVs,
        fsrc=collectorFs})

    self.progTongue = sf.make_shader_from_source({
        vsrc=tongueVs,
        fsrc=tongueFs})

    -- Vertex positions
    local s = .15
    v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    vl = gl.glGetAttribLocation(self.progColl, "aPos")
    vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
    table.insert(self.vbos, vvbo)

    -- Texture coordinates
    t = ffi.typeof('GLfloat[?]')(12,{0,0,1,0,1,1,0,0,1,1,0,1,})
    tl = gl.glGetAttribLocation(self.progColl, "aTex")
    tvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1, tvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, tvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(t), t, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(tl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(tl)
    table.insert(self.vbos, tvbo)
end

bgVs=[[
#version 100
attribute vec2 aPos;
uniform vec3 uOffset;
varying vec2 vfC;
void main()
{
    vfC = .5 + .5*aPos;
    gl_Position = vec4(aPos,0.,1.);
}
]]

bgFs=[[
#version 100
#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif
varying vec2 vfC;
uniform sampler2D tex;
void main()
{
    vec2 tc = vfC;
    tc.y = 1.-tc.y;
    vec4 col = texture2D(tex, tc);
    gl_FragColor = col;
}
]]

function frogs_eating_flies:initBackground()
    self.progBg = sf.make_shader_from_source({vsrc=bgVs,fsrc=bgFs})

    -- Vertex positions
    local s = 1
    local v = ffi.typeof('GLfloat[?]')(12,{-s,-s,s,-s,s,s,-s,-s,s,s,-s,s,})
    local vl = gl.glGetAttribLocation(self.prog, "aPos")
    local vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(v), v, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
    table.insert(self.vbos, vvbo)
end

function frogs_eating_flies:initGL()
    self:initTargets()
    self:initCollectors()
    self:initBackground()
    self.frogTexID = self:loadTexture('frog1.data', 32,32)
    self.tongueTexID = self:loadTexture('tongue1.data', 32,32)
    self.bgtexID = self:loadTexture('frogpond_256_200.data', 256,200)
    self.flytexID = self:loadTexture('fly1_spritesheet.data', 32,32)
    self.spritex,self.spritey = 2,2

    local fontname = 'press_start_512'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory(self.dataDir.."/fonts")
    glfont:initGL()
end

function frogs_eating_flies:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    local texdel = ffi.new("GLuint[1]", self.flytexID)
    gl.glDeleteTextures(1,texdel)
    local texdel = ffi.new("GLuint[1]", self.frogTexID)
    gl.glDeleteTextures(1,texdel)
    local texdel = ffi.new("GLuint[1]", self.tongueTexID)
    gl.glDeleteTextures(1,texdel)
    local texdel = ffi.new("GLuint[1]", self.bgtexID)
    gl.glDeleteTextures(1,texdel)
end

function frogs_eating_flies:render()
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    gl.glDisable(GL.GL_BLEND)

    -- Background
    gl.glUseProgram(self.progBg)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[5][0])
    local vl = gl.glGetAttribLocation(self.progBg, "aPos")
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.bgtexID)
    local stex_loc = gl.glGetUniformLocation(self.progBg, "tex")
    gl.glUniform1i(stex_loc, 0)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)


    -- Targets
    gl.glUseProgram(self.prog)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    local vl = gl.glGetAttribLocation(self.prog, "aPos")
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[2][0])
    local tl = gl.glGetAttribLocation(self.prog, "aTex")
    gl.glVertexAttribPointer(tl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(tl)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.flytexID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "tex")
    gl.glUniform1i(stex_loc, 0)

    local durationSeconds = .5
    local dt = math.fmod(self.t,durationSeconds)
    local sx,sy = self.spritex, self.spritey
    local numSteps = sx*sy
    local frame = math.floor(numSteps * (dt / durationSeconds))
    local texOffx = math.fmod(frame, sx)
    local texOffy = math.floor(frame/sy)
    local texoff_loc = gl.glGetUniformLocation(self.prog, "texOffset")
    gl.glUniform2f(texoff_loc, texOffx/sx, texOffy/sy)
    local texsc_loc = gl.glGetUniformLocation(self.prog, "texScale")
    gl.glUniform2f(texsc_loc, 1/sx, 1/sy)

    gl.glEnable(GL.GL_BLEND)
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

    -- TODO: pseudo-instancing
    for i=1,self.n_quads do
        local off = self.offsets[i]
        local uoff_loc = gl.glGetUniformLocation(self.prog, "uOffset")
        gl.glUniform2f(uoff_loc, off[1], off[2])

        local ut_loc = gl.glGetUniformLocation(self.prog, "uRot")
        gl.glUniform1f(ut_loc, self.t + .1*i)

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end

    -- Tongues and frogs share a VBO
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[3][0])
    local vl = gl.glGetAttribLocation(self.prog, "aPos")
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[4][0])
    local tl = gl.glGetAttribLocation(self.prog, "aTex")
    gl.glVertexAttribPointer(tl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(tl)

    -- Tongues
    gl.glUseProgram(self.progTongue)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.tongueTexID)
    local stex_loc = gl.glGetUniformLocation(self.progColl, "tex")
    gl.glUniform1i(stex_loc, 0)

    for i=1,#self.movers do
        local off = self.movers[i][1]
        local uoff_loc = gl.glGetUniformLocation(self.progTongue, "uOffset")
        gl.glUniform2f(uoff_loc, off[1], off[2])

        local dt = self.t - self.movers[i].tLast
        local b = math.max(0, 1-3*dt)
        local u_loc = gl.glGetUniformLocation(self.progTongue, "uBlue")
        gl.glUniform1f(u_loc, b)

        -- Shoots from top of collector
        gl.glUniform3f(uoff_loc, off[1], off[2], self.movers[i].inout)

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end

    -- Collectors
    gl.glUseProgram(self.progColl)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.frogTexID)
    local stex_loc = gl.glGetUniformLocation(self.progColl, "tex")
    gl.glUniform1i(stex_loc, 0)

    local ut_loc = gl.glGetUniformLocation(self.progColl, "uTime")
    gl.glUniform1f(ut_loc, self.t)
    
    for i=1,#self.movers do
        local off = self.movers[i][1]
        local uoff_loc = gl.glGetUniformLocation(self.progColl, "uOffset")
        gl.glUniform2f(uoff_loc, off[1], off[2])

        local dt = self.t - self.movers[i].tLast
        local b = math.max(0, 1-3*dt)
        local u_loc = gl.glGetUniformLocation(self.progColl, "uBlue")
        gl.glUniform1f(u_loc, b)

        gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    end

    -- Score/fps overlay
    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = 1.4
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 15, 15, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    local str = ''
    for i=1,#self.movers do
        str = str..tostring(self.movers[i].score)..'  '
    end
    glfont:render_string(m, p, col, str)

    local showfps = false
    if self.fps and showfps then
        mm.glh_translate(m, 0, 120, 0)
        glfont:render_string(m, p, col, 'frogs_eating_flies '..self.fps.." fps")
    end


    -- Scores under each frog
    mm.make_identity_matrix(m)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    local col = {.75, 1, .75}
    for i=1,#self.movers do
        mm.glh_ortho(p, -1, 1, 1, -1, -1, 1)
        local mov = self.movers[i]
        local o = mov[1]
        mm.make_identity_matrix(m)
        local s = .0016
        mm.glh_translate(m, o[1], -o[2], 0)
        mm.glh_scale(m, s, s, s)
        mm.glh_translate(m, -50, 90, 0)
        glfont:render_string(m, p, col, tostring(self.movers[i].score))
    end
end

function frogs_eating_flies:timestep(absTime, dt)
    self.t = absTime
    
    -- Move targets
    local vmax = .65
    for j=1,self.n_quads do
        for i=1,2 do
            self.velocities[j][i] = self.velocities[j][i] + self.accels[j][i] * dt
            self.velocities[j][i] = math.min(math.max(self.velocities[j][i],-vmax),vmax)
            self.offsets[j][i] = self.offsets[j][i] + self.velocities[j][i] * dt
        end
    end

    -- Randomize accels
    local s = 5
    for i=1,self.n_quads do
        local a = {s*(-1+2*math.random()), s*(-1+2*math.random())}
        self.accels[i] = a
        -- Keep them on screen
        if self.offsets[i][1] < -1 then self.accels[i][1] = s end
        if self.offsets[i][1] > 1 then self.accels[i][1] = -s end
        if self.offsets[i][2] < -1 then self.accels[i][2] = s end
        if self.offsets[i][2] > 1 then self.accels[i][2] = -s end
    end

    -- Check frogs pushing against frogs
    local frad = .25
    for j=1,#self.movers do
        local mov = self.movers[j]
        local off = mov[1]
        --if not mov then return end
        for i=1,#self.movers do
            if i ~= j then
                local mov2 = self.movers[i]
                local off2 = mov2[1]

                local dx,dy = off2[1]-off[1], off2[2]-off[2]
                local dist2 = dx*dx+dy*dy
                if dist2 < frad*frad then
                    local dist = math.pow(dist2,.5)
                    local ndx,ndy = dx/dist, dy/dist
                    local pushlen = .5*(dist - 2*frad)
                    -- TODO: accumulate diff and apply at end for fairness?
                    off[1] = off[1] + pushlen*dx
                    off[2] = off[2] + pushlen*dy
                    off2[1] = off2[1] - pushlen*dx
                    off2[2] = off2[2] - pushlen*dy
                end
            end
        end
    end

    -- Check frog tongues against flies
    for j=1,#self.movers do
        local mov = self.movers[j]
        if not mov then return end

        local vel = mov[2]
        for i=1,2 do
            mov[1][i] = math.min(math.max(-1,mov[1][i] + vel[i]),1)
        end

        local dt = absTime - mov.tLast
        local o = math.min(2*math.pi,5*dt)
        mov.inout = math.min(math.max(0,math.sin(o)),1)

        -- Check against all flies
        if mov.inout > 0 then
            local rad2 = .1*.07
            for i=1,self.n_quads do
                local off = mov[1]
                if not off then return end

                local target = self.offsets[i]
                local dx,dy = target[1]-off[1], target[2]-(off[2] + mov.inout)
                local dist2 = dx*dx+dy*dy
                if dist2 < rad2 then
                    -- Move them offscreen so they can fly back in
                    local o = self.offsets[i]
                    o[1] = -1+2*math.random()
                    o[2] = -1+2*math.random()
                    local len2 = o[1]*o[1] + o[2]*o[2]
                    if len2 < .0001 then o[1] = .1 end -- pathological case
                    local minlen = math.pow(2,.5)
                    local lenscale = minlen / math.pow(len2,.5)
                    o[1] = o[1] * lenscale
                    o[2] = o[2] * lenscale

                    snd.playSound("pop_drip.wav")
                    mov.score = mov.score + 1
                end
            end
        end
    end
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function frogs_eating_flies:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then

            -- Create mover if not already there
            if not self.movers[j] then
                -- Arrange players in a row
                local xo = -1+2*(j)/(#joystates+1)
                local yo = -1+2*(1/4)
                local offset = {xo,yo}
                local vel = {0,0}
                self.movers[j] = {
                    offset,
                    vel,
                    ['score'] = 0,
                    ['tLast'] = -99,
                    ['inout'] = 0,
                }
            end

            local v = .01
            local vel = self.movers[j][2]
            vel[1] = 0
            vel[2] = 0

            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:onButtonPressed(j,b)
                end
                last_states[j][b] = js[b]
            end

            if js[lr_input.JOYPAD_LEFT] ~= 0 then
                vel[1] = -v
            elseif js[lr_input.JOYPAD_RIGHT] ~= 0 then
                vel[1] = v
            end
            if js[lr_input.JOYPAD_UP] ~= 0 then
                vel[2] = v
            elseif js[lr_input.JOYPAD_DOWN] ~= 0 then
                vel[2] = -v
            end
        end
    end
end

function frogs_eating_flies:onButtonPressed(padIndex, button)
    if button == lr_input.JOYPAD_A
    or button == lr_input.JOYPAD_B
    or button == lr_input.JOYPAD_X
    or button == lr_input.JOYPAD_Y
        then
        local mov = self.movers[padIndex]
        local dt = self.t - mov.tLast
        if dt > 1 then -- cooldown
            snd.playSound("test.wav")
            self.movers[padIndex].tLast = self.t
        end
    elseif button == lr_input.JOYPAD_START then
        -- Reset
        self.offsets = {}
        for i=1,self.n_quads do
            local off = {-1+2*math.random(), -1+2*math.random()}
            table.insert(self.offsets,off)
        end
    end
end

return frogs_eating_flies
