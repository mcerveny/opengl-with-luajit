--[[ simple_slides.lua

 The quickest and easiest scene that:
  - draws something 
  - plays sounds
  - takes input
]]

simple_slides = {}
simple_slides.__index = simple_slides

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'

require("util.slideshow")
local slides = {
    Slideshow.new({
        title="Khronos APIs on Raspberry Pi",
        bullets={
        "",
        "",
        "    Jim Susinno",
        "    Khronos Boston Chapter",
        },
        shown_lines = 4,
    }),

    Slideshow.new({
        title="A cool platform",
        bullets={
            "- Cheap",
            "- Capable",
            "- Popular",
        },
    }),

    Slideshow.new({
        title="How cheap?",
        bullets={
            "- Pi 3                      $35",
            "- 2.5A Power Cable  $10",
            "- MicroSD Card      $10",
            "- Case  (optional)",
            "*Input devices not included"
        },
    }),

    Slideshow.new({
        title="How capable?",
        bullets={
            "- OpenGLES 2.0",
            "- OpenGL 2",
            "- OpenMAX, OpenVG",
            "- EGL   No OpenSL?",
            "- Debian: Python, Mathematica, etc.",
        },
    }),

    Slideshow.new({
        title="How popular?",
        bullets={
            "Model 2: 3 million",
            "Model 3: 5 million",
            "Model 0: 1-2 million",
            "Total: 15 million",
        },
    }),

    Slideshow.new({
        title="GL Extensions",
        bullets={
            "- GL_MAX_TEXTURE_SIZE = 2048",
            "- GL_MAX_TEX..._UNITS = 8",
            "- GL_OES_texture_npot",
            "- GL_OES_vertex_half_float",
            "- GL_OES_mapbuffer",
        },
    }),

    Slideshow.new({
        title="GLSL",
        bullets={
            "OpenGL ES GLSL ES 1.00",
            "#version 100",
            "#version 120 with OpenGL"
        },
    }),
    
    Slideshow.new({
        title="Broadcom VideoCore IV",
        bullets={
            "Docs released",
            "Graphics stack BSD 3-claused",
            "Firmware still proprietary",
            "Eric Anholt developed free driver",
            "Arch Ref Guide available online"
        },
    }),
    
    Slideshow.new({
        title="Linux Graphics Stack",
        bullets={
            "OpenGL on X11/dispmanx",
            "GLES on VT",
            "    $ chvt",
            "SDL - targets VT or X11",
            "Glfw works on X"
        },
    }),
    
    Slideshow.new({
        title="Boot configuration",
        bullets={
            "/boot/config.txt",
            "gpu mem",
            "arm/gpu/sdram freq",
            "over voltage, temp limit...",
            "Voids warranty!"
        },
    }),
    
    Slideshow.new({
        title="Manufacturing Variation",
        bullets={
            "Some overclock better than others",
            "Get the 2.5A power supply",
            "Stick-on heatsinks: do they help?",
        },
    }),

}

local slide_idx = 1
local slide = slides[1]


-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function simple_slides.new(...)
    local self = setmetatable({}, simple_slides)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_slides:setDataDirectory(dir)
    self.dataDir = dir
	if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_slides:init()
    self.buttons = {}
end

function simple_slides:initGL()
    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory(self.dataDir.."/fonts")
    glfont:initGL()
    slide:initGL(self.dataDir)
end

function simple_slides:exitGL()
    slide:exitGL()
end

function simple_slides:render()
    gl.glClearColor(1,1,1,1)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT)

    slide:draw_text(glfont)
end

function simple_slides:timestep(absTime, dt)
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function simple_slides:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then
            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end
        end
    end
end

function simple_slides:increment_slide(incr)
    slide_idx = slide_idx + incr

    if slide_idx < 1 then slide_idx = 1 end
    if slide_idx > #slides then slide_idx = #slides end

    slide:exitGL()
    slide = slides[slide_idx]
    slide:initGL(dataDir)
end

function simple_slides:forward()
    if slide.shown_lines >= #slide.bullet_points then
        self:increment_slide(1)
        return true
    end
    slide:advance(1)
end

function simple_slides:back()
    if slide.shown_lines <= 0 then
        self:increment_slide(-1)
        return true
    end
    slide:advance(-1)
end

function simple_slides:on_button_pressed(i)
    if i == lr_input.JOYPAD_A then
        snd.playSound("test.wav")
        self:forward()
    elseif i == lr_input.JOYPAD_B then
        snd.playSound("pop_drip.wav")
        self:back()
    elseif i == lr_input.JOYPAD_L then
        snd.playSound("pop_drip.wav")
        self:increment_slide(-1)
    elseif i == lr_input.JOYPAD_R then
        snd.playSound("pop_drip.wav")
        self:increment_slide(1)
    end
end

return simple_slides
