--[[ simple_shader.lua

 The quickest and easiest scene that:
  - draws something 
  - plays sounds
  - takes input
]]

simple_shader = {}
simple_shader.__index = simple_shader

local ffi = require "ffi"
local sf = require "util.shaderfunctions"

require("util.glfont")
local mm = require("util.matrixmath")
local glfont = nil
local lr_input = require 'util.libretro_input'

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function simple_shader.new(...)
    local self = setmetatable({}, simple_shader)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_shader:setDataDirectory(dir)
    self.dataDir = dir
    if snd.setDataDirectory then snd.setDataDirectory(dir) end
end

function simple_shader:init()
    self.t = 0
    self.t_last = 0
    self.vbos = {}
    self.prog = 0
    self.buttons = {}
end

vs=[[
#version 100
attribute vec2 vP;
varying vec3 vfC;
void main()
{
    vfC=vec3(vP,0.);
    gl_Position=vec4(vP,0.,1.);
}
]]

fs=[[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfC;
uniform float uBlue;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col,1.);
}
]]

function simple_shader:initGL()
    self.prog = sf.make_shader_from_source({vsrc=vs,fsrc=fs})
    v = ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
    vl = gl.glGetAttribLocation(self.prog,"vP")
    vvbo = ffi.typeof('GLint[?]')(0)
    gl.glGenBuffers(1,vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
    gl.glEnableVertexAttribArray(vl)
    gl.glBindVertexArray(0)
    table.insert(self.vbos, vvbo)

    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory(self.dataDir.."/fonts")
    glfont:initGL()
end

function simple_shader:exitGL()
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
end

function simple_shader:render()
    gl.glClearColor(0,1,0,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    gl.glUseProgram(self.prog)
    local dt = self.t - self.t_last
    local b = math.max(0, 1-3*dt)
    local u_loc = gl.glGetUniformLocation(self.prog, "uBlue")
    gl.glUniform1f(u_loc, b)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)


    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)
    mm.glh_translate(m, 30, 30, 0)
    -- TODO getStringWidth and center text
    if not win_w then
        win_w,win_h = 800,600
    end
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    glfont:render_string(m, p, col, "simple_shader")
end

function simple_shader:timestep(absTime, dt)
    self.t = absTime
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function simple_shader:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then
            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end
        end
    end
end

function simple_shader:on_button_pressed(i)
    self.t_last = self.t
    if i == lr_input.JOYPAD_A then
        snd.playSound("pop_drip.wav")
    elseif i == lr_input.JOYPAD_B then
        snd.playSound("Jump5.wav")
    elseif i == lr_input.JOYPAD_X then
        snd.playSound("Pickup_Coin7.wav")
    elseif i == lr_input.JOYPAD_Y then
        snd.playSound("test.wav")
    end
end

return simple_shader
