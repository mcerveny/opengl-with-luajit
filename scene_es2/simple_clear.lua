--[[ simple_clear.lua

 The quickest and easiest scene that:
  - draws something 
  - plays sounds
  - takes input
]]

simple_clear = {}
simple_clear.__index = simple_clear

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local lr_input = require 'util.libretro_input'
local snd = prequire 'util.libretro_audio'
if not snd then
    snd = require "util.soundfx"
end

function simple_clear.new(...)
    local self = setmetatable({}, simple_clear)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function simple_clear:setDataDirectory(dir)
    self.dataDir = dir
	snd.setDataDirectory(dir)
end

function simple_clear:init()
    self.t = 0
end

function simple_clear:initGL()
end

function simple_clear:exitGL()
end

function simple_clear:render() --_for_one_eye(view, proj)
    local t = 3*self.t
    local function f(t) return .5+.5*math.sin(t) end
    gl.glClearColor(f(t), f(1.3*t), f(1.7*t), 0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
end

function simple_clear:timestep(absTime, dt)
    self.t = absTime
end

local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function simple_clear:update_retropad(joystates)
    for j=1,#joystates do
        local js = joystates[j]
        if js then
            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    self:on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end
        end
    end
end

function simple_clear:on_button_pressed(i)
    self.t_last = self.t
    print("  pressed",i)
    if i == lr_input.JOYPAD_A then
        snd.playSound("pop_drip.wav")
    elseif i == lr_input.JOYPAD_B then
        snd.playSound("Jump5.wav")
    elseif i == lr_input.JOYPAD_X then
        snd.playSound("Pickup_Coin7.wav")
    elseif i == lr_input.JOYPAD_Y then
        snd.playSound("test.wav")
    end
end

return simple_clear
