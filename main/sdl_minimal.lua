-- sdl_mininal.lua
-- Entry point for a simple SDL OpenGL app
print(jit.version, jit.os, jit.arch)

package.path = package.path .. ';lib/?.lua'

local ffi = require "ffi"
local SDL = require "lib/sdl"

local openGL = require "opengl"
openGL.loader = SDL.SDL_GL_GetProcAddress
openGL:import()

local window
local win_w = 800
local win_h = 600

function initGL()
end

function display()
    gl.glViewport(0,0, win_w, win_h)
    local t = 100*os.clock()
    local function f(t) return .5+.5*math.sin(t) end
    gl.glClearColor(f(t), f(1.3*t), f(1.7*t), 0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
end

function main()
    SDL.SDL_Init(SDL.SDL_INIT_VIDEO)

    window = SDL.SDL_CreateWindow(
        "An SDL2 window",
        SDL.SDL_WINDOWPOS_UNDEFINED,
        SDL.SDL_WINDOWPOS_UNDEFINED,
        win_w,
        win_h,
        ffi.C.SDL_WINDOW_OPENGL +
        ffi.C.SDL_WINDOW_SHOWN +
        ffi.C.SDL_WINDOW_RESIZABLE
    )

    if window == nil then
        print("Could not create window: " .. ffi.string( SDL.SDL_GetError() ))
        os.exit(1)
    end

    glContext = SDL.SDL_GL_CreateContext(window);
    if glContext == nil then
        print("There was an error creating the OpenGL context!\n")
        return 0
    end

    SDL.SDL_GL_MakeCurrent(window, glContext)

    print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
    print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
    print(ffi.string(gl.glGetString(GL.GL_VERSION)))
    print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))

    local frames = 0
    local startTime = os.clock()
    local quit = false
    while not quit do
        local event = ffi.new("SDL_Event")
        while SDL.SDL_PollEvent(event) ~= 0 do
            if event.type == SDL.SDL_KEYDOWN then
                if event.key.keysym.sym == SDL.SDLK_ESCAPE then
                    quit = true
                end
                print(event.key.keysym.sym)
            end
        end

        display()
        SDL.SDL_GL_SwapWindow(window)
        frames = frames + 1
    end
    local endTime = os.clock()
    print(math.floor(frames/(endTime-startTime)).." fps")

    SDL.SDL_DestroyWindow(window)
    SDL.SDL_Quit()
end
main()
