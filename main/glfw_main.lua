--[[ main_glfw.lua

    Entry point for the pure LuaJIT engine
]]
print(jit.version, jit.os, jit.arch)

package.path = package.path .. ';lib/?.lua'
local ffi = require("ffi")
local glfw = require("glfw")
local openGL = require("opengl")
openGL.loader = glfw.glfw.GetProcAddress
openGL:import()
local DEBUG = false
local DEBUG_quitonerror = false

local snd = require("util.soundfx")

-- http://stackoverflow.com/questions/17877224/how-to-prevent-a-lua-script-from-failing-when-a-require-fails-to-find-the-scri
local function prequire(m)
  local ok, err = pcall(require, m)
  if not ok then return nil, err end
  return err
end

local socket = nil
if (ffi.os == "Windows") then
    package.cpath = package.cpath .. ';bin/windows/socket/core.dll'
    package.loadlib("socket/core.dll", "luaopen_socket_core")
    socket = require 'socket.core'
elseif (ffi.os == "Linux") then
    package.cpath = package.cpath .. ';bin/linux/'..jit.arch..'/socket/?.so'
    socket = prequire 'socket.core'
elseif (ffi.os == "OSX") then
    package.cpath = package.cpath .. ';bin/osx/socket/?.so'
    socket = require 'socket.core'
end

local clock = os.clock
if socket then
    print("Using socket.gettime in Luasocket version "..socket._VERSION)
    clock = socket.gettime
end

-- https://gist.github.com/calio/49653917ba24e4bec067
local ffi = require("ffi")
ffi.cdef[[
    typedef long time_t;
    typedef int clockid_t;

    typedef struct timespec {
            time_t   tv_sec;        /* seconds */
            long     tv_nsec;       /* nanoseconds */
    } nanotime;
    int clock_gettime(clockid_t clk_id, struct timespec *tp);
]]
function clock_gettime()
    local pnano = assert(ffi.new("nanotime[?]", 1))

    -- CLOCK_REALTIME -> 0
    ffi.C.clock_gettime(1, pnano)
    return pnano[0]
end

clock = function()
    local nano = clock_gettime()
    local s =
        tonumber(nano.tv_sec) +
        tonumber(nano.tv_nsec) / 1e9
    return s
end


require("util.glfont")
local CameraLibrary = require("util.camera4")
local EditorLibrary = require("scene.stringedit_scene")
local PromptLibrary = require("util.cmdprompt")
local mm = require("util.matrixmath")
local fpstimer = require("util.fpstimer")

local Scene = nil
local PostFX = nil
local glfont = nil
local Camera = CameraLibrary.new()
local Editor = nil
local Prompt = PromptLibrary.new()
local CmdTarget = Scene

local win_w = 2160/2
local win_h = 1440/2
local fb_w = win_w
local fb_h = win_h
local g_ft = fpstimer.create()
local lastSceneChangeTime = 0

local editor_toggle_first_char_hack = nil
local caught_error_in_draw = nil

local scenedir = "scene"
local effectdir = "effect"
local cameradir = "util"

-- Automatically display help if prompt is up
function showHelpForComponent(comp)
    if Prompt and Prompt.show then
        Prompt.lines = {}
        if comp and comp.commandHelp then
            Prompt.lines = comp:commandHelp()
        end
    end
end

-- Helper for below
-- http://lua-users.org/wiki/SplitJoin
function split_into_lines(str)
    local t = {}
    local function helper(line) table.insert(t, line) return "" end
    helper((str:gsub("(.-)\r?\n", helper)))
    return t
end

-- Separates a lua error message into parts:
--  The file it occurred in
--  The line number
--  The error message
function parseLuaErrorMessage(err)
    if not err then return end
    local errlines = split_into_lines(err)
    -- Expect 2 lines: 1 has error, 2 has location
    if not errlines then return end
    local fileerr = errlines[1]
    if #errlines > 1 then fileerr = errlines[2] end
    if not fileerr then return end
    fileerr = fileerr:match( "^%s*(.+)" )

    -- Extract line number between colons
    local a = string.find(fileerr, ':')
    if not b then print(err) return end
    local b = string.find(fileerr, ':', a+1)
    if not b then return end
    local errtext = string.sub(fileerr,b+2,#fileerr)
    local linenum = tonumber(string.sub(fileerr, a+1, b-1))
    local errfile = string.sub(fileerr, 1,a-1)
    return errfile, linenum, errtext
end

-- Pull out the source file mentioned in the error message
-- and print the context around the offending line.
function printContextOfErrorLine(err)
    local errfile, linenum, errtext = parseLuaErrorMessage(err)
    if not errfile then return end
    print('LUA ERROR ________'..errfile..':________',errtext)

    local lines = {}
    local file = io.open(errfile)
    if file then
        for line in file:lines() do
            table.insert(lines, line)
        end
        -- Print n lines of source context
        local n = 3
        for n=-n,n do
            local lnum = linenum + n
            local indicator = '\t'..tostring(lnum)
            local lnumwid = 8 -- pad digits
            if n==0 then
                indicator = indicator..'>>> '
            end
            for i=#indicator,lnumwid-1 do
                indicator = indicator..' '
            end
            print(indicator..lines[lnum])
        end
        file:close()
    else
        print("file "..errfile.." not found.")
    end
end

-- Grab the callstack from GL debug callback and print error's context.
function printContextFromCallstack(callstack)
    if not callstack then return end
    local lines = split_into_lines(callstack)
    if #lines < 2 then return end
    printContextOfErrorLine(lines[2])
end

function switch_to_scene(name)
    local fullname = scenedir.."."..name
    if Scene and Scene.exitGL then
        Scene:exitGL()
    end
    -- Do we need to unload the module?
    package.loaded[fullname] = nil
    Scene = nil

    if not Scene then
        local status,err = pcall(function ()
            local SceneLibrary = require(fullname)
            Scene = SceneLibrary.new()
        end)
        if not status then
            printContextOfErrorLine(err)
            local lines = split_into_lines(err)
            Prompt.lines = lines
        end
        if Scene then
            local now = clock()
            if Scene.setDataDirectory then Scene:setDataDirectory("data") end
            if Scene.resizeViewport then Scene:resizeViewport(win_w,win_h) end
            Scene:initGL()
            local initTime = clock() - now
            lastSceneChangeTime = now
            collectgarbage()
            print(name,
                "init time: "..math.floor(1000*initTime).." ms",
                "memory: "..math.floor(collectgarbage("count")).." kB")
            showHelpForComponent(Scene)
        end
    end
end

function switch_to_effect(name, params)
    if PostFX and PostFX.exitGL then
        PostFX:exitGL()
    end

    PostFX = nil
    if name == nil then return end
    local fullname = effectdir.."."..name
    package.loaded[fullname] = nil -- is this right? Shouldn't it be the *old* name?

    if not PostFX then
        --pcall(function ()
            local PostFXLibrary = require(fullname)
            PostFX = PostFXLibrary.new(params)
        --end)
        if PostFX then
            if PostFX.setDataDirectory then PostFX:setDataDirectory("data") end
            PostFX:initGL(win_w, win_h)
            lastSceneChangeTime = clock()
            showHelpForComponent(PostFX)
        end
    end
end

function switch_to_camera(name, params)
    local OldCam = Camera
    Camera = nil
    if name == nil then return end
    local fullname = cameradir.."."..name
    package.loaded[fullname] = nil -- is this right? Shouldn't it be the *old* name?

    if not Camera then
        pcall(function ()
            local CameraLibrary = require(fullname)
            Camera = CameraLibrary.new(params)
        end)
        if Camera then
            if Camera.setDataDirectory then Camera:setDataDirectory("data") end
            Camera:init()
            showHelpForComponent(Camera)
        else
            Camera = OldCam
        end
    end
end

local scene_modules = {
    "poisson_multigrid",
"clockface",
"swirly_text",
    "comets_in_space",
    "comets_in_space2",
    "comet",
    "meteorite",
    "space_scene",
    "starfield",
"ships_in_space",
"moon",
"sdf_mesh",
"text_terminal",
    "comet",
    "coma",
    "ejecta",
    "particles",
    "objscene",
    "coma",
    "textured_fbo",
    "moon",
    "cone",
    "shader_editor",
    "textured_fbo",
    "fbos_in_space",
    "list_of_scenes",
    "textured_quad",
    "textured_cubes",
    "hybrid_scene",
    "multipass_example",
    "linegraph",
    "diaspora04",
    "cone",
    "droid",
    "moon",
    "wavy_sheet",
    "eyetest",
    "molecule",
    "font_test",
    "shader_editor",
    "vs",
    "exploding_scene",
    "colorcube",
    "objscene",
    "gs",
    --"key_check",
    "compute_image",
    "clockface",
    "cubemap",
    "ifs",
    "nbody07",
    "simple_game",
    "image_fragments",
    "volume_render",

    -- Flat things
    "red_sunset",
    "a_sunset",
    "ThiSpawns_sunsets",
    "TDMs_seascape",
    "julia_set",
    "life",
    "fs",
    "touch_shader2",
    "slideshow_scene",
}

-- We really shouldn't be doing this...
-- Getting a list of files using the shell is a bad idea.
-- The alternative is building LuaFilesystem.
local use_shell_hacks_to_get_scene_list = false
if use_shell_hacks_to_get_scene_list then
    require("util.shell_hacks")
    scene_modules = {}
    local files = scandir_portable_sorta(scenedir)
    print(ffi.os, #files)
    for k,v in pairs(files) do
        local s = v:gsub(".lua", "")
        print(k,v,s)
        table.insert(scene_modules, s)
    end
end

local scene_module_idx = 1
function switch_scene(reverse)
    if reverse then
        scene_module_idx = scene_module_idx - 1
        if scene_module_idx < 1 then scene_module_idx = #scene_modules end
    else
        scene_module_idx = scene_module_idx + 1
        if scene_module_idx > #scene_modules then scene_module_idx = 1 end
    end
    switch_to_scene(scene_modules[scene_module_idx])

    -- Sound from http://rcptones.com/dev_tones/#tabr1
    snd.playSound("pop_drip.wav")
end

function toggle_editor()
    -- Disabled for now - to be revisited when:
    -- Source can be loaded and compiled and executed directly from memory
    -- Scene can be run in a different thread with its own context
    -- Then, a rethinking of architecture is in order:
    -- Where should the editor live? How should it connect to the scenes's
    -- code to be edited?

    --[[
    if Editor == nil then
        local filename = scenedir..'/'..scene_modules[scene_module_idx]..'.lua'
        Editor = EditorLibrary.new(filename)
        if Editor then
            if Editor.setDataDirectory then Editor:setDataDirectory("data") end
            Editor:initGL()
            editor_toggle_first_char_hack = true
        end    
    else
        Editor:exitGL()
        Editor = nil
    end
    ]]
end

function onKey_Prompt(window, key, scancode, action, mods)
    if Prompt and Prompt.show then
        if key == glfw.GLFW.KEY_TAB and action == glfw.GLFW.PRESS then
            Prompt:toggle()
        end
        if key == glfw.GLFW.KEY_ESCAPE and action == glfw.GLFW.PRESS then
            Prompt:toggle()
        end
        Prompt:keypressed(key, scancode, action, mods)
        if key == glfw.GLFW.KEY_ENTER and action == glfw.GLFW.PRESS then
            -- Handle command prompt
            -- Pass command along to scene, effect or camera
            local args = Prompt:handleCommand()
            onCommand(args)
        end
        return true
    end
    return false
end

-- If editor active, pass keys to it
function onKey_Editor(window, key, scancode, action, mods)
    if Editor then
        if action == glfw.GLFW.PRESS or action == glfw.GLFW.REPEAT then
            -- Check for our escape sequence
            if key == glfw.GLFW.KEY_GRAVE_ACCENT or
                key == glfw.GLFW.KEY_ESCAPE then
                toggle_editor()
            else
                Editor:keypressed(key, scancode, action, mods)
            end
            return true
        end
    end
    return false
end

-- X11 on Ubuntu 16 and Fedora 21 gives modifier flags that are out-of-date
-- at the time of key press. The key events themselves come through correctly,
-- so we can manually apply their effects to their bit fields.
-- NOTE: This does not account for the case of holding both sides of the
-- same modifier.
function workaroundX11ModifierState(key, action, mods)
    local key_mod_pairs = {
        [glfw.GLFW.KEY_LEFT_SHIFT] = glfw.GLFW.MOD_SHIFT,
        [glfw.GLFW.KEY_RIGHT_SHIFT] = glfw.GLFW.MOD_SHIFT,
        [glfw.GLFW.KEY_LEFT_CONTROL] = glfw.GLFW.MOD_CONTROL,
        [glfw.GLFW.KEY_RIGHT_CONTROL] = glfw.GLFW.MOD_CONTROL,
        [glfw.GLFW.KEY_LEFT_ALT] = glfw.GLFW.MOD_ALT,
        [glfw.GLFW.KEY_RIGHT_ALT] = glfw.GLFW.MOD_ALT,
    }
    local keymod = key_mod_pairs[key]
    if keymod then
        if action == glfw.GLFW.PRESS then
            mods = bit.bor(mods, keymod)
        elseif action == glfw.GLFW.RELEASE then
            mods = bit.bxor(mods, keymod)
        end
    end
    return mods
end

function onkey(window, key, scancode, action, mods)
    -- Hack around X11's broken modifier handling
    if (ffi.os == "Linux") then
        mods = workaroundX11ModifierState(key, action, mods)
    end

    local altdown = 0 ~= bit.band(mods, glfw.GLFW.MOD_ALT)
    local ctrldown = 0 ~= bit.band(mods, glfw.GLFW.MOD_CONTROL)
    local shiftdown = 0 ~= bit.band(mods, glfw.GLFW.MOD_SHIFT)

    if Prompt then
        Prompt.ctrldown = ctrldown
    end
    if Scene.Editor then
        -- For shaderedit_scene
        --TODO: handle main-scoped editor
        Scene.Editor.ctrldown = ctrldown
    end
    -- Temporary hack for shader_editor Scene which has its own prompt.
    -- TODO: Use this prompt and remove it from shader_editor
    if key == glfw.GLFW.KEY_TAB and action == glfw.GLFW.PRESS then
        if --not Scene.SceneLibraries and
            not ctrldown then
            Prompt:toggle()
            return
        end
    end

    -- Then try main's key handler
    local handled = false
    local func_table_ctrl_shift = {
        [glfw.GLFW.KEY_F1] = function (x) switch_to_camera("camera1") end,
        [glfw.GLFW.KEY_F2] = function (x) switch_to_camera("camera2") end,
        [glfw.GLFW.KEY_F3] = function (x) switch_to_camera("camera3") end,
        [glfw.GLFW.KEY_F4] = function (x) switch_to_camera("camera4") end,
    }
    local func_table_ctrl = {
        [glfw.GLFW.KEY_F1] = function (x) switch_to_effect(nil) end,
        [glfw.GLFW.KEY_F2] = function (x) switch_to_effect("custom_effect") end,
        [glfw.GLFW.KEY_F3] = function (x) switch_to_effect("clut_effect") end,
        [glfw.GLFW.KEY_F4] = function (x) switch_to_effect("effect_chain") end,
        [glfw.GLFW.KEY_F5] = function (x) switch_to_effect("iir_effect") end,
        [glfw.GLFW.KEY_F6] = function (x)
            local params = {}
            params.filter_names = {
                "vignette",
            }
            switch_to_effect("effect_chain", params)
            PostFX.uniforms.vignetteFactor = .3
            PostFX.uniforms.fadeOut = 0
        end,
        [glfw.GLFW.KEY_F7] = function (x)
            local params = {}
            params.filter_names = {
                "convolve",
                "downsample",
            }
            switch_to_effect("effect_chain", params)
        end,
        [glfw.GLFW.KEY_F8] = function (x)
            local params = {}
            params.filter_names = {
                "downsample",
                "convolve",
            }
            switch_to_effect("effect_chain", params)
        end,
        [glfw.GLFW.KEY_F9] = function (x)
            local params = {}
            params.filter_names = {
                "scanline",
                "scanline2",
                "monowarp",
            }
            switch_to_effect("effect_chain", params)
        end,
        [glfw.GLFW.KEY_GRAVE_ACCENT] = function (x) switch_scene(shiftdown) end,
        [glfw.GLFW.KEY_TAB] = function (x) switch_scene(shiftdown) end,
    }
    local func_table = {
        [glfw.GLFW.KEY_ESCAPE] = function (x) glfw.glfw.SetWindowShouldClose(window, 1) end,
        [glfw.GLFW.KEY_GRAVE_ACCENT] = function (x) toggle_editor() end,
    }
    if action == glfw.GLFW.PRESS or action == glfw.GLFW.REPEAT then
        if ctrldown and shiftdown then
            local f = func_table_ctrl_shift[key]
            if f then f() handled = true end
        end
        if handled ~= true and ctrldown then
            local f = func_table_ctrl[key]
            if f then f() handled = true end
        end
        if not handled then
            local promptHandled = onKey_Prompt(window, key, scancode, action, mods)
            if promptHandled then return end
            local editorHandled = onKey_Editor(window, key, scancode, action, mods)
            if editorHandled then return end

            local f = func_table[key]
            if f then f() handled = true end
        end
    end

    -- Now pass to Scene's handler
    if handled == false then
        if action == glfw.GLFW.PRESS or action == glfw.GLFW.REPEAT then
            if Scene and Scene.keypressed then
                local consumed = Scene:keypressed(key, scancode, action, mods)
                if consumed then return end
            end
        elseif action == glfw.GLFW.RELEASE then
            if Scene and Scene.keyreleased then
                local consumed = Scene:keyreleased(key, scancode, action, mods)
                if consumed then return end
            end
        end
    end

    if Camera.onkey then
        Camera:onkey(key, scancode, action, mods)
    end
end

-- Passing raw character values to scene lets us get away with not
-- including glfw key enums in scenes.
function onchar(window,ch)
    -- Mask off lower 8 bits of char for simplicity
    ch = bit.band(ch, 0xff)

    if Prompt and Prompt.show then
        Prompt:charkeypressed(string.char(ch))
        return
    end

    if Editor then
        -- Glfw calls onchar immediately after keypressed, so the tilde
        -- press to toggle the editor issues a char to the file on every
        -- open. This is the workaround.
        if editor_toggle_first_char_hack then
            editor_toggle_first_char_hack = nil
            return
        end

        Editor:charkeypressed(string.char(ch))
        return
    end

    if Scene.charkeypressed then
        Scene:charkeypressed(string.char(ch))
    end
end

function onCommand(args)
    if not args then return end
    if #args < 1 then return end
    --for _,v in pairs(args) do print(_,'['..v..']') end

    -- Main commands
    local main_cmd_table = {
        ['scene'] = function(args)
            if args[2] then switch_to_scene(args[2]) end
        end,
        ['effect'] = function(args)
            if args[2] then switch_to_effect(args[2]) end
        end,
        ['camera'] = function(args)
            if args[2] then switch_to_camera(args[2]) end
        end,
        ['quit'] = function(args)
            os.exit(0)
        end,
    }

    -- Pop off first arg for cmd type
    local cmd = args[1]

    -- Set command target
    if #args == 1 then
        if cmd == 'scene' then
            Prompt.prompt_text = 'Scene'
            Prompt.lines = {'Set target to scene.'}
            CmdTarget = Scene
            return
        elseif cmd == 'effect' then
            Prompt.prompt_text = 'Effect'
            Prompt.lines = {'Set target to effect.'}
            CmdTarget = PostFX
            return
        elseif cmd == 'camera' then
            Prompt.prompt_text = 'Camera'
            Prompt.lines = {'Set target to camera.'}
            CmdTarget = Camera
            return
        end
    end

    if main_cmd_table[cmd] then
        main_cmd_table[cmd](args)
    end

    local varname = args[1]
    local varval = args[2]
    local Target = CmdTarget
    if Target and Target.handleCommand then
        local ret = Target:handleCommand(args)
        if ret then Prompt.lines = ret end
    elseif Target and varname and varval then
        -- TODO: explicitly call 'set'
        Target[varname] = tonumber(varval)
    end

    if args[1] == '?' then
        if Target and Target.commandHelp then
            Prompt.lines = Target:commandHelp()
            return
        else
            Prompt.lines = {
                'Targets:',
                'scene, effect, camera',
                'Commands:',
                'quit',
                'Tab to toggle shell',
                '` to toggle source',
            }
        end
    end
end

function castRayIntoScene(x,y)
    -- TODO: extract getProjectionMatrix(win_w, win_h, fov, near, far) function
    -- In Camera? in main? in Scene?
    local p = {}
    local aspect = win_w / win_h
    mm.glh_perspective_rh(p, 60 * math.pi/180, aspect, .004, 500)

    -- TODO: extract getClickRay(viewMatrix, projMatrix) function
    local invproj = {}
    for i=1,16 do invproj[i] = p[i] end
    mm.projection_inverse(invproj)

    local u,v = x/win_w, y/win_h
    u = 2*u-1
    v = 2*v-1
    local ray_clip = {u, -v, -1, 1}
    local ray_eye = mm.transform(ray_clip, invproj)
    rd = {ray_eye[1], ray_eye[2], -1,0}
    mm.normalize(rd)

    local ro = {0,0,0,1}
    local cammtx = Camera:getViewMatrix()
    local rox = mm.transform(ro, cammtx)
    local rdx = mm.transform(rd, cammtx)

    ---print(rox[1], rox[2], rox[3])
    --print(rdx[1], rdx[2], rdx[3])
    -- TODO: select item in scene so it draws highlighted
    if Scene.onray then Scene:onray(rox, rdx) end
end

function onmousebutton(window, button, action, mods)
    local double_buffer = ffi.new("double[2]")
    glfw.glfw.GetCursorPos(window, double_buffer, double_buffer+1)
    local x,y = double_buffer[0], double_buffer[1]

    local mouse_actions = {
        [glfw.GLFW.RELEASE] = 0,
        [glfw.GLFW.PRESS] = 1,
    }
    local mouse_buttons = {
        [glfw.GLFW.MOUSE_BUTTON_1] = 0,
        [glfw.GLFW.MOUSE_BUTTON_2] = 1,
        [glfw.GLFW.MOUSE_BUTTON_3] = 2,
    }

    if action == 1 then -- down
        lastx, lasty = x,y
    elseif action == 0 then -- up
        if (lastx == x) and (lasty == y) then
            castRayIntoScene(x,y)
        end
    end

    -- TODO: set target matrix for camera input

    if Camera.onclick then
        Camera:onclick(mouse_buttons[button], mouse_actions[action], mods, x/win_w, y/win_h)
    end

    if action == glfw.GLFW.PRESS then
        if Scene.onmouse ~= nil then
            Scene:onmouse(x/win_w, y/win_h)
        end
    end
end

function onmousemove(window, x, y)
    if Editor then
        -- TODO: pass it through? Mouse handling for editor?
        return
    end

    if Camera.onmousemove then
        Camera:onmousemove(x/win_w, y/win_h)
    end
    if Camera.holding == glfw.GLFW.MOUSE_BUTTON_1 then
        if Scene.onmouse ~= nil then
            Scene:onmouse(x/win_w, y/win_h)
        end
    end
end

function onwheel(window,x,y)
    if jit.os == "OSX" then
        y = y * .3
    end

    if Editor then
        Editor:onwheel(x,y)
        return
    end

    if Prompt and Prompt.show then
        if Prompt:onwheel(x,y) then return end
    end

    if Scene.onwheel ~= nil then
        local ret = Scene:onwheel(x,y)
        if ret then return end
    end

    if Camera.onwheel then
        Camera:onwheel(x,y)
    end
end

function resize(window, w, h)
    win_w, win_h = w, h
    if Scene and Scene.resizeViewport then Scene:resizeViewport(w,h) end
    if PostFX then PostFX:resize_fbo(w,h) end
    fb_w, fb_h = w, h
end

function initGL()
    local status, err = pcall(function ()
        switch_to_scene(scene_modules[scene_module_idx])
    end)
    if not status then
        print(err)
    end

    if Scene and Scene.resizeViewport then Scene:resizeViewport(win_w, win_h) end
    if PostFX then PostFX:initGL(win_w, win_h) end

    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory("data/fonts")
    glfont:initGL()

    snd.setDataDirectory("data")

    if Prompt then
        Prompt:setDataDirectory("data")
        Prompt:initGL()
    end
end

function exitGL()
    if Scene and Scene.exitGL then Scene:exitGL() end
    if PostFX and PostFX.exitGL then PostFX:exitGL() end
    glfont:exitGL()
    if Prompt then Prompt:exitGL() end
end

-- This metadata is displayed after post-processing effects.
function display_scene_overlay()
    if Prompt and Prompt.show then
        Prompt:renderPrompt(m,p)
    end

    if not glfont then return end

    local showTime = 2
    local age = clock() - lastSceneChangeTime
    -- TODO a nice fade or something
    if age > showTime then return end

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)

    -- Draw scene name sliding down from window top
    -- Appearing on scene change, disappearing shortly.
    local yoff = 0
    local tin = .15
    local tout = .5
    local yslide = -250
    if age < tin then yoff = yslide * (1-age/tin) end
    if age > showTime - tout then yoff = yslide * (age-(showTime-tout)) end
    mm.glh_translate(m, 30, yoff, 0)
    -- TODO getStringWidth and center text
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    gl.glDisable(GL.GL_CULL_FACE)
    local col = {1, 1, 1}
    local name = scene_module_idx .. ") " .. scene_modules[scene_module_idx]
    glfont:render_string(m, p, col, name)

    -- Draw filter names at the bottom
    if PostFX and PostFX.get_filter_names then
        local lineh = 120

        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        --mm.glh_translate(m, 0, win_h/s - 300, 0)
        mm.glh_translate(m, 2*yoff, 0, 0)

        local filters = PostFX:get_filter_names()
        mm.glh_translate(m, 30, win_h/s - (#filters+2)*lineh, 0)
        for _,f in pairs(filters) do
            mm.glh_translate(m, 0, lineh, 0)
            glfont:render_string(m, p, col, f)
        end
    end
end

function display()
    gl.glClearColor(0.5, 0.5, 1.0, 0.0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    gl.glEnable(GL.GL_DEPTH_TEST)

    if Scene then
        local m = {}
        mm.make_identity_matrix(m)
        if Camera.getModelMatrix then m = Camera:getModelMatrix() end

        local v = Camera:getViewMatrix()
        mm.affine_inverse(v)

        local p = {}
        local aspect = win_w / win_h
        mm.glh_perspective_rh(p, 60 * math.pi/180, aspect, .004, 500)


        local status, err = pcall(function ()
            if Scene.renderEye then Scene:renderEye(m,v,p) else
            Scene:render_for_one_eye(v,p) end
        end)
        if not status then
            -- TODO: catch and print error only once here
            if not caught_error_in_draw then
                caught_error_in_draw = true
                printContextOfErrorLine(err)
                --push_error_messages_into_editor(err)
            end
        end

        if Scene.set_origin_matrix then Scene:set_origin_matrix(v) end
    end
end

function display_with_effect()
    gl.glViewport(0,0, fb_w, fb_h)
    if PostFX then PostFX:bind_fbo() end
    display()
    if PostFX then PostFX:unbind_fbo() end

    -- Apply post-processing and present
    if PostFX then
        gl.glDisable(GL.GL_DEPTH_TEST)
        gl.glViewport(0,0, win_w, win_h)
        PostFX:present(win_w, win_h)
    end

    if Editor then
        local id = {}
        mm.make_identity_matrix(id)
        local ids = {}
        local aspect = win_w / win_h
        mm.make_scale_matrix(ids,1/aspect,1,1)
        local s = .9
        mm.glh_scale(ids,s,s,s)
        mm.glh_translate(ids,.3,0,0)

        gl.glDisable(GL.GL_DEPTH_TEST)
        Editor:render_for_one_eye(ids,id)
        gl.glEnable(GL.GL_DEPTH_TEST)
    end

    display_scene_overlay()
end

function handleJoystick()
    local jsp = glfw.glfw.JoystickPresent(glfw.GLFW.JOYSTICK_1)
    if jsp > 0 then
        local intv = ffi.typeof('GLint[?]')
        local num = intv(0)
        
        local b = glfw.glfw.GetJoystickButtons(glfw.GLFW.JOYSTICK_1, num)
        local nb = num[0]
        local barr = ffi.cast('unsigned char*', b)

        local a = glfw.glfw.GetJoystickAxes(glfw.GLFW.JOYSTICK_1, num)
        local na = num[0]
        local aarr = ffi.cast('float*', a)

        local s = ''
        for i=0,nb do
            s = s..barr[i]
        end
        s = s..' '
        for i=0,na do
            s = s..aarr[i]..' '
        end
        --print(s)
    end
end

function timestep(absTime, dt)
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
    if PostFX and PostFX.timestep then PostFX:timestep(absTime, dt) end
    Camera:timestep(absTime, dt)
    handleJoystick()
end

ffi.cdef[[
typedef unsigned int GLenum;
typedef unsigned int GLuint;
typedef int GLsizei;
typedef char GLchar;
///@todo APIENTRY
typedef void (__stdcall *GLDEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
]]

local count = 0
local myCallback = ffi.cast("GLDEBUGPROC", function(source, type, id, severity, length, message, userParam)
    if severity == GL.GL_DEBUG_SEVERITY_NOTIFICATION then return end
    if severity == GL.GL_DEBUG_SEVERITY_LOW then return end
    local enum_table = {
        [tonumber(GL.GL_DEBUG_SEVERITY_HIGH)] = "SEVERITY_HIGH",
        [tonumber(GL.GL_DEBUG_SEVERITY_MEDIUM)] = "SEVERITY_MEDIUM",
        [tonumber(GL.GL_DEBUG_SEVERITY_LOW)] = "SEVERITY_LOW",
        [tonumber(GL.GL_DEBUG_SEVERITY_NOTIFICATION)] = "SEVERITY_NOTIFICATION",
        [tonumber(GL.GL_DEBUG_SOURCE_API)] = "SOURCE_API",
        [tonumber(GL.GL_DEBUG_SOURCE_WINDOW_SYSTEM)] = "SOURCE_WINDOW_SYSTEM",
        [tonumber(GL.GL_DEBUG_SOURCE_SHADER_COMPILER)] = "SOURCE_SHADER_COMPILER",
        [tonumber(GL.GL_DEBUG_SOURCE_THIRD_PARTY)] = "SOURCE_THIRD_PARTY",
        [tonumber(GL.GL_DEBUG_SOURCE_APPLICATION)] = "SOURCE_APPLICATION",
        [tonumber(GL.GL_DEBUG_SOURCE_OTHER)] = "SOURCE_OTHER",
        [tonumber(GL.GL_DEBUG_TYPE_ERROR)] = "TYPE_ERROR",
        [tonumber(GL.GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR)] = "TYPE_DEPRECATED_BEHAVIOR",
        [tonumber(GL.GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR)] = "TYPE_UNDEFINED_BEHAVIOR",
        [tonumber(GL.GL_DEBUG_TYPE_PORTABILITY)] = "TYPE_PORTABILITY",
        [tonumber(GL.GL_DEBUG_TYPE_PERFORMANCE)] = "TYPE_PERFORMANCE",
        [tonumber(GL.GL_DEBUG_TYPE_MARKER)] = "TYPE_MARKER",
        [tonumber(GL.GL_DEBUG_TYPE_PUSH_GROUP)] = "TYPE_PUSH_GROUP",
        [tonumber(GL.GL_DEBUG_TYPE_POP_GROUP)] = "TYPE_POP_GROUP",
        [tonumber(GL.GL_DEBUG_TYPE_OTHER)] = "TYPE_OTHER",
    }
    print(enum_table[source], enum_table[type], enum_table[severity], id)
    print("   "..ffi.string(message))
    print("   Stack Traceback\n   ===============")
    -- Chop off the first lines of the traceback, as it's always this function.
    local tb = debug.traceback()
    local i = string.find(tb, '\n')
    local i = string.find(tb, '\n', i+1)
    local callstack = string.sub(tb,i+1,-1)
    print(callstack)
    printContextFromCallstack(callstack)
    if DEBUG_quitonerror then os.exit(1) end
end)

function main()
    for k,v in pairs(arg) do
        if k > 0 then
            if v == '-d' then DEBUG = true end
            if v == '-D' then
                DEBUG = true
                DEBUG_quitonerror = true
            end
        end
    end

    glfw.glfw.Init()

    if ffi.os == "OSX" then
        glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MINOR, 1) -- 3 causes MacOSX to segfault
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_PROFILE, glfw.GLFW.OPENGL_CORE_PROFILE)
    else
        glfw.glfw.WindowHint(glfw.GLFW.DEPTH_BITS, 16)
        glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MINOR, 3)
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_PROFILE, glfw.GLFW.OPENGL_ANY_PROFILE)
    end
    glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MAJOR, 4)
    glfw.glfw.WindowHint(glfw.GLFW.OPENGL_FORWARD_COMPAT, 1)
    --glfw.glfw.WindowHint(glfw.GLFW.CLIENT_API, glfw.GLFW.OPENGL_ES_API)

    if DEBUG then
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_DEBUG_CONTEXT, GL.GL_TRUE)
    end

    window = glfw.glfw.CreateWindow(win_w,win_h,"Luajit",nil,nil)
    local int_buffer = ffi.new("int[2]")
    -- Get actual framebuffer size for oversampled ("Retina") displays
    glfw.glfw.GetFramebufferSize(window, int_buffer, int_buffer+1)
    fb_w = int_buffer[0]
    fb_h = int_buffer[1]
    glfw.glfw.SetKeyCallback(window, onkey)
    glfw.glfw.SetCharCallback(window, onchar);
    glfw.glfw.SetMouseButtonCallback(window, onmousebutton)
    glfw.glfw.SetCursorPosCallback(window, onmousemove)
    glfw.glfw.SetScrollCallback(window, onwheel)
    glfw.glfw.SetWindowSizeCallback(window, resize)
    glfw.glfw.MakeContextCurrent(window)
    glfw.glfw.SwapInterval(0)
    if DEBUG then
        gl.glDebugMessageCallback(myCallback, nil)
        gl.glDebugMessageControl(GL.GL_DONT_CARE, GL.GL_DONT_CARE, GL.GL_DONT_CARE, 0, nil, GL.GL_TRUE)
        gl.glDebugMessageInsert(GL.GL_DEBUG_SOURCE_APPLICATION, GL.GL_DEBUG_TYPE_MARKER, 0,
            GL.GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging")
        gl.glEnable(GL.GL_DEBUG_OUTPUT_SYNCHRONOUS);
    end

    local windowTitle = "OpenGL with Luajit"
    local firstFrameTime = clock()
    local lastFrameTime = clock()
    initGL()
    while glfw.glfw.WindowShouldClose(window) == 0 do
        glfw.glfw.PollEvents()
        display_with_effect()

        local now = clock()
        g_ft:onFrame(now)
        timestep(now - firstFrameTime, now - lastFrameTime)
        lastFrameTime = now

        local fpstitle = (ffi.os == "Windows" or ffi.os == "OSX")
        fpstitle = true
        if fpstitle then
            glfw.glfw.SetWindowTitle(window, windowTitle.." "..math.floor(g_ft:getFPS()).." fps")
        end
        if socket then socket.sleep(.001) end

        glfw.glfw.SwapBuffers(window)
    end
    exitGL()
end

main()
