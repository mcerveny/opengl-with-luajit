-- sdl_simple.lua

print(jit.version, jit.os, jit.arch)

package.path = package.path .. ';lib/?.lua'

local ffi = require "ffi"
local SDL = require "lib/sdl"
local libretro_input = require 'util.libretro_input'

local openGL = require "opengl"
openGL.loader = SDL.SDL_GL_GetProcAddress
openGL:import()

local socket = nil
if (ffi.os == "Windows") then
    package.cpath = package.cpath .. ';bin/windows/socket/core.dll'
    package.loadlib("socket/core.dll", "luaopen_socket_core")
    socket = require 'socket.core'
elseif (ffi.os == "Linux") then
    package.cpath = package.cpath .. ';bin/linux/'..jit.arch..'/socket/?.so'
    socket = require 'socket.core'
elseif (ffi.os == "OSX") then
    package.cpath = package.cpath .. ';bin/osx/socket/?.so'
    socket = require 'socket.core'
end

local clock = os.clock
if socket then
    print("Using socket.gettime in Luasocket version "..socket._VERSION)
    clock = socket.gettime
end

local lr_input = require 'util.libretro_input'
local fpstimer = require("util.fpstimer")
local g_ft = fpstimer.create()

local window
local win_w = 800
local win_h = 600
local pJoysticks = {}

local scenedir = "scene_es2"
local Scene
function switch_to_scene(name)
    local fullname = scenedir.."."..name
    if Scene and Scene.exitGL then
        Scene:exitGL()
    end
    -- Do we need to unload the module?
    package.loaded[fullname] = nil
    Scene = nil

    local status,err = pcall(function ()
        local SceneLibrary = require(fullname)
        Scene = SceneLibrary.new()
    end)
    if not status then
        print(err)
    end
    if Scene then
        if Scene.setDataDirectory then Scene:setDataDirectory("data") end
        if Scene.resizeViewport then Scene:resizeViewport(win_w,win_h) end
        Scene:initGL()
    end
end

local cur_keys = {}
for i=1,255 do cur_keys[i] = 0 end
function onkey(key, scancode, action, mods)
    if false then
        if Scene and Scene.keypressed then
            local consumed = Scene:keypressed(key, scancode, action, mods)
            if consumed then return end
        end
    else
        -- Simulate retropad inputs with keys
        cur_keys[key] = action
    end
end

-- Handle connection/disconnection during play by just calling this every frame.
function initJoystick()
    pJoysticks = {}
    local nj = SDL.SDL_NumJoysticks()
    for i=0,nj-1 do
        local jname = SDL.SDL_JoystickNameForIndex(i)
        local jp = SDL.SDL_JoystickOpen(i)
        local nb = SDL.SDL_JoystickNumButtons(jp)
        local na = SDL.SDL_JoystickNumAxes(jp)
        --print(i,'['..ffi.string(jname)..']',nb..' buttons',na..' axes')
        table.insert(pJoysticks, jp)
    end
end


local last_states = {}
-- Initial joystick states
-- TODO: dynamically allocate
for j=1,8 do
    local last_state = {}
    -- TODO: dynamically allocate button count
    for i=1,14 do last_state[i]=0 end
    table.insert(last_states, last_state)
end
function update_retropad(joystates) -- TODO add last_states parameter
    for j=1,#joystates do
        local js = joystates[j]
        if js then
            for b=1,14 do
                if last_states[j][b] == 0 and js[b] == 1 then
                    on_button_pressed(b)
                end
                last_states[j][b] = js[b]
            end
        end
    end
end

-- Handle button presses here too.
-- TODO: is there a slicker way to do this? Seems redundant.
function on_button_pressed(i)
    if Scene and Scene.getSelectedSceneName then
        local name = Scene:getSelectedSceneName()
        if i == lr_input.JOYPAD_A then
            switch_to_scene(name)
        end
    end
    if Scene then
        if i == lr_input.JOYPAD_START then
            -- TODO: isButtonPressed function for a given pad
            switch_to_scene('scene_selector')
        end
    end
end

-- Check here for button pressed state to initiate changes(load new scene)
-- TODO: move this out into another layer? Scene changer?
function updateRetropad(joyStates)
    update_retropad(joyStates)
    if Scene and Scene.update_retropad then Scene:update_retropad(joyStates) end
end

function handleJoystick()
    if #pJoysticks > 0 then
        local joyStates = {}
        for j=1,#pJoysticks do
            -- TODO: cache
            local pJoystick = pJoysticks[j]
            local status = tostring(j)..' '..tostring(pJoystick)..' '
            local axes = {}
            local buttons = {}

            local nb = SDL.SDL_JoystickNumButtons(pJoystick)
            for i=0,nb-1 do
                local bi = SDL.SDL_JoystickGetButton(pJoystick,i)
                status = status..bi
                table.insert(buttons, bi)
            end
            local na = SDL.SDL_JoystickNumAxes(pJoystick)
            for i=0,na-1 do
                local ai = SDL.SDL_JoystickGetAxis(pJoystick,i)
                status = status..' '..ai
                table.insert(axes, ai)
            end
            local name = ffi.string(SDL.SDL_JoystickName(pJoystick))
            local rp = libretro_input.getRetropadStateFromSDL(name, axes, buttons)
            table.insert(joyStates,rp)
            --print(status)
        end
        updateRetropad(joyStates)
    else
        -- Emulate joypad with keyboard
        local rp = {}
        for i=1,14 do rp[i] = 0 end
        local map = libretro_input.mappings['keyboard']
        for i=65,255 do
            local bi = string.char(i)
            local k = map[bi]
            if k and cur_keys[i] == 1 then rp[k] = 1 end
        end
        local joyStates = {rp}
        updateRetropad(joyStates)
    end
end

function initGL()
end

function display()
    local status, err = pcall(function () Scene:render() end)
    if not status then print(err) end
end

function timestep(absTime, dt)
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
    initJoystick()
    handleJoystick()
end

function main()
    local fullscreen = false
    for k,v in pairs(arg) do
        if k > 0 then
            if v == '-f' then fullscreen = true end
        end
    end

    local linkedver = ffi.new("SDL_version[1]")
    SDL.SDL_GetVersion(linkedver)
    local revnum = SDL.SDL_GetRevisionNumber()
    local revstr = ffi.string(SDL.SDL_GetRevision())
    local v = linkedver[0]
    local vstr = v.major..'.'..v.minor..'.'..v.patch..' rev '..revstr
    print('SDL version '..vstr)

    SDL.SDL_Init(SDL.SDL_INIT_VIDEO + SDL.SDL_INIT_JOYSTICK)

    if fullscreen then
        local current = ffi.new("SDL_DisplayMode[1]")
        local should_be_zero = SDL.SDL_GetCurrentDisplayMode(0, current)
        if should_be_zero ~= 0 then
            print('error: ',should_be_zero)
        end
        local function printMode(str, dispmode)
            local mode = dispmode[0]
            if not mode then return end
            print(str,mode.w..'x'..mode.h..'@'..mode.refresh_rate..' Hz')
        end
        printMode('Current: ', current)
        should_be_zero = SDL.SDL_GetDesktopDisplayMode(0, current)
        if should_be_zero ~= 0 then print('error: ',should_be_zero) end
        printMode('Desktop: ', current)
        if current then
            local mode = current[0]
            if mode then
                win_w = mode.w
                win_h = mode.h
                print("Video mode: "..win_w.."x"..win_h)
            end
        end
    end

    local flags = 
        ffi.C.SDL_WINDOW_OPENGL +
        ffi.C.SDL_WINDOW_SHOWN
    if fullscreen then
        flags = flags + ffi.C.SDL_WINDOW_FULLSCREEN
        SDL.SDL_ShowCursor(0)
    else
        flags = flags + ffi.C.SDL_WINDOW_RESIZABLE
    end
    window = SDL.SDL_CreateWindow(
        "sdl_simple",
        SDL.SDL_WINDOWPOS_UNDEFINED,
        SDL.SDL_WINDOWPOS_UNDEFINED,
        win_w,
        win_h,
        flags
    )

    if window == nil then
        print("Could not create window: " .. ffi.string( SDL.SDL_GetError() ))
        os.exit(1)
    end

    glContext = SDL.SDL_GL_CreateContext(window);
    if glContext == nil then
        print("There was an error creating the OpenGL context!\n")
        return 0
    end

    SDL.SDL_GL_MakeCurrent(window, glContext)
    initJoystick()

    switch_to_scene('scene_selector')
    --if Scene and Scene.resizeViewport then Scene:resizeViewport(win_w, win_h) end

    print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
    print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
    print(ffi.string(gl.glGetString(GL.GL_VERSION)))
    print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))

    local firstFrameTime = clock()
    local lastFrameTime = clock()
    local quit = false
    while not quit do
        local event = ffi.new("SDL_Event")
        while SDL.SDL_PollEvent(event) ~= 0 do
            if event.type == SDL.SDL_QUIT then
                quit = true
            elseif event.type == SDL.SDL_KEYDOWN then
                if event.key.keysym.sym == SDL.SDLK_ESCAPE then
                    quit = true
                end
                onkey(event.key.keysym.sym, 0, 1, 0)
            elseif event.type == SDL.SDL_KEYUP then
                onkey(event.key.keysym.sym, 0, 0, 0)
            end
        end

        display()
        g_ft:onFrame()
        Scene.fps = math.floor(g_ft:getFPS()) -- Just jam that in there

        local now = clock()
        timestep(now - firstFrameTime, now - lastFrameTime)
        lastFrameTime = now
        if socket then socket.sleep(.01) end

        SDL.SDL_GL_SwapWindow(window)
    end

    SDL.SDL_DestroyWindow(window)
    SDL.SDL_Quit()
end
main()
