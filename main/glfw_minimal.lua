package.path=package.path..';lib/?.lua'
local glfw=require'glfw'
local openGL=require'opengl'
openGL.loader=glfw.glfw.GetProcAddress
openGL:import()

glfw.glfw.Init()
local w = glfw.glfw.CreateWindow(800,600,"GL",nil,nil)
glfw.glfw.MakeContextCurrent(w)

local ffi = require "ffi"
print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
print(ffi.string(gl.glGetString(GL.GL_VERSION)))
print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))

local frames = 0
local startTime = os.clock()
while glfw.glfw.WindowShouldClose(w) == 0 do
    glfw.glfw.PollEvents()
    local t = 100*os.clock()
    local function f(t) return .5+.5*math.sin(t) end
    gl.glClearColor(f(t), f(1.3*t), f(1.7*t), 0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    if glfw.glfw.GetKey(w, 256) == 1 then
         -- glfw.GLFW.KEY_ESCAPE
        glfw.glfw.SetWindowShouldClose(w, 1)
    end
    frames = frames + 1
    glfw.glfw.SwapBuffers(w)
end
local endTime = os.clock()
print(math.floor(frames/(endTime-startTime)).." fps")
