-- sdl_main.lua

print(jit.version, jit.os, jit.arch)

package.path = package.path .. ';lib/?.lua'

local ffi = require "ffi"
local SDL = require "lib/sdl"

local openGL = require "opengl"
openGL.loader = SDL.SDL_GL_GetProcAddress
openGL:import()

local socket = nil
if (ffi.os == "Windows") then
    package.cpath = package.cpath .. ';bin/windows/socket/core.dll'
    package.loadlib("socket/core.dll", "luaopen_socket_core")
    socket = require 'socket.core'
elseif (ffi.os == "Linux") then
    package.cpath = package.cpath .. ';bin/linux/'..jit.arch..'/socket/?.so'
    socket = require 'socket.core'
elseif (ffi.os == "OSX") then
    package.cpath = package.cpath .. ';bin/osx/socket/?.so'
    socket = require 'socket.core'
end

local clock = os.clock
if socket then
    print("Using socket.gettime in Luasocket version "..socket._VERSION)
    clock = socket.gettime
end

require("util.glfont")
local CameraLibrary = require("util.camera")
local Camera = CameraLibrary.new()
local mm = require("util.matrixmath")
local Scene = nil
local glfont = nil

local window
local win_w = 800
local win_h = 600

local scenedir = "scene"

local pJoystick

function switch_to_scene(name)
    local fullname = scenedir.."."..name
    if Scene and Scene.exitGL then
        Scene:exitGL()
    end
    -- Do we need to unload the module?
    package.loaded[fullname] = nil
    Scene = nil

    if not Scene then
        local SceneLibrary = require(fullname)
        Scene = SceneLibrary.new()
        if Scene then
            local now = clock()
            if Scene.setDataDirectory then Scene:setDataDirectory("data") end
            Scene:initGL()
            local initTime = clock() - now
            lastSceneChangeTime = now
            collectgarbage()
            print(name,
                "init time: "..math.floor(1000*initTime).." ms",
                "memory: "..math.floor(collectgarbage("count")).." kB")
        end
    end
end

local scene_modules = {
--    "gamecontrol",
    "touch_shader2",
    "vsfstri",
    "clockface",
    "key_check",
    "shadertoy_editor",
    "shadertoy",
    "colorcube",
    "list_of_scenes",
    "objscene",
    "cone",
    "ifs",
    "red_sunset",
    "wavy_sheet",
    "multipass_example",
    "droid",
    "image_fragments",
    "ThiSpawns_sunsets",
    "TDMs_seascape",
    "a_sunset",
    "font_test",
    "eyetest",
    "julia_set",
    "hybrid_scene",
    "cubemap",
    "moon",
    "nbody07",
    "molecule",
    "simple_game",
}

local scene_module_idx = 1
function switch_scene(reverse)
    if reverse then
        scene_module_idx = scene_module_idx - 1
        if scene_module_idx < 1 then scene_module_idx = #scene_modules end
    else
        scene_module_idx = scene_module_idx + 1
        if scene_module_idx > #scene_modules then scene_module_idx = 1 end
    end
    switch_to_scene(scene_modules[scene_module_idx])
end

local lctrl_held = false
local lshift_held = false
function onkeydown(key, scancode, action, mods)
    if key == SDL.SDLK_LCTRL then
        lctrl_held = true
    end

    if key == SDL.SDLK_LSHIFT then
        lshift_held = true
    end

    if Scene.keypressed then
        local consumed = Scene:keypressed(key, scancode, action, mods)
        if consumed then return end
    end

    if Scene.charkeypressed then
        if scancode > 32 and scancode < string.byte('z') then
            Scene:charkeypressed(string.char(scancode))
        end
    end

    if lctrl_held then
        if key == SDL.SDLK_TAB then
            switch_scene(lshift_held)
        end
    end
    Camera:onkey(key, scancode, action, mods)
end

function onkeyup(key, scancode, action, mods)
    if key == SDL.SDLK_LCTRL then
        lctrl_held = false
    end
    if key == SDL.SDLK_LSHIFT then
        lshift_held = false
    end
    
    if Scene.keyreleased then
        Scene:keyreleased(key, scancode, action, mods)
    end

    Camera:onkey(key, scancode, action, mods)
end

function onclick(window, button, state, x, y)
    --[[
    #define SDL_BUTTON_LEFT     1
    #define SDL_BUTTON_MIDDLE   2
    #define SDL_BUTTON_RIGHT    3
    #define SDL_BUTTON_WHEELUP  4
    #define SDL_BUTTON_WHEELDOWN    5
    ]]
    local mouse_buttons = {
        [1] = 0,
        [2] = 2,
        [3] = 1,
    }
    Camera:onclick(mouse_buttons[button], state, 0, x, y)
end

function onmousemove(window, x, y)
    Camera:onmousemove(x,y)
    --if Camera.holding ~= 0 then
        if Scene.onmouse ~= nil then
            Scene:onmouse(x/win_w, y/win_h)
        end
    --end
end

function onwheel(window,x,y)
    Camera:onwheel(x,y)
end

function resize(window, w, h)
    win_w, win_h = w, h
    if Scene and Scene.resizeViewport then Scene:resizeViewport(w,h) end
    if PostFX then PostFX:resize_fbo(w,h) end
    fb_w, fb_h = w, h
end

function initGL()
    local status, err = pcall(function ()
        switch_to_scene(scene_modules[scene_module_idx])
    end)
    if not status then
        print(err)
    end

    if Scene and Scene.resizeViewport then Scene:resizeViewport(win_w, win_h) end
    if PostFX then PostFX:initGL(win_w, win_h) end

    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory("data/fonts")
    glfont:initGL()

end

function exitGL()
    if Scene and Scene.exitGL then Scene:exitGL() end
    if PostFX and PostFX.exitGL then PostFX:exitGL() end
    glfont:exitGL()
end

function display_scene_overlay()
    if not glfont then return end

    local showTime = 2
    local age = clock() - lastSceneChangeTime
    -- TODO a nice fade or something
    if age > showTime then return end

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)

    -- Draw scene name sliding down from window top
    -- Appearing on scene change, disappearing shortly.
    local yoff = 0
    local tin = .15
    local tout = .5
    local yslide = -250
    if age < tin then yoff = yslide * (1-age/tin) end
    if age > showTime - tout then yoff = yslide * (age-(showTime-tout)) end
    mm.glh_translate(m, 30, yoff, 0)
    -- TODO getStringWidth and center text
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    local name = scene_module_idx .. ") " .. scene_modules[scene_module_idx]
    glfont:render_string(m, p, col, name)

    -- Draw filter names at the bottom
    if PostFX and PostFX.get_filters then
        local filters = PostFX:get_filters()
        local lineh = 120

        mm.make_identity_matrix(m)
        local s = .5
        mm.glh_scale(m, s, s, s)
        --mm.glh_translate(m, 0, win_h/s - 300, 0)
        mm.glh_translate(m, 30, win_h/s - (#filters+2)*lineh, 0)
        for _,f in pairs(filters) do
            mm.glh_translate(m, 0, lineh, 0)
            glfont:render_string(m, p, col, f.name)
        end
    end
end

function display()
    gl.glClearColor(0.5, 0.5, 1.0, 0.0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    gl.glEnable(GL.GL_DEPTH_TEST)

    if Scene then
        local v = Camera:getViewMatrix()
        mm.affine_inverse(v)
        local p = {}
        local aspect = win_w / win_h
        mm.glh_perspective_rh(p, 90, aspect, .004, 500)

        local status, err = pcall(function ()
            Scene:render_for_one_eye(v,p)
        end)
        if not status then
            -- TODO: catch and print error only once here
            if not caught_error_in_draw then
                caught_error_in_draw = true
                print(err)
                push_error_messages_into_editor(err)
            end
        end

        if Scene.set_origin_matrix then Scene:set_origin_matrix(v) end
    end

    display_scene_overlay()
end

function handleJoystick()
    if pJoystick then
        -- TODO: cache
        local status = ''
        local axes = {}
        local buttons = {}

        local nb = SDL.SDL_JoystickNumButtons(pJoystick)
        for i=0,nb-1 do
            local bi = SDL.SDL_JoystickGetButton(pJoystick,i)
            status = status..bi
            table.insert(buttons, bi)
        end
        local na = SDL.SDL_JoystickNumAxes(pJoystick)
        for i=0,na-1 do
            local ai = SDL.SDL_JoystickGetAxis(pJoystick,i)
            status = status..' '..ai
            table.insert(axes, ai)
        end

        --print(status)
        if Scene and Scene.updateJoystick then Scene:updateJoystick(axes, buttons) end
    end
end

function timestep(absTime, dt)
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
    if PostFX and PostFX.timestep then PostFX:timestep(absTime, dt) end
    Camera:timestep(absTime, dt)
    handleJoystick()
end

function initJoystick()
    local nj = SDL.SDL_NumJoysticks()

    for i=0,nj-1 do
        local jname = SDL.SDL_JoystickNameForIndex(i)
        print(i,ffi.string(jname))

        local jp = SDL.SDL_JoystickOpen(0)
        print(jp)

        local nb = SDL.SDL_JoystickNumButtons(jp)
        local na = SDL.SDL_JoystickNumAxes(jp)
        print(nb,na)
        if i==0 then pJoystick = jp end
    end

end

function main()
    SDL.SDL_Init(SDL.SDL_INIT_VIDEO + SDL.SDL_INIT_JOYSTICK)

    window = SDL.SDL_CreateWindow(
        "An SDL2 window",
        SDL.SDL_WINDOWPOS_UNDEFINED,
        SDL.SDL_WINDOWPOS_UNDEFINED,
        win_w,
        win_h,
        ffi.C.SDL_WINDOW_OPENGL +
        ffi.C.SDL_WINDOW_SHOWN +
        ffi.C.SDL_WINDOW_RESIZABLE
    )

    if window == nil then
        print("Could not create window: " .. ffi.string( SDL.SDL_GetError() ))
        os.exit(1)
    end

    glContext = SDL.SDL_GL_CreateContext(window);
    if glContext == nil then
        print("There was an error creating the OpenGL context!\n")
        return 0
    end

    SDL.SDL_GL_MakeCurrent(window, glContext)

    print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
    print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
    print(ffi.string(gl.glGetString(GL.GL_VERSION)))
    print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))

    initJoystick()
    initGL()
    
    local firstFrameTime = clock()
    local lastFrameTime = clock()
    local quit = false
    while not quit do
        local event = ffi.new("SDL_Event")
        while SDL.SDL_PollEvent(event) ~= 0 do
            if event.type == SDL.SDL_KEYDOWN then
                if event.key.keysym.sym == SDL.SDLK_ESCAPE then
                    quit = true
                end
                onkeydown(event.key.keysym.sym, event.text.text[8], event.key.state, event.key.keysym.mod)
            elseif event.type == SDL.SDL_KEYUP then
                onkeyup(event.key.keysym.sym, event.text.text[8], event.key.state, event.key.keysym.mod)
            elseif event.type == SDL.SDL_WINDOWEVENT then
                if event.window.event == SDL.SDL_WINDOWEVENT_RESIZED then
                    win_w = event.window.data1
                    win_h = event.window.data2
                    resize(window, win_w, win_h)
                end
            elseif event.type == SDL.SDL_MOUSEBUTTONDOWN then
                onclick(window, event.button.button, event.button.state, event.button.x, event.button.y)
            elseif event.type == SDL.SDL_MOUSEBUTTONUP then
                onclick(window, event.button.button, event.button.state, event.button.x, event.button.y)
            elseif event.type == SDL.SDL_MOUSEMOTION then
                onmousemove(window, event.motion.x, event.motion.y)
            elseif event.type == SDL.SDL_MOUSEWHEEL then
                onwheel(window, event.wheel.x, event.wheel.y)
            elseif event.type == SDL.SDL_QUIT then
                quit = true
            end
        end

        local now = clock()
        timestep(now - firstFrameTime, now - lastFrameTime)
        lastFrameTime = now

        display()
        SDL.SDL_GL_SwapWindow(window)
    end

    exitGL()
    SDL.SDL_DestroyWindow(window)
    SDL.SDL_Quit()
end
main()
