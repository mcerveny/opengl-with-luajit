-- sdl_shader.lua
-- Entry point for a simple SDL OpenGL app
-- For Raspberry Pi 3 B:
--     `sudo apt-get install libsdl2-dev`

-- xrandr --output DVI-I-1  --mode 1920x1080

print(jit.version, jit.os, jit.arch)

package.path = package.path .. ';lib/?.lua'

local ffi = require "ffi"
local SDL = require "lib/sdl"

local openGL = require "opengl"
openGL.loader = SDL.SDL_GL_GetProcAddress
openGL:import()
local sf = require("util.shaderfunctions")

local window
local win_w = 1680
local win_h = 1050

local vbos = {}
local prog = 0

local basic_vert = [[
#version 100

attribute vec2 vPosition;
attribute vec2 vColor;

varying vec3 vfColor;

void main()
{
    vfColor = vec3(vColor, 0.);
    gl_Position = vec4(vPosition, 0., 1.);
}
]]

local basic_frag = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform float uBlue;
varying vec3 vfColor;

void main()
{
    vec3 col = .5*vfColor + vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col, 1.0);
}
]]

local function init_quad_attributes()
    local glIntv   = ffi.typeof('GLint[?]')
    local glUintv  = ffi.typeof('GLuint[?]')
    local glFloatv = ffi.typeof('GLfloat[?]')

    local verts = glFloatv(4*2, {
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        })

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local quads = glUintv(6*2, {
        0,1,2,
        0,2,3,
    })
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(vbos, qvbo)
end

function display()
    gl.glViewport(0,0, win_w, win_h)
    gl.glClearColor(0.5, 0.5, 1.0, 0.0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    gl.glUseProgram(prog)
    local u_loc = gl.glGetUniformLocation(prog, "uBlue")
    local ph = .5+.5*math.sin(300*os.clock())
    gl.glUniform1f(u_loc, ph)
    gl.glDrawElements(GL.GL_TRIANGLES, 6, GL.GL_UNSIGNED_INT, nil)
    gl.glUseProgram(0)
end

function main()
    SDL.SDL_Init(SDL.SDL_INIT_VIDEO)
    SDL.SDL_ShowCursor(0)

    local current = ffi.new("SDL_DisplayMode[1]")
    local should_be_zero = SDL.SDL_GetCurrentDisplayMode(0, current)
    if should_be_zero ~= 0 then
        print('error: ',should_be_zero)
    end
    local function printMode(str, dispmode)
        local mode = dispmode[0]
        if not mode then return end
        print(str,mode.w..'x'..mode.h..'@'..mode.refresh_rate..' Hz')
    end
    printMode('Current: ', current)
    should_be_zero = SDL.SDL_GetDesktopDisplayMode(0, current)
    if should_be_zero ~= 0 then print('error: ',should_be_zero) end
    printMode('Desktop: ', current)
    if current then
        local mode = current[0]
        if mode then
            win_w = mode.w
            win_h = mode.h
        end
    end

    window = SDL.SDL_CreateWindow(
        "sdl_shader.lua",
        SDL.SDL_WINDOWPOS_UNDEFINED,
        SDL.SDL_WINDOWPOS_UNDEFINED,
        win_w,
        win_h,
        0
          + ffi.C.SDL_WINDOW_FULLSCREEN
          + ffi.C.SDL_WINDOW_OPENGL
          + ffi.C.SDL_WINDOW_SHOWN
          --+ ffi.C.SDL_WINDOW_RESIZABLE
    )

    should_be_zero = SDL.SDL_GetWindowDisplayMode(window, current)
    if should_be_zero ~= 0 then print('error: ',should_be_zero) end
    printMode('Window: ', current)

    if window == nil then
        print("Could not create window: " .. ffi.string( SDL.SDL_GetError() ))
        os.exit(1)
    end

    glContext = SDL.SDL_GL_CreateContext(window);
    if glContext == nil then
        print("There was an error creating the OpenGL context!\n")
        return 0
    end

    SDL.SDL_GL_MakeCurrent(window, glContext)

    print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
    print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
    print(ffi.string(gl.glGetString(GL.GL_VERSION)))
    print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))

    prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    init_quad_attributes()
    
    local frames = 0
    local startTime = os.clock()
    local quit = false
    while not quit do
        local event = ffi.new("SDL_Event")
        while SDL.SDL_PollEvent(event) ~= 0 do
            if event.type == SDL.SDL_KEYDOWN then
                if event.key.keysym.sym == SDL.SDLK_ESCAPE then
                    quit = true
                end
                print(event.key.keysym.sym)
            elseif event.type == SDL.SDL_WINDOWEVENT then
                if event.window.event == SDL.SDL_WINDOWEVENT_RESIZED then
                    --win_w = event.window.data1
                    --win_h = event.window.data2
                end
            elseif event.type == SDL.SDL_QUIT then
                quit = true
            end
        end

        display()
        SDL.SDL_GL_SwapWindow(window)
        frames = frames + 1
    end

    local endTime = os.clock()
    print(math.floor(frames/(endTime-startTime)).." fps")

    SDL.SDL_DestroyWindow(window)
    SDL.SDL_Quit()
end
main()
