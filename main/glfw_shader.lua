package.path=package.path..';lib/?.lua'
glfw=require"glfw"
GL=require"opengl"
GL.loader=glfw.glfw.GetProcAddress
GL:import()
sf=require"util.shaderfunctions"
ffi=require"ffi"

local snd = require("util.soundfx")
snd.setDataDirectory("data")
function onkey(window, key, scancode, action, mods)
    if action==1 then snd.playSound("pop_drip.wav") end
end

vs=[[#version 120
attribute vec2 vP;
varying vec3 vfC;
void main()
{
    vfC=vec3(vP,0.);
    gl_Position=vec4(vP,0.,1.);
}
]]
fs=[[#version 120
varying vec3 vfC;
void main()
{
    vec3 col = .5*vfC+vec3(.5);
    gl_FragColor = vec4(col,1.);
}
]]

glfw.glfw.Init()
w=glfw.glfw.CreateWindow(800,600,"GL",nil,nil)
glfw.glfw.MakeContextCurrent(w)

glfw.glfw.SetKeyCallback(w, onkey)

p=sf.make_shader_from_source({vsrc=vs,fsrc=fs})
v=ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
vl=gl.glGetAttribLocation(p,"vP")
vvbo=ffi.typeof('GLint[?]')(0)
gl.glGenBuffers(1,vvbo)
gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
gl.glEnableVertexAttribArray(vl)
gl.glUseProgram(p)
local frames = 0
local startTime = os.clock()
while glfw.glfw.WindowShouldClose(w)==0 do
    if glfw.glfw.GetKey(w, 256) == 1 then glfw.glfw.SetWindowShouldClose(w, 1) end
    glfw.glfw.PollEvents()
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    glfw.glfw.SwapBuffers(w)
    frames = frames + 1
end
local endTime = os.clock()
print(math.floor(frames/(endTime-startTime)).." fps")