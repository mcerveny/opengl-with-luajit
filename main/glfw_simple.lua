-- glfw_simple.lua

package.path = package.path..';lib/?.lua'
local ffi = require("ffi")
local glfw = require "glfw"
GL = require "opengl"
GL.loader = glfw.glfw.GetProcAddress
GL:import()

local socket = nil
if (ffi.os == "Windows") then
    package.cpath = package.cpath .. ';bin/windows/socket/core.dll'
    package.loadlib("socket/core.dll", "luaopen_socket_core")
    socket = require 'socket.core'
elseif (ffi.os == "Linux") then
    package.cpath = package.cpath .. ';bin/linux/'..jit.arch..'/socket/?.so'
    socket = require 'socket.core'
elseif (ffi.os == "OSX") then
    package.cpath = package.cpath .. ';bin/osx/socket/?.so'
    socket = require 'socket.core'
end

local clock = os.clock
if socket then
    print("Using socket.gettime in Luasocket version "..socket._VERSION)
    clock = socket.gettime
end

local scenedir = "scene"
local Scene
function switch_to_scene(name)
    local fullname = scenedir.."."..name
    local status,err = pcall(function ()
        local SceneLibrary = require(fullname)
        Scene = SceneLibrary.new()
    end)
    if not status then
        print(err)
    end
    if Scene then
        if Scene.setDataDirectory then Scene:setDataDirectory("data") end
        if Scene.resizeViewport then Scene:resizeViewport(win_w,win_h) end
        Scene:initGL()
    end
end

function onkey(window, key, scancode, action, mods)
    if Scene and Scene.keypressed then
        local consumed = Scene:keypressed(key, scancode, action, mods)
        if consumed then return end
    end
end

function display()
    local status, err = pcall(function ()
        if Scene.renderEye then Scene:renderEye(m,v,p) else
        Scene:render_for_one_eye(v,p) end
    end)
    if not status then print(err) end
end

function timestep(absTime, dt)
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
    --handleJoystick()
end

glfw.glfw.Init()
w = glfw.glfw.CreateWindow(800,600,"glfw_simple",nil,nil)
glfw.glfw.MakeContextCurrent(w)
glfw.glfw.SetKeyCallback(w, onkey)
switch_to_scene('simple_shader')

print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
print(ffi.string(gl.glGetString(GL.GL_VERSION)))
print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))

local firstFrameTime = clock()
local lastFrameTime = clock()
while glfw.glfw.WindowShouldClose(w)==0 do
    if glfw.glfw.GetKey(w, 256) == 1 then glfw.glfw.SetWindowShouldClose(w, 1) end
    glfw.glfw.PollEvents()
    display()

    local now = clock()
    timestep(now - firstFrameTime, now - lastFrameTime)
    lastFrameTime = now

    if socket then socket.sleep(.01) end

    glfw.glfw.SwapBuffers(w)
end
