-- glfw_shader2.lua
-- Aiming for minimal bytecode. Export with:
-- ./bin/linux/luajit -b main/glfw_shader2.lua glfwsh.out
package.path=package.path..';lib/?.lua'
glfw=require"glfw"
GL=require"opengl"
GL.loader=glfw.glfw.GetProcAddress
GL:import()
sf=require"util.shaderfunctions2"
ffi=require"ffi"

local fpstimer = require("util.fpstimer")
local g_ft = fpstimer.create()

local vertsrc = [[
#version 120
attribute vec2 vP;
varying vec3 vfColor;

void main()
{
    vfColor=vec3(vP,0.);
    gl_Position=vec4(vP,0.,1.);
}
]]

local fragsrc = [[
#version 120
uniform float uBlue;
varying vec3 vfColor;

void main()
{
    vec3 col = .5*vfColor + vec3(.5);
    col.b = uBlue;
    gl_FragColor = vec4(col, 1.0);
}
]]

glfw.glfw.Init()

local primary = glfw.glfw.GetPrimaryMonitor()
local mode = glfw.glfw.GetVideoMode(primary)
print(mode.width..'x'..mode.height..'@'..mode.refreshRate..'Hz '
    ..mode.redBits..mode.greenBits..mode.blueBits)
--window = glfw.glfw.CreateWindow(800,600,"glfw_shader2.lua",nil,nil)
window = glfw.glfw.CreateWindow(
    mode.width, mode.height,
    "glfw_shader2.lua",
    glfw.glfw.GetPrimaryMonitor(),
    nil)
glfw.glfw.SetInputMode(window, glfw.GLFW.CURSOR, glfw.GLFW.CURSOR_DISABLED)
glfw.glfw.MakeContextCurrent(window)

p = sf.make_shader_from_source({vsrc=vertsrc,fsrc=fragsrc})
v = ffi.typeof('GLfloat[?]')(12,{-1,-1,1,-1,1,1,-1,-1,1,1,-1,1,})
vl = gl.glGetAttribLocation(p,"vP")
vvbo = ffi.typeof('GLint[?]')(0)
gl.glGenBuffers(1,vvbo)
gl.glBindBuffer(GL.GL_ARRAY_BUFFER,vvbo[0])
gl.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(v),v,GL.GL_STATIC_DRAW)
gl.glVertexAttribPointer(vl,2,GL.GL_FLOAT,GL.GL_FALSE,0,nil)
gl.glEnableVertexAttribArray(vl)
gl.glUseProgram(p)

while glfw.glfw.WindowShouldClose(window) == 0 do
    g_ft:onFrame()
    if glfw.glfw.GetKey(window, 256) == 1 then
         -- glfw.GLFW.KEY_ESCAPE
        glfw.glfw.SetWindowShouldClose(window, 1)
    end
    glfw.glfw.PollEvents()
    local u_loc = gl.glGetUniformLocation(p, "uBlue")
    local ph = .5+.5*math.sin(300*os.clock())
    gl.glUniform1f(u_loc, ph)
    gl.glDrawArrays(GL.GL_TRIANGLES,0,6)
    glfw.glfw.SwapBuffers(window)
end
print(math.floor(g_ft:getFPS()).." fps")
