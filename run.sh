#!/bin/bash

# http://stackoverflow.com/questions/394230/detect-the-os-from-a-bash-script
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    os='linux'
elif [[ "$OSTYPE" == "linux-gnueabihf" ]]; then
    os='linux'
elif [[ "$OSTYPE" == "darwin"* ]]; then
    os='osx'
elif [[ "$OSTYPE" == "cygwin" ]]; then
    os='windows'
elif [[ "$OSTYPE" == "msys" ]]; then
    os='windows'
elif [[ "$OSTYPE" == "freebsd"* ]]; then
    os='freebsd'
else
    os='solaris'
fi

if [[ "$HOSTTYPE" == "x86_64" ]]; then
    arch='x64'
elif [[ "$HOSTTYPE" == "arm" ]]; then
    arch='arm'
else
    arch=''
fi

if [[ "$os" == "windows" ]]; then
    arch=''
fi
echo $os

if [ "$#" -ne 1 ]; then
    mainfile='main/glfw_main.lua'
elif [[ "$1" == *lua ]]; then
    mainfile=$1
else
    mainfile=$1'.lua'
fi
echo $mainfile

LD_LIBRARY_PATH=./bin/$os/$arch ./bin/$os/$arch/luajit $mainfile $*
