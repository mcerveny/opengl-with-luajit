# make_dep_graph.py

import os
from glob import glob

def checkFile(filename):
	#print(filename)
	bn = os.path.basename(filename)
	bn = bn[:bn.find('.')]
	#print(bn)
	with open(filename) as f:
		content = f.readlines()
	content = [x.strip() for x in content]
	content = [k for k in content if 'require' in k]
	content = [k for k in content if 'prequire' not in k]
	content = [k for k in content if not k.startswith('--')]
	for c in content:
		c = c[c.find('require')+len('require'):]
		c = c.strip("""( )""''""")
		if ',' in c:
			continue
		if '"' in c:
			c = c[:c.find('"')]
		if '.' in c:
			c = c[c.find('.')+1:]
		#print('\t',c)
		skip = ['ffi', 'matrixmath', 'opengl', 'shaderfunctions']
		if c in skip: continue
		print('    "{0}" -> "{1}"'.format(bn,c))


print("strict digraph {")
print("    rankdir=RL")


dir_path = os.path.dirname(os.path.realpath(__file__))
result = [y for x in os.walk(dir_path) for y in glob(os.path.join(x[0], '*.lua'))]
for r in result:
	if '/jit/' in r: continue
	if '/main/' in r: continue
	if '/scene_old/' in r: continue
	if '/scene_es2/' in r: continue
	checkFile(r)
#print(result)

# Label main nodes red
if False:
	mainfiles = [y for x in os.walk(os.path.join(dir_path,'main')) for y in glob(os.path.join(x[0], '*.lua'))]
	for m in mainfiles:
		bn = os.path.basename(m)[:-4]
		print("""    {0} [style=filled fillcolor=red]""".format(bn))

# Label utils light blue
if True:
	utilfiles = [y for x in os.walk(os.path.join(dir_path,'util')) for y in glob(os.path.join(x[0], '*.lua'))]
	for m in utilfiles:
		bn = os.path.basename(m)[:-4]
		print("""    {0} [style=filled fillcolor=lightblue]""".format(bn))

# Some supplementary links
if False:
	for m in [
	        "vs",
	        "boids",
	        "gs",
	        "life",
	        "fs",]:
		print('''    "{0}" -> "{1}"'''.format('shader_editor',m))
print("}")
