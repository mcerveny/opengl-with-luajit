-- libretro_input.lua
-- Maps SDL button indices to Libretro's Retropad.

libretro_input = {}

libretro_input.JOYPAD_B      =  0+1
libretro_input.JOYPAD_Y      =  1+1
libretro_input.JOYPAD_SELECT =  2+1
libretro_input.JOYPAD_START  =  3+1
libretro_input.JOYPAD_UP     =  4+1
libretro_input.JOYPAD_DOWN   =  5+1
libretro_input.JOYPAD_LEFT   =  6+1
libretro_input.JOYPAD_RIGHT  =  7+1
libretro_input.JOYPAD_A      =  8+1
libretro_input.JOYPAD_X      =  9+1
libretro_input.JOYPAD_L      = 10+1
libretro_input.JOYPAD_R      = 11+1
libretro_input.JOYPAD_L2     = 12+1
libretro_input.JOYPAD_R2     = 13+1
libretro_input.JOYPAD_L3     = 14+1
libretro_input.JOYPAD_R3     = 15+1


libretro_input.mappings = {
    ['USB Gamepad '] = {
        [1] = libretro_input.JOYPAD_X,
        [2] = libretro_input.JOYPAD_A,
        [3] = libretro_input.JOYPAD_B,
        [4] = libretro_input.JOYPAD_Y,
        [5] = libretro_input.JOYPAD_L,
        [6] = libretro_input.JOYPAD_R,
        [9] = libretro_input.JOYPAD_SELECT,
        [10] = libretro_input.JOYPAD_START,
    },
    ['USB,2-axis 8-button gamepad  '] = {
        [1] = libretro_input.JOYPAD_A,
        [2] = libretro_input.JOYPAD_B,
        [3] = libretro_input.JOYPAD_X,
        [4] = libretro_input.JOYPAD_Y,
        [5] = libretro_input.JOYPAD_L,
        [6] = libretro_input.JOYPAD_R,
        [7] = libretro_input.JOYPAD_SELECT,
        [8] = libretro_input.JOYPAD_START,
    },
    ['SNES30             SNES30 Joy    '] = {
        [1] = libretro_input.JOYPAD_A,
        [2] = libretro_input.JOYPAD_B,
        [4] = libretro_input.JOYPAD_X,
        [5] = libretro_input.JOYPAD_Y,
        [7] = libretro_input.JOYPAD_L,
        [8] = libretro_input.JOYPAD_R,
        [11] = libretro_input.JOYPAD_SELECT,
        [12] = libretro_input.JOYPAD_START,
    },
    ['Microsoft X-Box 360 pad'] = {
        [1] = libretro_input.JOYPAD_B,
        [2] = libretro_input.JOYPAD_A,
        [3] = libretro_input.JOYPAD_Y,
        [4] = libretro_input.JOYPAD_X,
        [5] = libretro_input.JOYPAD_L,
        [6] = libretro_input.JOYPAD_R,
        [7] = libretro_input.JOYPAD_SELECT,
        [8] = libretro_input.JOYPAD_START,
    },
    ['keyboard'] = {
        ['i'] = libretro_input.JOYPAD_X,
        ['j'] = libretro_input.JOYPAD_Y,
        ['k'] = libretro_input.JOYPAD_B,
        ['l'] = libretro_input.JOYPAD_A,
        ['u'] = libretro_input.JOYPAD_L,
        ['o'] = libretro_input.JOYPAD_R,
        ['w'] = libretro_input.JOYPAD_UP,
        ['s'] = libretro_input.JOYPAD_DOWN,
        ['a'] = libretro_input.JOYPAD_LEFT,
        ['d'] = libretro_input.JOYPAD_RIGHT,
        ['v'] = libretro_input.JOYPAD_SELECT,
        ['b'] = libretro_input.JOYPAD_START,
    },
}

-- Jam SDL input state into Retropad state
function libretro_input.getRetropadStateFromSDL(name,axes, buttons)
    local rp = {}
    for i=1,14 do
        rp[i] = 0
    end
    local map = libretro_input.mappings['USB Gamepad ']
    if libretro_input.mappings[name] then map = libretro_input.mappings[name] end
    for i=1,math.min(14,#buttons) do
        --print(i,buttons[i])
        local RPi = map[i]
        if RPi then rp[RPi] = buttons[i] end
    end

    -- Axes
    if #axes == 5 then
        -- TODO: the first 4 axes are x, the 5th y? Seems wrong...
        local xshort = axes[1]
        local yshort = axes[5]
        local thresh = 1000
        if     xshort < -thresh then rp[libretro_input.JOYPAD_LEFT] = 1
        elseif xshort > thresh then rp[libretro_input.JOYPAD_RIGHT] = 1 end
        if     yshort < -thresh then rp[libretro_input.JOYPAD_UP] = 1
        elseif yshort > thresh then rp[libretro_input.JOYPAD_DOWN] = 1 end
    elseif #axes == 4 then
        local xshort = axes[1]
        local yshort = axes[2]
        local thresh = 1000
        if     xshort < -thresh then rp[libretro_input.JOYPAD_LEFT] = 1
        elseif xshort > thresh then rp[libretro_input.JOYPAD_RIGHT] = 1 end
        if     yshort < -thresh then rp[libretro_input.JOYPAD_UP] = 1
        elseif yshort > thresh then rp[libretro_input.JOYPAD_DOWN] = 1 end
    elseif #axes == 2 then -- 'USB,2-axis 8-button gamepad'
        local xshort = axes[1]
        local yshort = axes[2]
        local thresh = 1000
        if     xshort < -thresh then rp[libretro_input.JOYPAD_LEFT] = 1
        elseif xshort > thresh then rp[libretro_input.JOYPAD_RIGHT] = 1 end
        if     yshort < -thresh then rp[libretro_input.JOYPAD_UP] = 1
        elseif yshort > thresh then rp[libretro_input.JOYPAD_DOWN] = 1 end
    end

    return rp
end

return libretro_input
