-- camera1.lua

local mm = require 'util.matrixmath'

camera1 = {}
camera1.__index = camera1

function camera1.new(...)
    local self = setmetatable({}, camera1)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function camera1:init()
    self.chassis = {0,0,5}
    self.objrot = {0,0}
    self.camerapan = {0,0,0}
end

function camera1:reset()
    self:init()
end

function camera1:timestep(absTime, dt)
    self.rotx = math.sin(absTime)
end

function camera1:getViewMatrix()
    local v = {}
    mm.make_identity_matrix(v)

    -- Lookaround camera
    mm.glh_translate(v, self.chassis[1], self.chassis[2], self.chassis[3])
    mm.glh_translate(v, self.camerapan[1], self.camerapan[2], self.camerapan[3])
    mm.glh_rotate(v, -self.objrot[1], 0,1,0)
    mm.glh_rotate(v, -self.objrot[2], 1,0,0)
    return v
end

return camera1
