// screen space squares

layout (points) in;
layout (triangle_strip, max_vertices = 6) out;

uniform mat4 modelMtx;
uniform mat4 viewMtx;
uniform mat4 projMtx;

uniform int numParticles;
uniform float time;

out vec3 gfPos;
out vec3 gfNormal;

float hash( float n ) { return fract(sin(n)*43758.5453); }

vec3 hsv2rgb( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );
    return c.z * mix( vec3(1.0), rgb, c.y);
}

void main()
{
    int rows = int(sqrt(numParticles));
    float a = float(gl_PrimitiveIDIn) / float(numParticles);
    float t = float(gl_PrimitiveIDIn/rows);
    float u = float(gl_PrimitiveIDIn%rows);

    float r = float(rows);
    vec3 p = vec3(t/r, u/r, 0.);
    p = 2.*p - vec3(1.);
    float dx = 2./r;

    vec3 col = vec3(hash(a), hash(1.3*a), hash(1.7*a));
    col = hsv2rgb(col);
    p.xy += .01 * pow(1.+sin(time),4.) * (hash(a*t)-.5);

    gfPos = p;
    gl_Position = vec4(gfPos,1.);
    gfNormal = col;
    EmitVertex();

    gfPos = p+vec3(dx,0.,0.);
    gl_Position = vec4(gfPos,1.);
    gfNormal = col;
    EmitVertex();

    gfPos = p+vec3(0.,dx,0.);
    gl_Position = vec4(gfPos,1.);
    gfNormal = col;
    EmitVertex();

    EndPrimitive();

    gfPos = p+vec3(dx,0.,0.);
    gl_Position = vec4(gfPos,1.);
    gfNormal = col;
    EmitVertex();
    gfPos = p+vec3(dx,dx,0.);
    gl_Position = vec4(gfPos,1.);
    gfNormal = col;
    EmitVertex();
    gfPos = p+vec3(0.,dx,0.);
    gl_Position = vec4(gfPos,1.);
    gfNormal = col;
    EmitVertex();

    EndPrimitive();
}
