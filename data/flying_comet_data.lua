-- flying_comet_data.lua

flying_comets = {}

flying_comets.cometData = {
    {
        {"111",{0,0,0}},
        {"2222",{1,2,0}},
        {"3333",{2,4,0}},
        {"44444",{3,6,0}},
    },
    {
        {"Comet1",{0,0,0}},
        {"Comet2",{1,2,0}},
        {"Comet3",{2,4,0}},
        {"Comet4",{3,6,0}},
    },
}
