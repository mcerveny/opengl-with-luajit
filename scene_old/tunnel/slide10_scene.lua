--[[ slide10_scene.lua

    A Powerpoint-similar presentation in OpenGL.

    Each keypress reveals a bullet point.
]]
slide10_scene = {}

require("util.slideshow")

local slide = Slideshow.new({
    title="Questions?",
    bullets={
        "Code available on bitbucket",
        "bitbucket.org/jimbo00000",
        "",
        "Now let's make our own!",
    }
})

local dataDir = nil

-- Since data files must be loaded from disk, we have to know
-- where to find them. Set the directory with this standard entry point.
function slide10_scene.setDataDirectory(dir)
    dataDir = dir
end

function slide10_scene.initGL()
    slide:initGL(dataDir)
end

function slide10_scene.exitGL()
    slide:exitGL()
end

function slide10_scene.render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    slide:draw_text()
end

function slide10_scene.timestep(absTime, dt)
end

function slide10_scene.keypressed(ch)
    return slide:keypressed(ch)
end

return slide10_scene
