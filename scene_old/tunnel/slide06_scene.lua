--[[ slide06_scene.lua

    Polar coordinates - radius
]]
slide06_scene = {}

require("util.slideshow")
local slide = Slideshow.new({
    title="Polar Coordinates",
    bullets={
        "1/(x^2 + y^2) = r",
        "Aliasing at center",
        "What can be done?"
    }
})

local shad = require("util.fullscreen_shader")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local ffi = require("ffi")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

-- Module-internal state: hold a list of VBOs for deletion on exitGL
local vbos = {}
local vao = 0
local prog = 0

local dataDir = nil

local arrow_move = 0

function slide06_scene.setDataDirectory(dir)
    dataDir = dir
end

local basic_vert = [[
#version 310 es

in vec4 vPosition;
uniform mat4 mvmtx;

void main()
{
    gl_Position = mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

out vec4 fragColor;

void main()
{
    fragColor = vec4(1.,0.,0.,1.);
}
]]


local function init_tri_attributes()
    local verts = glFloatv(3*3, {
        0,0,0,
        .1,.2,0,
        .2,.1,0,
        })

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(vbos, vvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
end



local frag_main = [[
void main()
{
    vec2 uv_ = 2.*uv - vec2(1.);
    float radial = 1. / (uv_.x*uv_.x + uv_.y*uv_.y);
    float radwaves = 1.5 * sin(2.*radial);
    fragColor = vec4(radwaves,1.,radwaves, 1.0);
}
]]
function slide06_scene.initGL()
    slide:initGL(dataDir)

    shad = FullscreenShader.new(frag_main)
    shad:initGL()

    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)

    prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    init_tri_attributes()
    gl.glBindVertexArray(0)
end

function slide06_scene.exitGL()
    slide:exitGL()
    shad:exitGL()

    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}
    gl.glDeleteProgram(prog)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function slide06_scene.timestep(absTime, dt)
    arrow_move = math.abs(math.sin(3*absTime))
end

function slide06_scene.keypressed(ch)
    return slide:keypressed(ch)
end

function slide06_scene.render_for_one_eye(view, proj)
    shad:render(view, proj)

    gl.glDisable(GL.GL_DEPTH_TEST)

    if slide.shown_lines > 1 then
        gl.glUseProgram(prog)

        local m = {}
        mm.make_identity_matrix(m)
        local d = .12 * arrow_move, arrow_move
        mm.glh_translate(m, d, d, 0)
        local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
        gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, m))
        gl.glBindVertexArray(vao)
        gl.glDrawArrays(GL.GL_TRIANGLES, 0, 3)
        gl.glBindVertexArray(0)
        gl.glUseProgram(0)
    end

    slide:draw_text()
end

return slide06_scene
