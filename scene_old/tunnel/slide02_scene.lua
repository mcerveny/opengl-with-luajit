--[[ slide02_scene.lua

    Includes text display from a slide scene and shows a 3D scene over it.
]]
slide02_scene = {}

local s01 = require("scene.tunnel.slide01_scene")
local sVS = require("scene.vsfstri")
local mm = require("util.matrixmath")

function slide02_scene.setDataDirectory(dir)
    s01.setDataDirectory(dir)
end

function slide02_scene.initGL()
    s01.initGL()
    sVS.initGL()
end

function slide02_scene.exitGL()
    s01.exitGL()
    sVS.exitGL()
end

function slide02_scene.render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    gl.glDisable(GL.GL_DEPTH_TEST)
    s01.render_for_one_eye(view, proj)
end

function slide02_scene.timestep(absTime, dt)
    s01.timestep(absTime, dt)
    sVS.timestep(absTime, dt)
end

function slide02_scene.keypressed(ch)
    local ret = s01.keypressed(ch)
    if ret then return true end
    return false
end

return slide02_scene
