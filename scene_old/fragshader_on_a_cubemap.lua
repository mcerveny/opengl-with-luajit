-- fragshader_on_a_cubemap.lua

fragshader_on_a_cubemap = {}

local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local prog_simple = 0
local vao = 0
local vbos = {}

local simple_vert = [[
#version 330

in vec4 vPosition;
in vec3 vColor;
out vec3 vfColor;

void main()
{
    vfColor = vColor;
    gl_Position = vPosition;
}
]]

local simple_frag = [[
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor,1.0);
}
]]


local function make_quad_vbos(prog)
    vbos = {}

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    table.insert(vbos, vvbo)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    local verts = glFloatv(4*2, {
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        })
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)

    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vpos_loc)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    table.insert(vbos, cvbo)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    local cols = glFloatv(4*2, {
        0,0,
        1,0,
        1,1,
        0,1,
        })
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)

    local col_loc = gl.glGetAttribLocation(prog, "vColor")
    gl.glVertexAttribPointer(col_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(col_loc)

    local quads = glUintv(3*2, {
        0,1,2,
        0,2,3,
    })
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    table.insert(vbos, qvbo)

    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)

    return vbos
end

function fragshader_on_a_cubemap.initGL()
    prog_simple = sf.make_shader_from_source({
        vsrc = simple_vert,
        fsrc = simple_frag,
        })

    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.glBindVertexArray(vao)
    vbos = make_quad_vbos(prog_simple)
    gl.glBindVertexArray(0)
end

function fragshader_on_a_cubemap.exitGL()
    gl.glBindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.glDeleteBuffers(1,v)
    end
    vbos = {}
    gl.glDeleteProgram(prog_simple)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function fragshader_on_a_cubemap.draw_to_cubemap()
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL)
    gl.glUseProgram(prog_simple)
    gl.glBindVertexArray(vao)
    local y0 = 0
    local y1 = 1/3
    local y2 = 2/3
    local y3 = 1
    --[[
     y3 ---------------------
        |    |    |    | +y |
     y2 |--------------------
        | +x | -z | -x | +z |
     y1 |--------------------
        | -y |    |    |    |
     y0 ---------------------
    ]]
    local quads = {
        -- Look up "nets of a cube"
        {  0,y1, .25,y1, .25,y2,   0,y2,}, -- +x
        { .5,y1, .75,y1, .75,y2,  .5,y2,}, -- -x
        {.75,y0,   1,y0,   1,y1, .75,y1,}, -- +y (y flipped)
        {  0,y3,   0,y2, .25,y2, .25,y3,}, -- -y (rotated and flipped)
        {.75,y1,   1,y1,   1,y2, .75,y2,}, -- +z
        {.25,y1,  .5,y1,  .5,y2, .25,y2,}, -- -z
    }
    for i=1,6 do
        gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0,
            GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X+i-1, fbo.tex, 0)

        local q = quads[i]
        local verts = glFloatv(#q, q)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbos[2][0])
        gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)

        gl.glDrawElements(GL.GL_TRIANGLES, 3*2, GL.GL_UNSIGNED_INT, nil)
    end
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function fragshader_on_a_cubemap.render_for_one_eye(mview, proj)
end

function fragshader_on_a_cubemap.timestep(dt)
end

return fragshader_on_a_cubemap
