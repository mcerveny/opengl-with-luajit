--[[ capsule_scene.lua

]]
capsule_scene = {}

local Grid = require("scene.gridcube_scene")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local progs = {}

--[[
    Perturb all vertices with a function.
]]
local perturbverts_comp_src = [[
#version 310 es
#line 22
layout(local_size_x=128) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };

uniform int uIndexMin;
uniform int uIndexMax;

vec3 displacePointToCylinder(vec3 p)
{
    vec3 center = vec3(.5);
    float radius = .5;

    vec3 centerLinePoint = vec3(center.x, p.y, center.z);
    vec3 toCenterLine = p - centerLinePoint;

    vec3 pushedPoint = p;
    if (length(toCenterLine) > radius)
    {
        pushedPoint = centerLinePoint + radius * normalize(toCenterLine);
    }
    return pushedPoint;
}

vec3 displacePointToCapsuleEnds(vec3 p)
{
    vec3 center = vec3(.5, .01,.5); // top face
    vec3 toCenter = center - p.xyz;
    float radius = .5;
    return center + radius*normalize(p-center);

    float r2 = radius * radius;
    // x^2 + y^2 == r^2
    float x = length(toCenter.xz);
    float y = sqrt(r2 - x*x);

    if (x > radius)
    {
        p.xz /= x;
    }

    if (p.y > .5)
        p.y += y;
    else
        p.y -= y;
    return p;
}

void main()
{
    int index = int(gl_GlobalInvocationID.x);
    vec4 p = positions[index];

    if ((index < uIndexMin) || (index >= uIndexMax))
    {
        p.xyz = displacePointToCylinder(p.xyz);
    }
    else
    {
        p.xyz = displacePointToCapsuleEnds(p.xyz);
    }

    positions[index] = p;
}
]]

local function perturbVertexPositions()
    local vvbo = Grid.get_vertices_vbo()
    local num_verts = Grid.get_num_verts()
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, vvbo[0])
    local prog = progs.perturbverts
    gl.glUseProgram(prog)

    local f_1 = Grid.get_cubemesh().subdivs + 1
    local verts_per_face = f_1 * f_1
    local i0_loc = gl.glGetUniformLocation(prog, "uIndexMin")
    local i1_loc = gl.glGetUniformLocation(prog, "uIndexMax")

    gl.glUniform1i(i0_loc, 2*verts_per_face)
    gl.glUniform1i(i1_loc, 3*verts_per_face)
    gl.glDispatchCompute(num_verts/128+1, 1, 1)
    --gl.glUniform1i(i0_loc, 5*verts_per_face)
    --gl.glUniform1i(i1_loc, 6*verts_per_face)
    --gl.glDispatchCompute(num_verts/128+1, 1, 1)

    gl.glUseProgram(0)
end

function capsule_scene.initGL()
    progs.perturbverts = sf.make_shader_from_source({
        compsrc = perturbverts_comp_src,
        })

    Grid.initGL()
    perturbVertexPositions()
    Grid.recalc_normals()
end

function capsule_scene.exitGL()
    Grid.exitGL()
    for _,v in pairs(progs) do
        gl.glDeleteProgram(v)
    end
end

function capsule_scene.render_for_one_eye(view, proj)
    --gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_LINE)
    Grid.render_for_one_eye(view, proj)
end

function capsule_scene.timestep(absTime, dt)
    Grid.timestep(absTime, dt)
end

function capsule_scene.keypressed(ch)
end

function capsule_scene.onSingleTouch(pointerid, action, x, y)
end

return capsule_scene
