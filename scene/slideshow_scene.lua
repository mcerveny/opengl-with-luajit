-- slideshow_scene.lua
-- A basic outline for a powerpoint-style presentation.
-- Add your own bullet points in init() and optionally add extra
-- display functionality in render.

slideshow_scene = {}
slideshow_scene.__index = slideshow_scene

local ffi = require("ffi")
local sf = require("util.shaderfunctions")

require("util.slideshow")

function slideshow_scene.new(...)
    local self = setmetatable({}, slideshow_scene)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function slideshow_scene:init()
    self.slides = {}
    table.insert(self.slides, Slideshow.new({
            title="Title",
            bullets={
                " - bullet 1",
                " - bullet 2",
                " - bullet 3",
            }
        })
    )
    table.insert(self.slides, Slideshow.new({
            title="First slide",
            bullets={
                " - item",
                " - another item",
                " - bla bla bla",
            }
        })
    )
    self.cur_slide = 1
end

function slideshow_scene:setDataDirectory(dir)
    self.data_dir = dir
end

function slideshow_scene:initGL()
    for _,s in pairs(self.slides) do
        s:initGL(self.data_dir)
    end

    local dir = "fonts"
    local fontname = "segoe_ui128"
    if self.data_dir then dir = self.data_dir .. "/" .. dir end
    self.glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function slideshow_scene:exitGL()
    for _,s in pairs(self.slides) do
        s:exitGL()
    end
    self.glfont:exitGL()
end

function slideshow_scene:render_for_one_eye(view, proj)
    gl.glClearColor(1,1,1,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    self.slides[self.cur_slide]:draw_text(self.glfont)
end

function slideshow_scene:timestep(absTime, dt)
end

function slideshow_scene:increment_slide(incr)
    self.cur_slide = self.cur_slide + incr

    if self.cur_slide < 1 then self.cur_slide = 1 end
    if self.cur_slide > #self.slides then self.cur_slide = #self.slides end
end

function slideshow_scene:keypressed(ch)
    local slide = self.slides[self.cur_slide]

    local func_table = {
        [257] --[[glfw.GLFW.KEY_ENTER]] = function (x)
            self:increment_slide(1)
            return true
        end,
        [259] --[[glfw.GLFW.KEY_BACKSPACE]] = function (x)
            self:increment_slide(-1)
            return true
        end,
        [262] --[[right arrow in glfw]] = function (x)
            if slide.shown_lines >= #slide.bullet_points then
                self:increment_slide(1)
                return true
            end
        end,
        [263] --[[left arrow in glfw]] = function (x)
            if slide.shown_lines <= 0 then
                self:increment_slide(-1)
                return true
            end
        end,
    }
    local f = func_table[ch]
    if f then ret = f() if ret then return ret end end

    return slide:keypressed(ch)
end

local action_types = {
  [0] = "Down",
  [1] = "Up",
  [2] = "Move",
  [3] = "Cancel",
  [4] = "Outside",
  [5] = "PointerDown",
  [6] = "PointerUp",
}

function slideshow_scene:onSingleTouch(pointerid, action, x, y)
    --print("slideshow_scene.onSingleTouch",pointerid, action, x, y)    local actionflag = action % 255
    local actionflag = action % 255
    local a = action_types[actionflag]
    if a == "Down" or a == "PointerDown" then
        slideshow_scene.keypressed(262)
    end
end

return slideshow_scene
