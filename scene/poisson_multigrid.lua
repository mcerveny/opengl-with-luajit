--
-- poisson_multigrid.lua
--
-- Run a 2D semi-Lagrangian vortex method in vorticity-streamfunction variables
-- Solve the Poisson equation with a geometric multigrid method using FMG cycles
-- Advect using a 2nd order semi-Lagrangian method with linear interpolation
-- Advect a high-resolution color scalar field
--
-- (c)2018 Mark J. Stock markjstock@gmail.com
-- portions from compute_image.lua (c)2018 James Sussino
--

poisson_multigrid = {}
poisson_multigrid.__index = poisson_multigrid

function poisson_multigrid.new(...)
    local self = setmetatable({}, poisson_multigrid)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local glFloatv = ffi.typeof('GLfloat[?]')

function poisson_multigrid:init()
    self.vao = 0
    self.vbos = {}
    self.prog_draw = 0

    -- time step and diffusion coefficient (not used)
    self.dt = 0.1
    --self.nu = 0.0001
    self.pause = false

    -- view mode
    self.mode = 2
    -- self.mode is
    --   0 is color and vort, always fit completely
    --   1 is color and vort, cropped to fill window
    --   2 is color only, cropped to fill window

    -- size of work group in compute shaders
    self.gridx = 16

    -- base resolution (set the power to 1..6)
    -- levels=1 means run a sim on a texture with resolution gridx*2^1 cells
    -- gridx=16 and levels=4 means run a 256x256 sim, 6 is 1024^2, 8 is 4096^2
    self.levels = 6
    local res = self.gridx * math.pow(2,self.levels)

    -- at lowest resolution are the streamfunction and associated velocity
    self.field = {}
    local aspect = 1
    -- self.sim is
    --   0 is dead (no vorticity)
    --   1 is pseudo-random vorticity
    --   2 is an asymmetric vortex patch
    --   3 is a pair of counterrotating vortices
    --   4 is conter-oriented rings of vorticity
    --   6 is a sinusoidal shear layer
    --   7 is two sinusoidal shear layers
    self.sim = 0

    self.field["psi"] = {x=res, y=res/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F, mag=100}
    self.field["vel"] = {x=res, y=res/aspect, efmt=GL.GL_RG, ifmt=GL.GL_RG32F, mag=200}
    -- need a temporary to allow ping-ponging buffers
    self.field["tpsi"] = {x=res, y=res/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F}

    self.field["vort"]   = {x=res, y=res/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F, mag=3}
    --self.field["scalar"] = {x=res, y=res/aspect, ifmt=GL.GL_R32F}
    -- need a temporary to allow ping-ponging buffers
    self.field["tscal"]   = {x=res, y=res/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F}
    --self.field["resid"]   = {x=res, y=res/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F, mag=5}

    -- extra fields for the multigrid solution
    self.rhs = {}
    self.psi = {}
    self.tmp = {}
    local cres = res
    for ilev=1,self.levels do
      cres = cres / 2
      self.rhs[ilev] = {x=cres, y=cres/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F, mag=2}
      self.psi[ilev] = {x=cres, y=cres/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F, mag=100}
      self.tmp[ilev] = {x=cres, y=cres/aspect, efmt=GL.GL_RED, ifmt=GL.GL_R32F, mag=100}
      print("Creating coarse grid",ilev,"with resolution",cres)
    end

    -- potentially at higher resolution is the conserved, drawn color field
    local color_mult = 2
    -- self.sim is
    --   0 is checkerboard
    --   1 is random noise
    --   2 is one thin black line
    --   3 is color bands
    --   4 is vertical blank and white bands
    --   6 is horizontal blank and white bands
    self.colsim = 3
    self.field["color"] = {x=res*color_mult, y=res*color_mult/aspect, efmt=GL.GL_RGBA, ifmt=GL.GL_RGBA32F}
    -- need a temporary to allow ping-ponging buffers
    self.field["tcol"] = {x=res*color_mult, y=res*color_mult/aspect, efmt=GL.GL_RGBA, ifmt=GL.GL_RGBA32F}

    self.mousing = false
    self.nmousept = 0
    self.maxmpts = 64
    self.mousevec = ffi.new("float[?]", 2*self.maxmpts)
end

function poisson_multigrid:setDataDirectory(dir)
    self.data_dir = dir
end

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

-- render an rgba fragment
local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;
uniform float texMag;

void main()
{
    vec2 tc = vfColor.xy;
    tc.y = 1.-tc.y;
    vec4 col = texture(sTex, tc);
    fragColor = vec4(texMag*col.xyz, 1.);
}
]]

-- render a single-channel (red) fragment as greyscale
local twochan_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;
uniform float texMag;

void main()
{
    vec2 tc = vfColor.xy;
    tc.y = 1.-tc.y;
    vec4 col = texture(sTex, tc);
    fragColor = vec4(0.5*texMag*col.x, 0., 0.5*texMag*col.y, 1.);
}
]]

-- render a single-channel (red) fragment as greyscale
local red_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;
uniform float texMag;

void main()
{
    vec2 tc = vfColor.xy;
    tc.y = 1.-tc.y;
    vec4 col = texture(sTex, tc);
    float px = 0.5 + texMag * col.x;
    fragColor = vec4(px, px, px, 1.);
}
]]

local img_init_rgba8 = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_in;
layout(rgba8, binding = 1) uniform restrict image2D img_output;

uniform int uFillType;

float hash( float n ) { return fract(sin(n)*43758.5453); }

float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);
}

void main() {
    vec4 pixel = vec4(0.0);
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    if (uFillType == 0)
    {
        // Init image to checkerboard
        if (((pixel_coords.x & 64) == 0)
         != ((pixel_coords.y & 64) == 0))
        {
            pixel = vec4(1.);
        }
    }
    else if (uFillType == 1)
    {
        // Random init
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            float(pixel_coords.y)/float(imageSize(img_output).y));
        float hval = hash(coord);
        pixel = hval > .5 ? vec4(1.) : vec4(0.);
    }
    else if (uFillType == 2)
    {
        // Single black pixel in center init
        pixel = vec4(1.);
        if (pixel_coords.x == imageSize(img_output).x/2)
            pixel = vec4(0.);
    }
    else if (uFillType == 5)
    {
        // sample from the input texture
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            1.0-float(pixel_coords.y)/float(imageSize(img_output).y));
        vec4 currpix = imageLoad(img_output, pixel_coords);
        vec4 newpixl = texture(tex_in, coord);
        float a = newpixl.w;
        pixel = vec4((1.0-a)*currpix.xyz + a*newpixl.xyz, 1.0);
    }

    imageStore(img_output, pixel_coords, pixel);
}
]]

local img_init_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_in;
layout(rgba32f, binding = 1) uniform restrict image2D img_output;

uniform int uFillType;

float hash( float n ) { return fract(sin(n)*43758.5453); }

float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);
}

void main() {
    vec4 pixel = vec4(0.0);
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    if (uFillType == 0)
    {
        // Init image to checkerboard
        if (((pixel_coords.x & 64) == 0)
         != ((pixel_coords.y & 64) == 0))
        {
            pixel = vec4(1.);
        }
    }
    else if (uFillType == 1)
    {
        // Random init
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            float(pixel_coords.y)/float(imageSize(img_output).y));
        float hval = hash(coord);
        pixel = hval > .5 ? vec4(1.) : vec4(0.);
    }
    else if (uFillType == 2)
    {
        // Single black pixel in center init
        pixel = vec4(1.);
        if (pixel_coords.x == imageSize(img_output).x/2)
            pixel = vec4(0.);
    }
    else if (uFillType == 3)
    {
        // Random bands of color
        float xc = floor(pixel_coords.x / 64.0);
        vec2 coord = vec2(xc, 0.0);
        pixel = vec4(0.1+0.7*hash(vec2(xc, 0.0)),
                     0.2+0.7*hash(vec2(xc, 1.0)),
                     0.3+0.7*hash(vec2(xc, 2.0)),
                     1.0);
    }
    else if (uFillType == 4)
    {
        // Vertical bands of black and white
        if ((pixel_coords.x & 128) == 0)
        {
            pixel = vec4(1.);
        }
    }
    else if (uFillType == 5)
    {
        // sample from the input texture
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            1.0-float(pixel_coords.y)/float(imageSize(img_output).y));
        vec4 currpix = imageLoad(img_output, pixel_coords);
        vec4 newpixl = texture(tex_in, coord);
        float a = newpixl.w;
        pixel = vec4((1.0-a)*currpix.xyz + a*newpixl.xyz, 1.0);
    }
    else if (uFillType == 6)
    {
        // Horizontal bands of black and white
        if ((pixel_coords.y & 128) == 0)
        {
            pixel = vec4(1.);
        }
    }

    imageStore(img_output, pixel_coords, pixel);
}
]]

-- modify a field of color
local img_mod_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(rgba32f, binding = 0) uniform restrict image2D img_inout;

uniform float brite_in;
uniform float contrast_in;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
    vec4 pixel = imageLoad(img_inout, pixel_coords);

    pixel = (1.0+brite_in) * pixel;
    pixel = vec4(0.5) + (1.0+contrast_in) * (pixel-vec4(0.5));

    imageStore(img_inout, pixel_coords, pixel);
}
]]

local img_init_r32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_in;
layout(r32f, binding = 1) uniform writeonly restrict image2D img_output;

uniform int uFillType;

float hash( float n ) { return fract(sin(n)*43758.5453); }

float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);
}

const float TWOPI = 2.0*3.1415926535897932384626433832795;

void main() {
    float pixel = 0.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    if (uFillType == 0)
    {
        // Init image to zero
    }
    else if (uFillType == 1)
    {
        // Random init
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            float(pixel_coords.y)/float(imageSize(img_output).y));
        pixel = 3.0 * (2.0*hash(coord) - 1.0);
    }
    else if (uFillType == 2)
    {
        // Asymmetric vortex
        vec2 coord = vec2(
            4.0*(float(pixel_coords.x)/float(imageSize(img_output).x)-0.3),
                (float(pixel_coords.y)/float(imageSize(img_output).y)-0.7) );
        //float dr = length(coord);
        //if (length(coord) < 0.15)
        //    pixel = 1.0;
        pixel = smoothstep(0.16, 0.14, length(coord));
    }
    else if (uFillType == 3)
    {
        // counter-rotating vortexes
        vec2 coord = vec2(
            (float(pixel_coords.x)/float(imageSize(img_output).x)-0.3),
            (float(pixel_coords.y)/float(imageSize(img_output).y)-0.7) );
        if (length(coord) < 0.1)
            pixel += 1.0;
        coord = vec2(
            (float(pixel_coords.x)/float(imageSize(img_output).x)-0.4),
            (float(pixel_coords.y)/float(imageSize(img_output).y)-0.5) );
        if (length(coord) < 0.13)
            pixel -= 1.0;
    }
    else if (uFillType == 4)
    {
        vec2 coord = vec2(
            (float(pixel_coords.x)/float(imageSize(img_output).x)-0.47),
            (float(pixel_coords.y)/float(imageSize(img_output).y)-0.51) );
        float dr = length(coord);
        pixel = smoothstep(0.18, 0.20, dr) - smoothstep(0.22, 0.24, dr)
              - (smoothstep(0.15, 0.17, dr) - smoothstep(0.18, 0.20, dr)) ;
    }
    else if (uFillType == 5)
    {
        // sample from the input texture
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            1.0-float(pixel_coords.y)/float(imageSize(img_output).y));
        pixel = texture(tex_in, coord).x;
    }
    else if (uFillType == 6)
    {
        // sinusoidal shear layer
        float coord = float(pixel_coords.y)/float(imageSize(img_output).y) - 0.5;
        coord += 0.013*sin(0.2+3*TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        coord += 0.01*sin(-0.05+2*TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        coord += 0.004*sin(0.1+TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        pixel = -10.0 * smoothstep(0.004, 0.002, abs(coord));
    }
    else if (uFillType == 7)
    {
        // two sinusoidal shear layers
        float coord = float(pixel_coords.y)/float(imageSize(img_output).y) - 0.25;
        coord += 0.013*sin(0.2+3*TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        coord += 0.01*sin(-0.05+2*TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        coord += 0.004*sin(0.1+TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        if (abs(coord) < 0.004) pixel = -4.0 * smoothstep(0.004, 0.002, abs(coord));

        coord = float(pixel_coords.y)/float(imageSize(img_output).y) - 0.75;
        coord += 0.012*sin(2.7+3*TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        coord += 0.009*sin(0.9+2*TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        coord += 0.005*sin(-1.15+TWOPI*float(pixel_coords.x)/float(imageSize(img_output).x));
        if (abs(coord) < 0.004) pixel = -5.0 * smoothstep(0.004, 0.002, abs(coord));
    }

    imageStore(img_output, pixel_coords, vec4(pixel,0,0,1));
}
]]

-- perform a restriction to a coarser resolution AND a same-res copy
local img_restrict_blocked = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D fine_in;
layout(r32f, binding = 1) uniform writeonly restrict image2D coarse_out;
//layout(r32f, binding = 2) uniform writeonly restrict image2D fine_out;

void main() {
    ivec2 out_coord = ivec2(gl_GlobalInvocationID.xy);

    // center coordinates of finer mesh
    int inx = 2 * out_coord.x;
    int iny = 2 * out_coord.y;

    // shifted coordinates of finer mesh
    int inxp1 = inx + 1;
    int inxm1 = inx - 1;
    int inyp1 = iny + 1;
    int inym1 = iny - 1;

    // account for the periodic domain
    if (inx == 0) inxm1 = imageSize(fine_in).x - 1;
    if (iny == 0) inym1 = imageSize(fine_in).y - 1;
    if (inx == imageSize(fine_in).x-1) inxp1 = 0;
    if (iny == imageSize(fine_in).y-1) inyp1 = 0;

    float ll = imageLoad(fine_in, ivec2(inxm1, inym1)).x;
    float cl = imageLoad(fine_in, ivec2(inx,   inym1)).x;
    float rl = imageLoad(fine_in, ivec2(inxp1, inym1)).x;
    float lc = imageLoad(fine_in, ivec2(inxm1, iny)).x;
    float cc = imageLoad(fine_in, ivec2(inx,   iny)).x;
    float rc = imageLoad(fine_in, ivec2(inxp1, iny)).x;
    float lu = imageLoad(fine_in, ivec2(inxm1, inyp1)).x;
    float cu = imageLoad(fine_in, ivec2(inx,   inyp1)).x;
    float ru = imageLoad(fine_in, ivec2(inxp1, inyp1)).x;

    float pixel = (4.0*cc + 2.0*(cl+lc+rc+cu) + ll+rl+lu+ru) / 16.0;

    // store the coarse version
    imageStore(coarse_out, out_coord, vec4(pixel, 0, 0, 1));

    // 9 loads, 1 store, 11 flops, 10 iops

    // store the four fine pixels
    //imageStore(fine_out, ivec2(inx, iny), vec4(cc, 0, 0, 1));
    //imageStore(fine_out, ivec2(inxp1, iny), vec4(rc, 0, 0, 1));
    //imageStore(fine_out, ivec2(inx, inyp1), vec4(cu, 0, 0, 1));
    //imageStore(fine_out, ivec2(inxp1, inyp1), vec4(ru, 0, 0, 1));
}
]]

-- perform one step of a relaxation scheme for the Poisson equation
local img_relax_once = [[
#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // right-hand side
    vec4 temp = imageLoad(img_vort_in, pixel_coords);
    //float ootdx = imageSize(img_psi_in).x / 2.0;
    float psi_new = temp.x * pow(1.0/imageSize(img_psi_in).x, 2);

    // psi <- stuff
    int ishift = pixel_coords.y - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).y;
    temp = imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift));
    psi_new += temp.x;

    ishift = pixel_coords.y + 1;
    ishift %= imageSize(img_psi_in).y;
    temp = imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift));
    psi_new += temp.x;

    ishift = pixel_coords.x - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).x;
    temp = imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y));
    psi_new += temp.x;

    ishift = pixel_coords.x + 1;
    ishift %= imageSize(img_psi_in).x;
    temp = imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y));
    psi_new += temp.x;

    imageStore(img_psi_out, pixel_coords, vec4(0.25*psi_new, 0, 0, 1));
}
]]

-- perform one step of a relaxation scheme for the Multigrid solution
local img_relax_once_blocked = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // right-hand side
    float temp = imageLoad(img_vort_in, pixel_coords).x;
    float psi_new = temp * pow(1.0/imageSize(img_psi_in).x, 2);

    // psi <- stuff
    int ishift = pixel_coords.y - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).y;
    temp = imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift)).x;
    psi_new += temp;

    ishift = pixel_coords.y + 1;
    ishift %= imageSize(img_psi_in).y;
    temp = imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift)).x;
    psi_new += temp;

    ishift = pixel_coords.x - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).x;
    temp = imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y)).x;
    psi_new += temp;

    ishift = pixel_coords.x + 1;
    ishift %= imageSize(img_psi_in).x;
    temp = imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y)).x;
    psi_new += temp;

    imageStore(img_psi_out, pixel_coords, vec4(0.25*psi_new, 0, 0, 1));

    // total is 5 loads, 1 store, 8 flops, 8 iops
}
]]

-- perform one step of a relaxation scheme for the Poisson equation
local img_relax_once_blocked_shared = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

shared float tmp[gl_WorkGroupSize.x+2][gl_WorkGroupSize.y+2];

void main() {
    ivec2 global_index = ivec2(gl_GlobalInvocationID.xy);
    ivec2 local_index = ivec2( int(gl_WorkGroupID.x)*int(gl_WorkGroupSize.x) + int(gl_LocalInvocationID.x),
                               int(gl_WorkGroupID.y)*int(gl_WorkGroupSize.y) + int(gl_LocalInvocationID.y) );

    // load psi_in into shared memory

    // first, the middle (most of the loads)
    tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)] = imageLoad(img_psi_in, global_index).x;

    // then the left and right sides (this assumes work groups fit perfectly into overall dimensions)

    if (int(gl_LocalInvocationID.x) == 0) {
        // only some threads do this

        int ileft = int(gl_WorkGroupID.x)*int(gl_WorkGroupSize.x) - 1;
        if (ileft < 0) ileft = imageSize(img_psi_in).x - 1;
        tmp[0][1+int(gl_LocalInvocationID.y)]                         = imageLoad(img_psi_in, ivec2(ileft, global_index.y)).x;

        int iright = int(1+gl_WorkGroupID.x)*int(gl_WorkGroupSize.x);
        if (iright > imageSize(img_psi_in).x-1) iright = 0;
        tmp[1+int(gl_WorkGroupSize.x)][1+int(gl_LocalInvocationID.y)] = imageLoad(img_psi_in, ivec2(iright, global_index.y)).x;
    }

    // and the top and bottom
    if (int(gl_LocalInvocationID.y) == 0) {
        // only some threads do this

        int ileft = int(gl_WorkGroupID.y)*int(gl_WorkGroupSize.y) - 1;
        if (ileft < 0) ileft = imageSize(img_psi_in).y - 1;
        tmp[1+int(gl_LocalInvocationID.x)][0]                         = imageLoad(img_psi_in, ivec2(global_index.x, ileft)).x;

        int iright = int(1+gl_WorkGroupID.y)*int(gl_WorkGroupSize.y);
        if (iright > imageSize(img_psi_in).y-1) iright = 0;
        tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_WorkGroupSize.y)] = imageLoad(img_psi_in, ivec2(global_index.x, iright)).x;
    }

    // don't load the corners because we don't need them

    groupMemoryBarrier();
    barrier();

    // perform some number of iterations
    //for (int i = 0; i < 10; ++i) {

        // right-hand side
        vec4 temp = imageLoad(img_vort_in, global_index);
        float psi_new = temp.x * pow(1.0/imageSize(img_psi_in).x, 2);

        // psi <- stuff
        psi_new += tmp[1+int(gl_LocalInvocationID.x)][0+int(gl_LocalInvocationID.y)];
        psi_new += tmp[1+int(gl_LocalInvocationID.x)][2+int(gl_LocalInvocationID.y)];
        psi_new += tmp[0+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)];
        psi_new += tmp[2+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)];

    //}

    imageStore(img_psi_out, global_index, vec4(0.25*psi_new, 0, 0, 1));
}
]]

-- perform one step of a relaxation scheme for the Poisson equation
local img_relax_many_blocked_shared = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

shared float tmp[gl_WorkGroupSize.x+2][gl_WorkGroupSize.y+2];

void main() {
    ivec2 global_index = ivec2(gl_GlobalInvocationID.xy);
    ivec2 local_index = ivec2( int(gl_WorkGroupID.x)*int(gl_WorkGroupSize.x) + int(gl_LocalInvocationID.x),
                               int(gl_WorkGroupID.y)*int(gl_WorkGroupSize.y) + int(gl_LocalInvocationID.y) );

    // load psi_in into shared memory

    // first, the middle (most of the loads)
    tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)] = imageLoad(img_psi_in, global_index).x;

    // then the left and right sides (this assumes work groups fit perfectly into overall dimensions)

    if (int(gl_LocalInvocationID.x) == 0) {
        // only some threads do this

        int ileft = int(gl_WorkGroupID.x)*int(gl_WorkGroupSize.x) - 1;
        if (ileft < 0) ileft = imageSize(img_psi_in).x - 1;
        tmp[0][1+int(gl_LocalInvocationID.y)]                         = imageLoad(img_psi_in, ivec2(ileft, global_index.y)).x;

        int iright = int(1+gl_WorkGroupID.x)*int(gl_WorkGroupSize.x);
        if (iright > imageSize(img_psi_in).x-1) iright = 0;
        tmp[1+int(gl_WorkGroupSize.x)][1+int(gl_LocalInvocationID.y)] = imageLoad(img_psi_in, ivec2(iright, global_index.y)).x;
    }

    // and the top and bottom
    if (int(gl_LocalInvocationID.y) == 0) {
        // only some threads do this

        int ileft = int(gl_WorkGroupID.y)*int(gl_WorkGroupSize.y) - 1;
        if (ileft < 0) ileft = imageSize(img_psi_in).y - 1;
        tmp[1+int(gl_LocalInvocationID.x)][0]                         = imageLoad(img_psi_in, ivec2(global_index.x, ileft)).x;

        int iright = int(1+gl_WorkGroupID.y)*int(gl_WorkGroupSize.y);
        if (iright > imageSize(img_psi_in).y-1) iright = 0;
        tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_WorkGroupSize.y)] = imageLoad(img_psi_in, ivec2(global_index.x, iright)).x;
    }

    // don't load the corners because we don't need them

    //groupMemoryBarrier();
    //memoryBarrierShared();
    barrier();

    float thisvort = imageLoad(img_vort_in, global_index).x * pow(1.0/imageSize(img_psi_in).x, 2);
    float psi_new = 0.0;

    // perform some number of iterations
    for (int i = 0; i < int(gl_WorkGroupSize.x); ++i) {
    //for (int i = 0; i < 2; ++i) {

        // right-hand side
        psi_new = thisvort;

        // add all neibs
        psi_new += tmp[1+int(gl_LocalInvocationID.x)][0+int(gl_LocalInvocationID.y)];
        psi_new += tmp[1+int(gl_LocalInvocationID.x)][2+int(gl_LocalInvocationID.y)];
        psi_new += tmp[0+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)];
        psi_new += tmp[2+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)];

        // wait for all procs to complete
        //groupMemoryBarrier();
        //memoryBarrierShared();
        barrier();

        // save back in shared array
        tmp[1+int(gl_LocalInvocationID.x)][1+int(gl_LocalInvocationID.y)] = 0.25*psi_new;
        //groupMemoryBarrier();
        barrier();
    }

    imageStore(img_psi_out, global_index, vec4(0.25*psi_new, 0, 0, 1));
}
]]

-- Find the residual r = f - L u, all textures are images and the same resolution
local img_compute_residual = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_rhs_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_rhs_out;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    float lu = 4.0 * imageLoad(img_psi_in, pixel_coords).x;

    // psi <- stuff
    int ishift = pixel_coords.y - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).y;
    lu -= imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift)).x;

    ishift = pixel_coords.y + 1;
    ishift %= imageSize(img_psi_in).y;
    lu -= imageLoad(img_psi_in, ivec2(pixel_coords.x, ishift)).x;

    ishift = pixel_coords.x - 1;
    if (ishift < 0) ishift += imageSize(img_psi_in).x;
    lu -= imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y)).x;

    ishift = pixel_coords.x + 1;
    ishift %= imageSize(img_psi_in).x;
    lu -= imageLoad(img_psi_in, ivec2(ishift, pixel_coords.y)).x;

    // is there an rsqrt function?
    float dxs = 1.0 / pow(imageSize(img_psi_in).x, 2.0);
    float rhs = imageLoad(img_rhs_in, pixel_coords).x;

    imageStore(img_rhs_out, pixel_coords, vec4(rhs - dxs*lu, 0, 0, 1));
}
]]

-- relax at the finest level to solution - only one work group here
local img_solve_coarsest = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_vort_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_psi_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_psi_out;

shared float tmp[gl_WorkGroupSize.x][gl_WorkGroupSize.y];

void main() {
    ivec2 indx = ivec2(gl_LocalInvocationID.xy);

    // load psi_in into shared memory
    tmp[indx.x][indx.y] = imageLoad(img_psi_in, indx).x;

    // then the left and right sides (this assumes work groups fit perfectly into overall dimensions)

    // wait until all memory accesses are done
    barrier();

    // shifted coordinates of finer mesh
    int inxp1 = indx.x + 1;
    int inxm1 = indx.x - 1;
    int inyp1 = indx.y + 1;
    int inym1 = indx.y - 1;

    // account for the periodic domain
    if (indx.x == 0) inxm1 = imageSize(img_psi_in).x - 1;
    if (indx.y == 0) inym1 = imageSize(img_psi_in).y - 1;
    if (indx.x == imageSize(img_psi_in).x-1) inxp1 = 0;
    if (indx.y == imageSize(img_psi_in).y-1) inyp1 = 0;

    float thisvort = imageLoad(img_vort_in, indx).x * pow(1.0/imageSize(img_psi_in).x, 2);
    float psi_new = 0.0;

    // perform some number of iterations
    for (int i = 0; i < int(gl_WorkGroupSize.x); ++i) {

        // right-hand side
        psi_new = thisvort;

        // add all neibs
        psi_new += tmp[indx.x][inym1];
        psi_new += tmp[indx.x][inyp1];
        psi_new += tmp[inxm1][indx.y];
        psi_new += tmp[inxp1][indx.y];

        // wait for all threads to complete
        barrier();

        // save back in shared array
        tmp[indx.x][indx.y] = 0.25*psi_new;

        // wait again
        barrier();
    }

    imageStore(img_psi_out, indx, vec4(0.25*psi_new, 0, 0, 1));
}
]]

-- coarse-to-fine operator - use texture lookup to get bilinear interpolation
--  run one thread per *output* (fine) pixel
local img_interpolate_up = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D coarse_in;
layout(r32f, binding = 1) uniform readonly restrict image2D fine_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D fine_out;

void main() {
    ivec2 indx = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    // note that we need to add 1.0 to the fine pixel index so that index 0
    //   on the fine grid exactly samples index 0 on the coarse grid, because
    //   that's how we did restriction!
    vec2 tc = vec2( float(1.0+indx.x) / float(imageSize(fine_out).x),
                    float(1.0+indx.y) / float(imageSize(fine_out).y) );
    float pixel = texture(coarse_in, tc).x;

    // and the solution at this level
    float fineval = imageLoad(fine_in, indx).x;

    imageStore(fine_out, indx, vec4(pixel+fineval, 0, 0, 1));
}
]]

-- perform second-order, two-point gradient calculation to find velocity
local img_psi_to_vel = [[
#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_psi_in;
layout(rg32f, binding = 1) uniform writeonly restrict image2D img_vel_out;

//const float TWOPI = 2.0*3.1415926535897932384626433832795;

void main() {
    float ootdx = imageSize(img_psi_in).x / 2.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // u = d psi / dy
    int ileft = pixel_coords.y - 1;
    if (ileft < 0) ileft += imageSize(img_psi_in).y;
    int iright = pixel_coords.y + 1;
    iright %= imageSize(img_psi_in).y;
    vec4 lval = imageLoad(img_psi_in, ivec2(pixel_coords.x, ileft));
    vec4 rval = imageLoad(img_psi_in, ivec2(pixel_coords.x, iright));
    float u = ootdx * (lval.x - rval.x);

    // v = - d psi / dx
    ileft = pixel_coords.x - 1;
    if (ileft < 0) ileft += imageSize(img_psi_in).x;
    iright = pixel_coords.x + 1;
    iright %= imageSize(img_psi_in).x;
    lval = imageLoad(img_psi_in, ivec2(ileft, pixel_coords.y));
    rval = imageLoad(img_psi_in, ivec2(iright, pixel_coords.y));
    float v = -ootdx * (lval.x - rval.x);

    // for testing, just prescribe the velocities
    //u = cos(TWOPI*float(pixel_coords.y)/float(imageSize(img_psi_in).y));
    //v = -sin(TWOPI*float(pixel_coords.x)/float(imageSize(img_psi_in).x));

    imageStore(img_vel_out, pixel_coords, vec4(u, v, 0, 1));
}
]]

-- perform second-order, two-point gradient calculation to find velocity
local img_psi_to_vel_blocked = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(r32f, binding = 0) uniform readonly restrict image2D img_psi_in;
layout(rg32f, binding = 1) uniform writeonly restrict image2D img_vel_out;

void main() {
    // 1 flop in this section
    float ootdx = imageSize(img_psi_in).x / 2.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // u = d psi / dy
    // 2 loads, 2 flops, 4 iops
    int ileft = pixel_coords.y - 1;
    if (ileft < 0) ileft += imageSize(img_psi_in).y;
    int iright = pixel_coords.y + 1;
    iright %= imageSize(img_psi_in).y;
    float lval = imageLoad(img_psi_in, ivec2(pixel_coords.x, ileft)).x;
    float rval = imageLoad(img_psi_in, ivec2(pixel_coords.x, iright)).x;
    float u = ootdx * (lval - rval);

    // v = - d psi / dx
    // 2 loads, 2 flops, 4 iops
    ileft = pixel_coords.x - 1;
    if (ileft < 0) ileft += imageSize(img_psi_in).x;
    iright = pixel_coords.x + 1;
    iright %= imageSize(img_psi_in).x;
    lval = imageLoad(img_psi_in, ivec2(ileft, pixel_coords.y)).x;
    rval = imageLoad(img_psi_in, ivec2(iright, pixel_coords.y)).x;
    float v = -ootdx * (lval - rval);

    // with this line, 4 loads, 2 stores, 5 flops, 8 iops
    imageStore(img_vel_out, pixel_coords, vec4(u, v, 0, 1));
}
]]

-- semi-lagrangian advection, 1st order in time, 1st order in space
local comp_sla_euler_lin = [[
#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(r32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    float pixel = 0.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 col = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    tc -= dt*col.xy;
    col = texture(tex_scalar_in, tc);
    pixel = col.x;

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, vec4(pixel, 0, 0, 1));
}
]]

-- semi-lagrangian advection, 2nd order in time, 1st order in space
local comp_sla_rk2_lin = [[
#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(r32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    float pixel = 0.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);
    vec4 col = texture(tex_scalar_in, tc2);
    pixel = col.x;

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, vec4(pixel, 0, 0, 1));
}
]]

-- semi-lagrangian advection for scalar float, 2nd order in time, 1st order in space
local comp_sla_rk2_lin_blocked = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(r32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    float pixel = 0.0;
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);
    vec4 col = texture(tex_scalar_in, tc2);
    pixel = col.x;

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, vec4(pixel, 0, 0, 1));
}
]]

-- semi-lagrangian advection for rgba8, 2nd order in time, 1st order in space
local comp_sla_rk2_lin_blocked_rgba8 = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(rgba8, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);
    vec4 pixel = texture(tex_scalar_in, tc2);

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, pixel);
}
]]

-- semi-lagrangian advection for rgba32f, 2nd order in time, 1st order in space
local comp_sla_rk2_lin_blocked_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(rgba32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);
    vec4 pixel = texture(tex_scalar_in, tc2);

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, pixel);
}
]]

-- semi-lagrangian advection for rgba32f, 2nd order in time, ? order in space
local comp_sla_rk2_m4h_blocked_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(rgba32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);

    // now use a hack of M4' to pull the color
    float dx = 1.0 / float(imageSize(img_scalar_out).x);
    vec4 pixel = 0.015625 * texture(tex_scalar_in, tc2 + dx*vec2(-1.0, -1.0))
               + 0.015625 * texture(tex_scalar_in, tc2 + dx*vec2(+1.0, -1.0))
               + 0.015625 * texture(tex_scalar_in, tc2 + dx*vec2(-1.0, +1.0))
               + 0.015625 * texture(tex_scalar_in, tc2 + dx*vec2(+1.0, +1.0))
               + -0.15625 * texture(tex_scalar_in, tc2 + dx*vec2(+0.0, +1.0))
               + -0.15625 * texture(tex_scalar_in, tc2 + dx*vec2(+0.0, -1.0))
               + -0.15625 * texture(tex_scalar_in, tc2 + dx*vec2(+1.0, +0.0))
               + -0.15625 * texture(tex_scalar_in, tc2 + dx*vec2(-1.0, +0.0))
               +   1.5625 * texture(tex_scalar_in, tc2) ;

    //float dc = 0.01;
    //vec4 pixel = (1.0+4.0*dc) * texture(tex_scalar_in, tc2)
    //           - dc * texture(tex_scalar_in, tc2 + dx*vec2(+0.0, +1.0))
    //           - dc * texture(tex_scalar_in, tc2 + dx*vec2(+0.0, -1.0))
    //           - dc * texture(tex_scalar_in, tc2 + dx*vec2(+1.0, +0.0))
    //           - dc * texture(tex_scalar_in, tc2 + dx*vec2(-1.0, +0.0)) ;

    pixel = clamp(pixel, vec4(0, 0, 0, 0), vec4(1, 1, 1, 1));

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, pixel);
}
]]

-- semi-lagrangian advection for rgba32f, 2nd order in time, 4th order in space
local comp_sla_rk2_m4p_blocked_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(rgba32f, binding = 1) uniform readonly restrict image2D img_scalar_in;
layout(rgba32f, binding = 2) uniform writeonly restrict image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the velocity there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);

    // now use real M4' to pull the color
    vec2 xc = tc2 * float(imageSize(img_scalar_in).x) - vec2(0.5);
    ivec2 ic = ivec2(floor(xc));
    vec2 dx = xc - vec2(ic);

    // the M4' weights
    vec4 xwt = vec4( 0.5*pow(1-dx.x,2)*(-dx.x),
                     1.0 - dx.x*dx.x*(2.5 - 1.5*dx.x),
                     1.0 - pow(1-dx.x,2)*(1.0 + 1.5*dx.x),
                     0.5*dx.x*dx.x*(dx.x-1) );
    vec4 ywt = vec4( 0.5*pow(1-dx.y,2)*(-dx.y),
                     1.0 - dx.y*dx.y*(2.5 - 1.5*dx.y),
                     1.0 - pow(1-dx.y,2)*(1.0 + 1.5*dx.y),
                     0.5*dx.y*dx.y*(dx.y-1) );

    // the indices
    ivec4 ix = ivec4(mod( vec4(ic.x-1, ic.x, ic.x+1, ic.x+2), vec4(imageSize(img_scalar_in).x) ));
    ivec4 iy = ivec4(mod( vec4(ic.y-1, ic.y, ic.y+1, ic.y+2), vec4(imageSize(img_scalar_in).y) ));

    // up to here: 136 flops and 4 loads

    // this is 64 loads and 140 flops
    vec4 pixel = xwt.x * ywt.x * imageLoad(img_scalar_in, ivec2(ix.x, iy.x))
               + xwt.y * ywt.x * imageLoad(img_scalar_in, ivec2(ix.y, iy.x))
               + xwt.z * ywt.x * imageLoad(img_scalar_in, ivec2(ix.z, iy.x))
               + xwt.w * ywt.x * imageLoad(img_scalar_in, ivec2(ix.w, iy.x))
               + xwt.x * ywt.y * imageLoad(img_scalar_in, ivec2(ix.x, iy.y))
               + xwt.y * ywt.y * imageLoad(img_scalar_in, ivec2(ix.y, iy.y))
               + xwt.z * ywt.y * imageLoad(img_scalar_in, ivec2(ix.z, iy.y))
               + xwt.w * ywt.y * imageLoad(img_scalar_in, ivec2(ix.w, iy.y))
               + xwt.x * ywt.z * imageLoad(img_scalar_in, ivec2(ix.x, iy.z))
               + xwt.y * ywt.z * imageLoad(img_scalar_in, ivec2(ix.y, iy.z))
               + xwt.z * ywt.z * imageLoad(img_scalar_in, ivec2(ix.z, iy.z))
               + xwt.w * ywt.z * imageLoad(img_scalar_in, ivec2(ix.w, iy.z))
               + xwt.x * ywt.w * imageLoad(img_scalar_in, ivec2(ix.x, iy.w))
               + xwt.y * ywt.w * imageLoad(img_scalar_in, ivec2(ix.y, iy.w))
               + xwt.z * ywt.w * imageLoad(img_scalar_in, ivec2(ix.z, iy.w))
               + xwt.w * ywt.w * imageLoad(img_scalar_in, ivec2(ix.w, iy.w))
    ;

    // 8 more flops
    pixel = clamp(pixel, vec4(0, 0, 0, 0), vec4(1, 1, 1, 1));

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, pixel);

    // total is 68 loads, 4 stores, 284 flops
}
]]

-- semi-lagrangian advection for r32f, 2nd order in time, 4th order in space
local comp_sla_rk2_m4p_blocked_r32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(r32f, binding = 1) uniform readonly restrict image2D img_scalar_in;
layout(r32f, binding = 2) uniform writeonly restrict image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location (4 loads, 22 flops)
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the velocity there (4 loads, 22 flops)
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity (8 flops)
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);

    // now use real M4' to pull the color (8 flops)
    vec2 xc = tc2 * float(imageSize(img_scalar_in).x) - vec2(0.5);
    ivec2 ic = ivec2(floor(xc));
    vec2 dx = xc - vec2(ic);

    // the M4' weights (38 flops)
    vec4 xwt = vec4( 0.5*pow(1-dx.x,2)*(-dx.x),
                     1.0 - dx.x*dx.x*(2.5 - 1.5*dx.x),
                     1.0 - pow(1-dx.x,2)*(1.0 + 1.5*dx.x),
                     0.5*dx.x*dx.x*(dx.x-1) );
    vec4 ywt = vec4( 0.5*pow(1-dx.y,2)*(-dx.y),
                     1.0 - dx.y*dx.y*(2.5 - 1.5*dx.y),
                     1.0 - pow(1-dx.y,2)*(1.0 + 1.5*dx.y),
                     0.5*dx.y*dx.y*(dx.y-1) );

    // the indices (38 flops)
    ivec4 ix = ivec4(mod( vec4(ic.x-1, ic.x, ic.x+1, ic.x+2), vec4(imageSize(img_scalar_in).x) ));
    ivec4 iy = ivec4(mod( vec4(ic.y-1, ic.y, ic.y+1, ic.y+2), vec4(imageSize(img_scalar_in).y) ));

    // 47 flops, 16 loads
    float pixel = xwt.x * ywt.x * imageLoad(img_scalar_in, ivec2(ix.x, iy.x)).x
                + xwt.y * ywt.x * imageLoad(img_scalar_in, ivec2(ix.y, iy.x)).x
                + xwt.z * ywt.x * imageLoad(img_scalar_in, ivec2(ix.z, iy.x)).x
                + xwt.w * ywt.x * imageLoad(img_scalar_in, ivec2(ix.w, iy.x)).x
                + xwt.x * ywt.y * imageLoad(img_scalar_in, ivec2(ix.x, iy.y)).x
                + xwt.y * ywt.y * imageLoad(img_scalar_in, ivec2(ix.y, iy.y)).x
                + xwt.z * ywt.y * imageLoad(img_scalar_in, ivec2(ix.z, iy.y)).x
                + xwt.w * ywt.y * imageLoad(img_scalar_in, ivec2(ix.w, iy.y)).x
                + xwt.x * ywt.z * imageLoad(img_scalar_in, ivec2(ix.x, iy.z)).x
                + xwt.y * ywt.z * imageLoad(img_scalar_in, ivec2(ix.y, iy.z)).x
                + xwt.z * ywt.z * imageLoad(img_scalar_in, ivec2(ix.z, iy.z)).x
                + xwt.w * ywt.z * imageLoad(img_scalar_in, ivec2(ix.w, iy.z)).x
                + xwt.x * ywt.w * imageLoad(img_scalar_in, ivec2(ix.x, iy.w)).x
                + xwt.y * ywt.w * imageLoad(img_scalar_in, ivec2(ix.y, iy.w)).x
                + xwt.z * ywt.w * imageLoad(img_scalar_in, ivec2(ix.z, iy.w)).x
                + xwt.w * ywt.w * imageLoad(img_scalar_in, ivec2(ix.w, iy.w)).x
    ;

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, vec4(pixel, 0, 0, 1));

    // total: 183 flops, 20 loads, 1 store
}
]]

-- pixel-wise addition of mouse-drag vorticity
local comp_drag_linear_blocked = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSX) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(r32f, binding = 1) uniform restrict image2D img_vort_inout;

uniform int nsegs;
uniform float xseg[128];

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_vort_inout).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_vort_inout).y) );

    const float width = 0.003;
    const float dt = 0.1;

    // read the original vorticity value
    float pixel = imageLoad(img_vort_inout, pixel_coords).x;

    // loop over segments
    for (int i=0; i<nsegs; ++i) {

        const vec2 x1 = vec2(xseg[2*i+0], xseg[2*i+1]);
        const vec2 x2 = vec2(xseg[2*i+2], xseg[2*i+3]);
        const vec2 dx = x2 - x1;

        // find distance to the line segment
        const float l2 = pow(dx.x, 2) + pow(dx.y, 2);

        // as long as segment is not zero-length
        if (l2 > 0.0) {

            // the number of mouse actions per frame is highly variable!
            //const float speed = sqrt(l2) / float(nsegs);
            const float speed = sqrt(l2);

            const float t = dot(tc - x1, dx) / l2;
            if (t > 0.0 && t < 1.0) {

                // proj is the point on the line that is closest
                const vec2 proj = x1 + t * dx;

                // sample velocity at the current location (2 loads, ? flops)
                //vec2 vel = dt * texture(tex_vel_in, tc).xy;

                // distance to line
                const vec2 vectoline = proj - tc;
                const float dist = length(vectoline);
                float mag = 0.0;
                if (dist < width) {
                    mag = dist;
                } else if (dist < 2.0*width) {
                    mag = 2.0 * width - dist;
                }

                // scale it
                //mag = float(imageSize(img_vort_inout).x) * 0.5 * speed * mag / width;
                mag = 1536.0 * 0.5 * speed * mag / width;

                // cross-product to line
                const float cp = dx.y*vectoline.x - dx.x*vectoline.y;

                // add to it
                if (cp < 0.0) pixel = pixel + mag;
                else if (cp > 0.0) pixel = pixel - mag;
            }
        }
    }

    // store the new vorticity value in the output buffer
    imageStore(img_vort_inout, pixel_coords, vec4(pixel, 0, 0, 1));
}
]]


function poisson_multigrid:initTriAttributes()
    local glIntv = ffi.typeof('GLint[?]')
    local glUintv = ffi.typeof('GLuint[?]')

    local verts = glFloatv(4*3, {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
    })

    local vpos_loc = gl.glGetAttribLocation(self.prog_draw, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog_draw, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function poisson_multigrid:initTextureImage(w, h, ifmt, dtyp, efmt)
    gl.glActiveTexture(GL.GL_TEXTURE0)
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, ifmt,
                    w, h, 0,
                    efmt, dtyp, nil)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    gl.glBindImageTexture(0, texID, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, ifmt)

    return texID
end

function poisson_multigrid:initializeTextures()
    for k,v in pairs(self.field) do
        print("poisson_multigrid.initializeTextures initializing (",k,")")

        -- generate and set the tid
        local texID = self:initTextureImage(v.x, v.y, v.ifmt, GL.GL_FLOAT, v.efmt)
        v.tid = texID

        -- set the initialization program
        if k == "vort" then v.init_prog = self.prog_init_r32f end
        if k == "psi" then v.init_prog = self.prog_init_r32f end
        if k == "color" then v.init_prog = self.prog_init_rgba32f end
    end

    -- no initialization programs needed for the coarse multigrid levels
    for _,v in pairs(self.rhs) do
        local texID = self:initTextureImage(v.x, v.y, v.ifmt, GL.GL_FLOAT, v.efmt)
        v.tid = texID
    end
    for _,v in pairs(self.psi) do
        local texID = self:initTextureImage(v.x, v.y, v.ifmt, GL.GL_FLOAT, v.efmt)
        v.tid = texID
        v.init_prog = self.prog_init_r32f
    end
    for _,v in pairs(self.tmp) do
        local texID = self:initTextureImage(v.x, v.y, v.ifmt, GL.GL_FLOAT, v.efmt)
        v.tid = texID
    end
end

function poisson_multigrid:adjustColor(brite, contrast)
    local v = self.field.color

    gl.glBindImageTexture(0, v.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_WRITE, v.ifmt)
    local thisprog = self.prog_mod_rgba32f
    gl.glUseProgram(thisprog)

    local brite_loc = gl.glGetUniformLocation(thisprog, "brite_in")
    gl.glUniform1f(brite_loc, brite)
    local contr_loc = gl.glGetUniformLocation(thisprog, "contrast_in")
    gl.glUniform1f(contr_loc, contrast)

    gl.glDispatchCompute(v.x/self.gridx, v.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
end

function poisson_multigrid:loadTextureIntoColor(overlayTexId)
    --print("loadTextureIntoColor taking tid ",overlayTexId," over color")
    local v = self.field.color

    gl.glBindImageTexture(1, v.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, v.ifmt)
    gl.glUseProgram(v.init_prog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, overlayTexId)
    local tex_loc0 = gl.glGetUniformLocation(v.init_prog, "tex_in")
    gl.glUniform1i(tex_loc0, 0)

    local sfill_loc = gl.glGetUniformLocation(v.init_prog, "uFillType")
    --gl.glUniform1i(sfill_loc, 0)
    gl.glUniform1i(sfill_loc, 5)

    gl.glDispatchCompute(v.x/self.gridx, v.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
end

function poisson_multigrid:clearTextures(condition, overlayTexId)
    for k,v in pairs(self.field) do
        if v.init_prog ~= nil then
            print("clearTextures resetting (",k,") tid is",v.tid)

            local fill_type = 0
            if k == "vort" then fill_type = self.sim end
            if k == "color" then fill_type = self.colsim end

            gl.glBindImageTexture(1, v.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, v.ifmt)
            gl.glUseProgram(v.init_prog)

            -- if a texture ID is given in the args, overlay that
            if overlayTexId ~= nil then
                gl.glActiveTexture(GL.GL_TEXTURE0)
                gl.glBindTexture(GL.GL_TEXTURE_2D, overlayTexId)
                local tex_loc0 = gl.glGetUniformLocation(v.init_prog, "tex_in")
                gl.glUniform1i(tex_loc0, 0)
                -- make sure to override the fill type
                fill_type = 5
            end

            local sfill_loc = gl.glGetUniformLocation(v.init_prog, "uFillType")
            gl.glUniform1i(sfill_loc, fill_type)

            gl.glDispatchCompute(v.x/self.gridx, v.y/self.gridx, 1)
            gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
        end
    end
end

function poisson_multigrid:clearSolution()
    -- clear out psi
    gl.glBindImageTexture(1, self.field.psi.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.psi.ifmt)
    gl.glUseProgram(self.field.psi.init_prog)
    local sfill_loc = gl.glGetUniformLocation(self.field.psi.init_prog, "uFillType")
    gl.glUniform1i(sfill_loc, 0)
    gl.glDispatchCompute(self.field.psi.x/self.gridx, self.field.psi.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    for _,v in pairs(self.psi) do
        if v.init_prog ~= nil then
            gl.glBindImageTexture(1, v.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, v.ifmt)
            gl.glUseProgram(v.init_prog)

            local sfill_loc = gl.glGetUniformLocation(v.init_prog, "uFillType")
            local fill_type = 0
            gl.glUniform1i(sfill_loc, fill_type)

            gl.glDispatchCompute(v.x/self.gridx, v.y/self.gridx, 1)
            gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
        end
    end
end

function poisson_multigrid:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    -- two draw programs, one for rg and rgb buffers, another for just r
    self.prog_draw = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self.prog_twochan = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = twochan_frag,
        })

    self.prog_scalar = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = red_frag,
        })

    -- two init programs for setting and resetting the fields
    self.prog_init_rgba8 = sf.make_shader_from_source({
        compsrc = string.gsub(img_init_rgba8, "GSX", self.gridx, 2),
        })

    self.prog_init_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(img_init_rgba32f, "GSX", self.gridx, 2),
        })

    self.prog_mod_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(img_mod_rgba32f, "GSX", self.gridx, 2),
        })

    self.prog_init_r32f = sf.make_shader_from_source({
        compsrc = string.gsub(img_init_r32f, "GSX", self.gridx, 2),
        })

    -- and our solver programs
    self.prog_restrict_blocked = sf.make_shader_from_source({
        compsrc = string.gsub(img_restrict_blocked, "GSX", self.gridx, 2),
        })

    self.prog_relax_once = sf.make_shader_from_source({
        compsrc = img_relax_once,
        })

    self.prog_relax_once_blocked = sf.make_shader_from_source({
        compsrc = string.gsub(img_relax_once_blocked, "GSX", self.gridx, 2),
        })

    self.prog_relax_once_blocked_shared = sf.make_shader_from_source({
        compsrc = string.gsub(img_relax_once_blocked_shared, "GSX", self.gridx, 2),
        })

    self.prog_relax_many_blocked_shared = sf.make_shader_from_source({
        compsrc = string.gsub(img_relax_many_blocked_shared, "GSX", self.gridx, 2),
        })

    self.prog_compute_residual = sf.make_shader_from_source({
        compsrc = string.gsub(img_compute_residual, "GSX", self.gridx, 2),
        })

    self.prog_solve_coarsest = sf.make_shader_from_source({
        compsrc = string.gsub(img_solve_coarsest, "GSX", self.gridx, 2),
        })

    self.prog_interpolate_up = sf.make_shader_from_source({
        compsrc = string.gsub(img_interpolate_up, "GSX", self.gridx, 2),
        })

    self.prog_psi_to_vel = sf.make_shader_from_source({
        compsrc = img_psi_to_vel,
        })

    self.prog_psi_to_vel_blocked = sf.make_shader_from_source({
        compsrc = string.gsub(img_psi_to_vel_blocked, "GSX", self.gridx, 2),
        })

    self.prog_sladvect_1 = sf.make_shader_from_source({
        compsrc = comp_sla_euler_lin,
        })

    self.prog_sladvect_2 = sf.make_shader_from_source({
        compsrc = comp_sla_rk2_lin,
        })

    self.prog_sladvect_2_blocked = sf.make_shader_from_source({
        compsrc = string.gsub(comp_sla_rk2_lin_blocked, "GSX", self.gridx, 2),
        })

    self.prog_sladvect_2_blocked_rgba8 = sf.make_shader_from_source({
        compsrc = string.gsub(comp_sla_rk2_lin_blocked_rgba8, "GSX", self.gridx, 2),
        })

    self.prog_sladvect_2_blocked_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(comp_sla_rk2_lin_blocked_rgba32f, "GSX", self.gridx, 2),
        })

    self.prog_sladvect_3_blocked_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(comp_sla_rk2_m4h_blocked_rgba32f, "GSX", self.gridx, 2),
        })

    self.prog_sladvect_4_blocked_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(comp_sla_rk2_m4p_blocked_rgba32f, "GSX", self.gridx, 2),
        })

    self.prog_sladvect_4_blocked_r32f = sf.make_shader_from_source({
        compsrc = string.gsub(comp_sla_rk2_m4p_blocked_r32f, "GSX", self.gridx, 2),
        })

    self.prog_draw_vort_linear_blocked = sf.make_shader_from_source({
        compsrc = string.gsub(comp_drag_linear_blocked, "GSX", self.gridx, 2),
        })

    -- init the meshes and textures
    self:initTriAttributes()
    self:initializeTextures(0)
    self:clearTextures(0)

    gl.glBindVertexArray(0)
end

function poisson_multigrid:advectScalars(dt)
    -- first-order semi-Lagrangian advection
    --local thisprog = self.prog_sladvect_1
    --local thisprog = self.prog_sladvect_2
    --local thisprog = self.prog_sladvect_2_blocked
    local thisprog = self.prog_sladvect_4_blocked_r32f
    gl.glUseProgram(thisprog)

    -- first, advect vorticity
    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.vel.tid)
    local tex_loc0 = gl.glGetUniformLocation(thisprog, "tex_vel_in")
    gl.glUniform1i(tex_loc0, 0)

    gl.glBindImageTexture(1, self.field.vort.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.vort.ifmt)

    gl.glBindImageTexture(2, self.field.tscal.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.tscal.ifmt)

    local dt_loc = gl.glGetUniformLocation(thisprog, "dt")
    gl.glUniform1f(dt_loc, dt)

    gl.glDispatchCompute(self.field.vort.x/self.gridx, self.field.vort.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- now swap the buffers
    local tempid = self.field.vort.tid
    self.field.vort.tid = self.field.tscal.tid
    self.field.tscal.tid = tempid

    -- then, advect color
    thisprog = self.prog_sladvect_4_blocked_rgba32f
    gl.glUseProgram(thisprog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.vel.tid)
    local tex_loc0 = gl.glGetUniformLocation(thisprog, "tex_vel_in")
    gl.glUniform1i(tex_loc0, 0)

    -- load the old color as an image
    gl.glBindImageTexture(1, self.field.color.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.color.ifmt)

    gl.glBindImageTexture(2, self.field.tcol.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.tcol.ifmt)

    local dt_loc = gl.glGetUniformLocation(thisprog, "dt")
    gl.glUniform1f(dt_loc, dt)

    gl.glDispatchCompute(self.field.color.x/self.gridx, self.field.color.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- now swap the buffers
    local tempid = self.field.color.tid
    self.field.color.tid = self.field.tcol.tid
    self.field.tcol.tid = tempid
end

function poisson_multigrid:jacobiSolver()
    --gl.glUseProgram(self.prog_relax_once)
    --gl.glUseProgram(self.prog_relax_once_blocked)
    --gl.glUseProgram(self.prog_relax_once_blocked_shared)
    gl.glUseProgram(self.prog_relax_many_blocked_shared)

    -- keep reading from the (constant) rhs vector
    gl.glBindImageTexture(0, self.field.vort.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.vort.ifmt)

    -- loop, relax, and swap buffers
    for i=1,10*self.field.psi.x do
    --for i=1,10 do
        -- bind the last two images
        gl.glBindImageTexture(1, self.field.psi.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.psi.ifmt)
        gl.glBindImageTexture(2, self.field.tpsi.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.tpsi.ifmt)

        -- run the computation
        gl.glDispatchCompute(self.field.psi.x/self.gridx, self.field.psi.y/self.gridx, 1)
        gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

        -- swap the buffers
        local tempid = self.field.psi.tid
        self.field.psi.tid = self.field.tpsi.tid
        self.field.tpsi.tid = tempid
    end
end

function poisson_multigrid:multigridSolver()
    -- first take: perform one V-cycle
    local num_presmooth = 1
    local num_postsmooth = 1

    -- need this to reject initial noise and stabilize solution
    self:clearSolution()

    -- first, restrict the vort/residual/rhs, psi, and temp all the way down to the coarsest level
    local finer = self.field.vort
    local finep = self.field.psi
    local finet = self.field.tpsi
    local coarser = nil
    local coarsep = nil
    local coarset = nil

    for ilev=1,self.levels do

        -- run pre-smoothing on the fine grid
        for ismo=1,num_presmooth do
            -- ping
            gl.glUseProgram(self.prog_relax_once_blocked)
            gl.glBindImageTexture(0, finer.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finer.ifmt)
            gl.glBindImageTexture(1, finep.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finep.ifmt)
            gl.glBindImageTexture(2, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finet.ifmt)
            gl.glDispatchCompute(finep.x/self.gridx, finep.y/self.gridx, 1)
            gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

            -- pong back to finep
            gl.glBindImageTexture(1, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finet.ifmt)
            gl.glBindImageTexture(2, finep.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finep.ifmt)
            gl.glDispatchCompute(finep.x/self.gridx, finep.y/self.gridx, 1)
            gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
        end

        -- compute the residual (put it into temp)
        gl.glUseProgram(self.prog_compute_residual)
        gl.glBindImageTexture(0, finer.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finer.ifmt)
        gl.glBindImageTexture(1, finep.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finep.ifmt)
        gl.glBindImageTexture(2, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finet.ifmt)
        gl.glDispatchCompute(finep.x/self.gridx, finep.y/self.gridx, 1)
        gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

        -- run the restriction on the residual AND copy it back to finer
        coarser = self.rhs[ilev]
        gl.glUseProgram(self.prog_restrict_blocked)
        gl.glBindImageTexture(0, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finet.ifmt)
        gl.glBindImageTexture(1, coarser.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, coarser.ifmt)
        --gl.glBindImageTexture(1, finer.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finer.ifmt)
        gl.glDispatchCompute(coarser.x/self.gridx, coarser.y/self.gridx, 1)
        gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

        -- get ready for the next level
        coarsep = self.psi[ilev]
        coarset = self.tmp[ilev]
        finer = coarser
        finep = coarsep
        finet = coarset
    end

    -- then solve at the coarsest level
    gl.glUseProgram(self.prog_solve_coarsest)
    gl.glBindImageTexture(0, finer.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finer.ifmt)
    gl.glBindImageTexture(1, finep.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finep.ifmt)
    gl.glBindImageTexture(2, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finet.ifmt)
    gl.glDispatchCompute(1, 1, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- now, solution at coarsest level is stored in finet

    -- prepare for the upward pass
    coarser = finer
    coarsep = finep
    coarset = finet

    -- finally, interpolate this solution all the way up to the finest grid
    for i=1,self.levels do
        ilev = self.levels - i

        if ilev == 0 then
            -- this is the last upward prolongation, use the final arrays
            finer = self.field.vort
            finep = self.field.psi
            finet = self.field.tpsi
        else
            finer = self.rhs[ilev]
            finep = self.psi[ilev]
            finet = self.tmp[ilev]
        end

        -- interpolate the coarse solution and add this level's solution
        gl.glUseProgram(self.prog_interpolate_up)
        gl.glActiveTexture(GL.GL_TEXTURE0)
        gl.glBindTexture(GL.GL_TEXTURE_2D, coarset.tid)
        local tex_loc0 = gl.glGetUniformLocation(self.prog_interpolate_up, "coarse_in")
        gl.glUniform1i(tex_loc0, 0)
        gl.glBindImageTexture(1, finep.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finep.ifmt)
        gl.glBindImageTexture(2, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finet.ifmt)
        gl.glDispatchCompute(finep.x/self.gridx, finep.y/self.gridx, 1)
        gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

        -- and some smoothing steps
        for ismo=1,num_postsmooth do
            -- ping
            gl.glUseProgram(self.prog_relax_once_blocked)
            gl.glBindImageTexture(0, finer.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finer.ifmt)
            gl.glBindImageTexture(1, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finet.ifmt)
            gl.glBindImageTexture(2, finep.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finep.ifmt)
            gl.glDispatchCompute(finep.x/self.gridx, finep.y/self.gridx, 1)
            gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

            -- pong
            gl.glBindImageTexture(1, finep.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finep.ifmt)
            gl.glBindImageTexture(2, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finet.ifmt)
            gl.glDispatchCompute(finep.x/self.gridx, finep.y/self.gridx, 1)
            gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
        end

        -- prepare for the next level
        coarser = finer
        coarsep = finep
        coarset = finet
    end

    -- finally, do one smoothing to get the answer back into psi
    gl.glUseProgram(self.prog_relax_once_blocked)
    gl.glBindImageTexture(0, finer.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finer.ifmt)
    gl.glBindImageTexture(1, finet.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, finet.ifmt)
    gl.glBindImageTexture(2, finep.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, finep.ifmt)
    gl.glDispatchCompute(finep.x/self.gridx, finep.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- we should have an answer now
end

function poisson_multigrid:solveForVelocity()

    -- first, perform Jacobi relaxation to find psi (streamfunction) from vorticity
    --self:jacobiSolver()
    self:multigridSolver()

    -- then, perform 2nd order derivatives to find velocity from that
    gl.glBindImageTexture(0, self.field.psi.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.psi.ifmt)
    gl.glBindImageTexture(1, self.field.vel.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.vel.ifmt)

    gl.glUseProgram(self.prog_psi_to_vel_blocked)
    gl.glDispatchCompute(self.field.vel.x/self.gridx, self.field.vel.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    --gl.glUseProgram(0)
end

-- use the self.mousevec to generate new vorticity
function poisson_multigrid:mouseToColor()
    --print("poisson_multigrid.mouseToColor processing ", self.nmousept, " points")

    -- what we do depends on the current and previous number of points
    if self.mousing then
        -- we drew lines last step, so we have one point in the array
        if self.nmousept == 0 then
            -- shouldn't happen
            self.mousing = false
            return
        elseif self.nmousept == 1 then
            -- no new points, we stop drawing
            self.mousing = false
            self.nmousept = 0
            return
        else
            -- there are new points, draw n-1 segments
        end

    else
        -- we did not draw last step
        if self.nmousept == 0 then
            -- no points, still not drawing
            return
        elseif self.nmousept == 1 then
            -- one new point, save it, and try to draw next step
            self.mousing = true
            return
        else
            -- more than one new point, draw n-1 segments
            self.mousing = true
        end
    end

    -- if we are here, we are going to draw n-1 segments
    print("poisson_multigrid.mouseToColor drawing ", (self.nmousept-1), " segments")

    -- upload the mouse movement array and array size

    -- call the shader to modify the vorticity
    local thisprog = self.prog_draw_vort_linear_blocked
    gl.glUseProgram(thisprog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.vel.tid)
    local tex_loc0 = gl.glGetUniformLocation(thisprog, "tex_vel_in")
    gl.glUniform1i(tex_loc0, 0)

    gl.glBindImageTexture(1, self.field.vort.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_WRITE, self.field.vort.ifmt)

    local ns_loc = gl.glGetUniformLocation(thisprog, "nsegs")
    gl.glUniform1i(ns_loc, self.nmousept-1)

    local xm_loc = gl.glGetUniformLocation(thisprog, "xseg")
    gl.glUniform1fv(xm_loc, 2*self.nmousept, self.mousevec)

    gl.glDispatchCompute(self.field.vort.x/self.gridx, self.field.vort.y/self.gridx, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- copy the last mouse point to the first position and "remove" the rest
    self.mousevec[0] = self.mousevec[2*self.nmousept-2]
    self.mousevec[1] = self.mousevec[2*self.nmousept-1]
    self.nmousept = 1
end

function poisson_multigrid:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    -- delete programs
    gl.glDeleteProgram(self.prog_draw)
    gl.glDeleteProgram(self.prog_twochan)
    gl.glDeleteProgram(self.prog_scalar)
    gl.glDeleteProgram(self.prog_init_rgba8)
    gl.glDeleteProgram(self.prog_init_rgba32f)
    gl.glDeleteProgram(self.prog_mod_rgba32f)
    gl.glDeleteProgram(self.prog_init_r32f)
    gl.glDeleteProgram(self.prog_restrict_blocked)
    gl.glDeleteProgram(self.prog_relax_once)
    gl.glDeleteProgram(self.prog_relax_once_blocked)
    gl.glDeleteProgram(self.prog_relax_once_blocked_shared)
    gl.glDeleteProgram(self.prog_relax_many_blocked_shared)
    gl.glDeleteProgram(self.prog_compute_residual)
    gl.glDeleteProgram(self.prog_solve_coarsest)
    gl.glDeleteProgram(self.prog_interpolate_up)
    gl.glDeleteProgram(self.prog_psi_to_vel)
    gl.glDeleteProgram(self.prog_psi_to_vel_blocked)
    gl.glDeleteProgram(self.prog_sladvect_1)
    gl.glDeleteProgram(self.prog_sladvect_2)
    gl.glDeleteProgram(self.prog_sladvect_2_blocked)
    gl.glDeleteProgram(self.prog_sladvect_2_blocked_rgba8)
    gl.glDeleteProgram(self.prog_sladvect_2_blocked_rgba32f)
    gl.glDeleteProgram(self.prog_sladvect_3_blocked_rgba32f)
    gl.glDeleteProgram(self.prog_sladvect_4_blocked_rgba32f)
    gl.glDeleteProgram(self.prog_sladvect_4_blocked_r32f)
    gl.glDeleteProgram(self.prog_draw_vort_linear_blocked)

    -- delete textures
    for _,v in pairs(self.field) do
        local tid = ffi.new("GLuint[1]", v.tid)
        gl.glDeleteTextures(1,tid)
    end
    for _,v in pairs(self.rhs) do
        local tid = ffi.new("GLuint[1]", v.tid)
        gl.glDeleteTextures(1,tid)
    end
    for _,v in pairs(self.psi) do
        local tid = ffi.new("GLuint[1]", v.tid)
        gl.glDeleteTextures(1,tid)
    end
    for _,v in pairs(self.tmp) do
        local tid = ffi.new("GLuint[1]", v.tid)
        gl.glDeleteTextures(1,tid)
    end
    -- clean up
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function poisson_multigrid:render_one_texture(tex, prog, view, proj)

    gl.glUseProgram(prog)
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, tex.tid)
    local stex_loc = gl.glGetUniformLocation(prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    local to_loc = gl.glGetUniformLocation(prog, "texMag")
    local val_mag = 1.0
    if tex.mag ~= nil then val_mag = tex.mag end
    gl.glUniform1f(to_loc, val_mag)

    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4)
end

function poisson_multigrid:render_for_one_eye(view, proj)

    gl.glBindVertexArray(self.vao)

    local vp = ffi.new("int[4]")
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    local win_w,win_h = vp[2]-vp[0], vp[3]-vp[1]
    local aspect = win_w / win_h

    gl.glClearColor(0.0, 0.0, 0.0, 0.0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    -- orthographic draw
    local newproj = {}
    mm.glh_ortho(newproj, -aspect, aspect, -1, 1, 0.1, 10.0)

    local newview = {}
    mm.make_identity_matrix(newview)
    local s = aspect
    if self.mode == 0 then
        if aspect > 2.0 then
            s = 2.0
        end
    elseif self.mode == 1 then
        if aspect < 2.0 then
            s = 2.0
        end
    elseif self.mode == 2 then
        s = aspect * 2.0
        if aspect < 1.0 then
            s = 2.0
        end
    end
    mm.glh_scale(newview, s, s, s)

    if self.mode < 2 then
        -- draw both
        mm.glh_translate(newview, 0, -0.5, -1)
        self:render_one_texture(self.field.vort, self.prog_scalar, newview, newproj)

        mm.glh_translate(newview, -1, 0, 0)
        self:render_one_texture(self.field.color, self.prog_draw, newview, newproj)

    else
        -- draw only the color scalar
        mm.glh_translate(newview, -0.5, -0.5, -1)
        self:render_one_texture(self.field.color, self.prog_draw, newview, newproj)
    end


    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function poisson_multigrid:timestep(absTime, dt)
    if not self.pause then
        -- advect vorticity, scalar, and color
        self:advectScalars(self.dt)
        -- process mouse drag array
        self:mouseToColor()
        -- calculate new velocities
        self:solveForVelocity()
        --print("poisson_multigrid.timestep", self.dt)
    end
end

function poisson_multigrid:keypressed(ch, tid)
    if ch == string.byte(' ') then
        self.pause = not self.pause

    elseif ch == string.byte('=') then
        if tid ~= nil then
            self:loadTextureIntoColor(tid)
        end

    elseif ch == string.byte('[') then
        self.dt = self.dt / 1.2

    elseif ch == string.byte(']') then
        self.dt = self.dt * 1.2

    elseif ch == string.byte('W') then
        -- adjust brightness of color
        self:adjustColor(0.1, 0.0)

    elseif ch == string.byte('S') then
        -- adjust brightness of color
        self:adjustColor(-0.1, 0.0)

    elseif ch == string.byte('A') then
        -- adjust color contrast
        self:adjustColor(0.0, -0.1)

    elseif ch == string.byte('D') then
        -- adjust color contrast
        self:adjustColor(0.0, 0.1)

    elseif ch == string.byte('M') then
        -- jump through various display modes
        --   0  color on left, vorticity on right, always fully fit in screen (allow bars)
        --   1  color on left, vorticity on right, crop to fill screen
        --   2  color only, crop to fill screen
        self.mode = self.mode + 1
        if self.mode == 3 then
            self.mode = 0
        end
        print("poisson_multigrid:keypressed now using mode", self.mode)

    end

end

function poisson_multigrid:onmouse(x, y)
    --multiple of these can trigger in the span of a time step
    if (self.nmousept == self.maxmpts) then return end

    local vp = ffi.new("int[4]")
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    local win_w,win_h = vp[2]-vp[0], vp[3]-vp[1]
    local aspect = win_w / win_h

    --print("poisson_multigrid.onmouse", x, y, aspect)
    newx = x
    newy = y
    if self.mode == 2 then
        if aspect > 1.0 then
            newy = 0.5 + (y-0.5)/aspect
        else
            newx = 0.5 + (x-0.5)*aspect
        end
    --else
    end
    self.mousevec[self.nmousept*2+0] = newx
    self.mousevec[self.nmousept*2+1] = newy
    self.nmousept = self.nmousept + 1
end

function poisson_multigrid:onSingleTouch(pointerid, action, x, y)
    --print("poisson_multigrid.onSingleTouch",pointerid, action, x, y)
end

return poisson_multigrid
