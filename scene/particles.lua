--[[ particles_scene.lua

]]
particles = {}

particles.__index = particles

function particles.new(...)
    local self = setmetatable({}, particles)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function particles:init()
    self.vbos = {}
    self.vao = 0
    self.progs = {}
    self.numPoints = 1024 --*1024
    self.time = 0
    self.lightData = {}
end

require("util.objfile")
local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

--[[
    Drawing instanced cubes: simple color per-vertex.
]]
local per_vertex_color_vert = [[
#version 330

in vec4 vPosition;
in vec4 vNormal;
in vec4 instancePosition;
in vec4 instanceOrientation;

out vec3 vfWorldPos;
out vec3 vfNormal;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;
uniform float instScale;

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

void main()
{
    vec3 pos = vPosition.xyz;
    vec4 q = normalize(instanceOrientation);
    pos = qtransform(q, pos);
    pos *= instScale;
    pos += instancePosition.xyz;
    vfWorldPos = (mmtx * vec4(pos,1.)).xyz;
    vfNormal = normalize((mat3(mmtx)) * qtransform(q,vNormal.xyz));
    gl_Position = prmtx * vmtx * mmtx * vec4(pos, 1.);
}
]]

local per_vertex_color_frag = [[
#version 330

in vec3 vfWorldPos;
in vec3 vfNormal;
out vec4 fragColor;

float shininess = 125.;

struct SpotLight {
    vec4 pos;
    vec4 dir;
    vec4 diffCol;
    float intensity;
    float cutoff;
};

const int numberOfLights = 1;
layout (std140) uniform Light
{
    SpotLight lights[numberOfLights];
} Lgt;

vec3 calculateSpotLight(vec3 pos, vec3 normal)
{
    vec3 sumColor = vec3(0.);
    for (int i=0; i<numberOfLights; ++i)
    {
        SpotLight spot = Lgt.lights[i];
        vec3 lightDir = normalize(vec3(spot.pos.xyz - pos));
        vec3 eye = vec3(0.);

        float spec = 0.;

        if (dot(-spot.dir.xyz,lightDir) > spot.cutoff)
        {
            spot.intensity = max(dot(normal,lightDir), 0.);
            if (spot.intensity > 0.)
            {
                vec3 h = normalize(lightDir + eye);
                float intSpec = max(dot(h,normal), 0.);
                spec = pow(intSpec, shininess);
            }
        }
        sumColor += vec3(spot.intensity * spot.diffCol + spec * spot.diffCol);
    }
    return sumColor;
}

void main()
{
    //fragColor = vec4(vfNormal, 1.0);
    fragColor = vec4(calculateSpotLight(vfWorldPos, vfNormal), 1.);
}
]]

function particles:setDataDirectory(dir)
    self.data_dir = dir
end

function particles:loadModel(filename)
    self.obj = objfile.new()
    if self.data_dir then filename = self.data_dir .. "/" .. filename end
    print("Loading obj "..filename)
    self.obj:loadmodel(filename)
    local str = (#self.obj.vertlist/4).." vertices*3, "
    str = str..(#self.obj.normlist/4).." normals*3, "
    str = str..(#self.obj.texlist/4).." texcoords*3, "
    str = str..(#self.obj.idxlist/4).." ints*3 for triangle indices."
    print(str)

    -- Compute aabb of obj
    if true then
        local nv = #self.obj.vertlist/3
        local aabbmin = {}
        local LARGEVAL = 999999999
        self.obj.aabb = {
            min={LARGEVAL,LARGEVAL,LARGEVAL,},
            max={-LARGEVAL,-LARGEVAL,-LARGEVAL,},
        }
        for i=1,nv-1 do
            local vo = 3*i
            for j=1,3 do
                self.obj.aabb.min[j] = math.min(self.obj.aabb.min[j], self.obj.vertlist[vo+j-1])
                self.obj.aabb.max[j] = math.max(self.obj.aabb.max[j], self.obj.vertlist[vo+j-1])
            end
        end
        print("AABB min: ",
            self.obj.aabb.min[1],
            self.obj.aabb.min[2],
            self.obj.aabb.min[3]
            )
        print("AABB max: ",
            self.obj.aabb.max[1],
            self.obj.aabb.max[2],
            self.obj.aabb.max[3]
            )
        self.objscale = 0
        for j=1,3 do
            self.objscale = math.max(self.objscale, self.obj.aabb.max[j]-self.obj.aabb.min[j])
        end
        print("Obj scale: ",self.objscale)
    end
end

function particles:initModelAttributes()
    --print("Number of objects: "..#self.obj.olist)
    if #self.obj.olist == 0 then return end

    local v = self.obj.vertlist
    local n = self.obj.normlist
    local i = self.obj.idxlist

    local verts = glFloatv(#v,v)
    local norms = glFloatv(#n,n)

    local prog = self.progs.color
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.originalTris = vvbo

    gl.glEnableVertexAttribArray(vpos_loc)

    local vnorm_loc = gl.glGetAttribLocation(prog, "vNormal")
    if vnorm_loc > -1 then
        local nvbo = glIntv(0)
        gl.glGenBuffers(1, nvbo)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, nvbo[0])
        gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(norms), norms, GL.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(vnorm_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
        self.vbos.originalNorms = nvbo

        gl.glEnableVertexAttribArray(vnorm_loc)
    end

    local lines = glUintv(#i,i)
    local ivbo = glIntv(0)
    gl.glGenBuffers(1, ivbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, ivbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(lines), lines, GL.GL_STATIC_DRAW)
    self.vbos.triIndices = ivbo
end

-- A simple cube mesh with per-vertex colors.
function particles:init_single_instance_attributes()
    self:init_single_instance_attributes_from_array()
    --self:init_single_instance_attributes_from_obj("sphere2")
end

function particles:init_single_instance_attributes_from_array()
    local verts = {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,

        0,0,1,
        1,0,1,
        1,1,1,
        0,1,1,

        0,0,0,
        1,0,0,
        1,0,1,
        0,0,1,

        0,1,0,
        1,1,0,
        1,1,1,
        0,1,1,

        0,0,0,
        0,1,0,
        0,1,1,
        0,0,1,

        1,0,0,
        1,1,0,
        1,1,1,
        1,0,1,
    }
    for i=1,#verts do verts[i] = verts[i]-.5 end
    local vertsArray = glFloatv(#verts, verts)

    local norms = {
        0,0,-1,
        0,0,-1,
        0,0,-1,
        0,0,-1,

        0,0,1,
        0,0,1,
        0,0,1,
        0,0,1,

        0,-1,0,
        0,-1,0,
        0,-1,0,
        0,-1,0,

        0,1,0,
        0,1,0,
        0,1,0,
        0,1,0,

        -1,0,0,
        -1,0,0,
        -1,0,0,
        -1,0,0,

        1,0,0,
        1,0,0,
        1,0,0,
        1,0,0,
    }
    local normsArray = glFloatv(#norms, norms)

    local prog = self.progs.color
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(prog, "vNormal")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(vertsArray), vertsArray, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(normsArray), normsArray, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local tris = {
        0,3,2, 1,0,2,
        4,5,6, 7,4,6,

        8,11,10, 9,8,10,
        12,13,14, 15,12,14,

        16,19,18, 17,16,18,
        20,21,22, 23,20,22,
    }
    local trisArray = glUintv(#tris, tris)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(trisArray), trisArray, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)

    self.numInstTris = #tris
end

--[[
"cube",
"tetrahedron",
"octahedron",
"sphere",
"sphere2",
"Biplane2",
"NYERSEY_",
"Submarine2",
"hull22",
]]
function particles:init_single_instance_attributes_from_obj(objname)
    self:loadModel("models/"..objname..".obj")
    self:initModelAttributes()
    self.numInstTris = #self.obj.idxlist
end

--[[
    Scatter instance positions randomly in particles array.
]]
local randomize_instance_positions_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numPoints;

float hash( float n ) { return fract(sin(n)*43758.5453); }

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numPoints)
        return;
    float fi = float(index+1) / float(numPoints);

    positions[index] = vec4(hash(fi*2.3), hash(fi*4.1), hash(fi*5.3), 1.);

    orientations[index] = vec4(hash(fi*4.3), hash(fi*2.9), hash(fi*7.), hash(fi*4.7));

    vec3 randv = vec3(hash(fi*4.9), hash(fi*1.9), hash(fi*3.7));
    vec3 motiondir = 2.*(vec3(-.5) + randv); // isotropic, go in all directions
    motiondir = pow(length(motiondir), 7.) * normalize(motiondir);
    velocities[index] = vec4(.05*motiondir, 0.);

    vec3 randav = vec3(hash(fi*6.1), hash(fi*5.9), hash(fi*3.1));
    vec3 rotangle = 2.*(vec3(-.5) + randav); // isotropic, go in all directions
    rotangle = pow(rotangle, vec3(13.));
    angularVelocities[index] = normalize(vec4(rotangle, 30.));
}
]]
function particles:randomize_instance_positions()
    if self.progs.scatter_instances == nil then
        self.progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end
    local pvbo = self.vbos.inst_positions
    local ovbo = self.vbos.inst_orientations
    local vvbo = self.vbos.inst_velocities
    local avvbo = self.vbos.inst_angularvelocities
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, avvbo[0])
    local prog = self.progs.scatter_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniform1i(uni("numPoints"), self.numPoints)
    gl.glDispatchCompute(self.numPoints/256+1, 1, 1)
    gl.glUseProgram(0)
end

--[[
    Apply angular velocity every timestep
]]
local instance_timestep_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numPoints;
uniform float dt;

// http://www.gamedev.net/reference/articles/article1095.asp
//w = w1w2 - x1x2 - y1y2 - z1z2
//x = w1x2 + x1w2 + y1z2 - z1y2
//y = w1y2 + y1w2 + z1x2 - x1z2
//z = w1z2 + z1w2 + x1y2 - y1x2
// Remember the w coordinate is last!
vec4 qconcat3(vec4 a, vec4 b)
{
    return vec4(
        a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y,
        a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z,
        a.w*b.z + a.z*b.w + a.x*b.y - a.y*b.x,
        a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z
        );
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numPoints)
        return;

    // Position
    vec4 p = positions[index];
    vec3 v = velocities[index].xyz;
    p.xyz += dt * v.xyz;

    // Boundary conditions
    if (p.x < 0.)
    {
        p.x = 0.;
        v = reflect(v, vec3(1.,0.,0.));
    }
    if (p.x > 1.)
    {
        p.x = 1.;
        v = reflect(v, vec3(-1.,0.,0.));
    }
    if (p.y < 0.)
    {
        p.y = 0.;
        v = reflect(v, vec3(0.,1.,0.));
    }
    if (p.y > 1.)
    {
        p.y = 1.;
        v = reflect(v, vec3(0.,-1.,0.));
    }
    if (p.z < 0.)
    {
        p.z = 0.;
        v = reflect(v, vec3(0.,0.,1.));
    }
    if (p.z > 1.)
    {
        p.z = 1.;
        v = reflect(v, vec3(0.,0.,-1.));
    }

    positions[index] = p;
    velocities[index].xyz = v;

    // Rotation
    vec4 q = orientations[index];
    q = normalize(qconcat3(q, dt * angularVelocities[index]));
    orientations[index] = q;
}
]]
function particles:timestep_instances(dt)
    if self.progs.step_instances == nil then
        self.progs.step_instances = sf.make_shader_from_source({
            compsrc = instance_timestep_comp_src,
            })
    end
    local prog = self.progs.step_instances
    local function uni(name) return gl.glGetUniformLocation(prog, name) end

    local pvbo = self.vbos.inst_positions
    local ovbo = self.vbos.inst_orientations
    local vvbo = self.vbos.inst_velocities
    local avvbo = self.vbos.inst_angularvelocities
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, 3, avvbo[0])
    gl.glUseProgram(prog)
    gl.glUniform1i(uni("numPoints"), self.numPoints)
    gl.glUniform1f(uni("dt"), dt)
    gl.glDispatchCompute(self.numPoints/256+1, 1, 1)
    gl.glUseProgram(0)
end

function particles:init_per_instance_attributes()
    local prog = self.progs.color
    local sz = 4 * self.numPoints * ffi.sizeof('GLfloat') -- xyzw

    local insp_loc = gl.glGetAttribLocation(prog, "instancePosition")
    local ipvbo = glIntv(0)
    gl.glGenBuffers(1, ipvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, ipvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_COPY)
    gl.glVertexAttribPointer(insp_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_positions = ipvbo
    gl.glVertexAttribDivisor(insp_loc, 1)
    gl.glEnableVertexAttribArray(insp_loc)

    local inso_loc = gl.glGetAttribLocation(prog, "instanceOrientation")
    local iovbo = glIntv(0)
    gl.glGenBuffers(1, iovbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, iovbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(inso_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.inst_orientations = iovbo
    gl.glVertexAttribDivisor(inso_loc, 1)
    gl.glEnableVertexAttribArray(inso_loc)

    -- Velocities and angular velocities are not drawn; used only by compute.
    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    self.vbos.inst_velocities = vvbo

    -- Angular velocities are not drawn; used only by compute.
    local avvbo = glIntv(0)
    gl.glGenBuffers(1, avvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, avvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, sz, nil, GL.GL_STATIC_DRAW)
    self.vbos.inst_angularvelocities = avvbo
end

function particles:initGL()
    self.progs.color = sf.make_shader_from_source({
        vsrc = per_vertex_color_vert,
        fsrc = per_vertex_color_frag,
        })

    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)
    do
        self:init_single_instance_attributes()
        self:init_per_instance_attributes()
        self:randomize_instance_positions()
    end

    do
        local lv = {}
        local numLights = 1
        local numflts = numLights*(4*4) -- sizeof light struct with padding
        local ubo = glIntv(0)
        gl.glGenBuffers(1, ubo)
        gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, ubo[0])
        gl.glBufferData(GL.GL_UNIFORM_BUFFER, 4*numflts, nil, GL.GL_DYNAMIC_DRAW)
        self.uniformbuf = ubo

        local prog = self.progs.color
        local lightBlock = gl.glGetUniformBlockIndex(prog, "Light")
        print("lb:",lightBlock)
        gl.glUniformBlockBinding(prog, lightBlock, 0)
        gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])
        --gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)
    end

    gl.glBindVertexArray(0)
end

function particles:exitGL()
    for _,p in pairs(self.progs) do
        gl.glDeleteProgram(p)
    end
    self.progs = {}

    gl.glBindVertexArray(self.vao)
    do
        for _,v in pairs(self.vbos) do
            gl.glDeleteBuffers(1,v)
        end
        self.vbos = {}
    end
    gl.glBindVertexArray(0)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function particles:renderEye(model, view, proj)
    local prog = self.progs.color
    local function uni(name) return gl.glGetUniformLocation(prog, name) end
    gl.glUseProgram(prog)
    gl.glUniformMatrix4fv(uni("prmtx"), 1, GL.GL_FALSE, glFloatv(16, proj))
    
    local m = {}
    for i=1,16 do m[i] = model[i] end
    local s = 10
    mm.glh_scale(m, s,s,s)
    mm.glh_translate(m, -.5, -.5, -.5)

    gl.glUniformMatrix4fv(uni("mmtx"), 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glUniformMatrix4fv(uni("vmtx"), 1, GL.GL_FALSE, glFloatv(16, view))

    local sc = .3 / math.pow(self.numPoints,1/3)
    gl.glUniform1f(uni("instScale"), sc)

    do
        -- Flatten array of light values
        local ldata = {}
        for _,v in pairs(self.lightData) do
            for i=1,#v do
                table.insert(ldata, v[i])
            end
        end
        local lightArr = glFloatv(#ldata, ldata)
        gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, self.uniformbuf[0])
        gl.glBufferSubData(GL.GL_UNIFORM_BUFFER, 0, ffi.sizeof(lightArr), lightArr)
        gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)

        local prog = self.progs.color
        local lightBlock = gl.glGetUniformBlockIndex(prog, "Light")
        gl.glUniformBlockBinding(prog, lightBlock, 0)
        gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])
    end

    gl.glBindVertexArray(self.vao)
    gl.glDrawElementsInstanced(GL.GL_TRIANGLES, self.numInstTris, GL.GL_UNSIGNED_INT, nil, self.numPoints)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function particles:timestep(absTime, dt)
    self:timestep_instances(dt+.000000001)

    do
        -- Animate lighting data
        local r = 3
        local speed = 2
        local time = absTime --self.time
        local x,y,z = r*math.sin(speed*time),5,r*math.cos(speed*time)
        local dir = {.5*-x,-y,.5*-z}
        mm.normalize(dir)
        --print(dir[1], dir[2], dir[3])
        self.lightData = {
            -- Light 1
            {
                x,y,z,0,
                dir[1], dir[2], dir[3],0,
                1,1,0,0,
                0,.75,0,0, -- NOTE: std140 layout adds padding to 16-byte boundaries
            },
        }
    end
end

function particles:rescaleInstances()
    --print(self.numPoints)
    gl.glBindVertexArray(self.vao)
    do
        gl.glDeleteBuffers(1,self.vbos.inst_positions)
        gl.glDeleteBuffers(1,self.vbos.inst_orientations)
        gl.glDeleteBuffers(1,self.vbos.inst_velocities)
        gl.glDeleteBuffers(1,self.vbos.inst_angularvelocities)

        self:init_per_instance_attributes()
        self:randomize_instance_positions()
    end
    gl.glBindVertexArray(0)
end

function particles:keypressed(key, scancode, action, mods)
    if key == 45 then
        self.numPoints = self.numPoints / 2
        self:rescaleInstances()
    elseif key == 61 then
        self.numPoints = self.numPoints * 2
        self:rescaleInstances()
    end
end

function particles:getNumPoints() return self.numPoints end

return particles
