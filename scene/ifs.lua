--[[ ifs.lua

    A simple iterated fractal system, recursively drawing
    self-similar copies.
]]
ifs = {}

ifs.__index = ifs

function ifs.new(...)
    local self = setmetatable({}, ifs)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function ifs:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
end

--local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]


local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]

function ifs:init_tri_attributes()
    local verts = glFloatv(3*3, {
        -1,0,0,
        1,0,0,
        0,1,0,
        })

    local cols = glFloatv(3*3, {
        0,0,0,
        1,0,0,
        0,1,0,
        })

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(cols), cols, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function ifs:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:init_tri_attributes()
    gl.glBindVertexArray(0)
end

function ifs:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function ifs:draw_recurse(view, proj, depth)
    if depth <= 0 then return end

    gl.glUseProgram(self.prog)
    local umv_loc = gl.glGetUniformLocation(self.prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))
    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_TRIANGLES, 0, 3)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)

    -- Draw 2 children
    local s = .5
    local r = 45
    local t = {.38, .45, .01}
    if depth > 1 then
        local m = {}
        for i=1,16 do m[i] = view[i] end
        mm.glh_translate(m, -t[1], t[2], t[3])
        mm.glh_rotate(m, r, 0,0,1)
        mm.glh_scale(m,s,s,s)
        self:draw_recurse(m, proj, depth-1)
    end

    if depth > 1 then
        local m = {}
        for i=1,16 do m[i] = view[i] end
        mm.glh_translate(m, t[1], t[2], t[3])
        mm.glh_rotate(m, -r, 0,0,1)
        mm.glh_scale(m,s,s,s)
        self:draw_recurse(m, proj, depth-1)
    end
end

function ifs:render_for_one_eye(view, proj)
    self:draw_recurse(view, proj, 5)
end

function ifs:timestep(absTime, dt)
end

function ifs:onSingleTouch(pointerid, action, x, y)
    --print("ifs.onSingleTouch",pointerid, action, x, y)
end

return ifs
