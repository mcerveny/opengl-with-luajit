--[[ linegraph.lua

]]
linegraph = {}

linegraph.__index = linegraph

function linegraph.new(...)
    local self = setmetatable({}, linegraph)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function linegraph:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.npts = 32*8

    local tsLib = require("util.timeseries")
    self.ts = tsLib.new()
    self.ts:init("constrained random walk", 1)

    self.timescale = 1
    self.snd_duration = .2
end

--local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

package.path = package.path .. ';lib/?.lua'
local bass = require "bass"

local glIntv   = ffi.typeof('GLint[?]')
local glUintv  = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

in vec4 vPosition;

out vec3 vfColor;

uniform mat4 modelMtx;
uniform mat4 viewMtx;
uniform mat4 projMtx;

void main()
{
    vfColor = vec3(1.,1.,0.);

    // Stretch out a bit
    vec4 vpos = vPosition;
    vpos.x *= 3.;

    gl_Position =
        projMtx * viewMtx * modelMtx *
        vpos;
}
]]

local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]

function linegraph:init_line_attributes()
    pts = {}
    for i=0,self.npts-1 do
        local x01 = i/self.npts
        local x11 = 2*x01 - 1
        table.insert(pts, x11)
        table.insert(pts, self.ts:getval(x01))
        table.insert(pts, 0)
    end

    local verts = glFloatv(3*self.npts, pts)
    
    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
end


function linegraph:update_line_attributes()
    pts = {}
    for i=0,self.npts-1 do
        local x01 = i/self.npts
        local x11 = 2*x01 - 1
        table.insert(pts, x11)
        table.insert(pts, self.ts:getval(x01))
        table.insert(pts, 0)
    end
    local verts = glFloatv(3*self.npts, pts)

    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
end

function linegraph:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:init_line_attributes()
    gl.glBindVertexArray(0)

    local init_ret = bass.BASS_Init(-1, 44100, 0, 0, nil)
    print("BASS init: ", init_ret)
end

function linegraph:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)

    bass.BASS_Free()
end

function linegraph:renderEye(model, view, proj)
    local m = {}
    for i=1,16 do m[i] = model[i] end
    mm.glh_translate(m, -.5,-.5,-.5)

    local umm_loc = gl.glGetUniformLocation(self.prog, "modelMtx")
    local uvm_loc = gl.glGetUniformLocation(self.prog, "viewMtx")
    local upm_loc = gl.glGetUniformLocation(self.prog, "projMtx")
    gl.glUseProgram(self.prog)
    gl.glUniformMatrix4fv(umm_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glUniformMatrix4fv(uvm_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upm_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    --gl.glLineWidth(5.0)
    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_LINE_STRIP, 0, self.npts)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end



function playSoundForNSeconds(duration, ts)
    -- Header
    local rate = 44100
    local numchan = 1
    local numsamps = duration * rate
    local numsampsc = numsamps * numchan
    local samps = duration * rate*numchan * 100
    local byte_array = ffi.new("int[?]", samps)
    --[[
    struct wavfile_header {
        char    riff_tag[4];
        int riff_length;
        char    wave_tag[4];
        char    fmt_tag[4];
        int fmt_length;
        short   audio_format;
        short   num_channels;
        int sample_rate;
        int byte_rate;
        short   block_align;
        short   bits_per_sample;
        char    data_tag[4];
        int data_length;
    };
    ]]
    -- https://github.com/in4k/isystem1k4k/blob/master/i4k_OGLShader/src/main_win_deb.cpp
    byte_array[0] = 0x46464952
    byte_array[1] = numsampsc * 2 + 36
    byte_array[2] = 0x45564157
    byte_array[3] = 0x20746D66
    byte_array[4] = 16
    byte_array[5] = 1 + numchan*65536
    byte_array[6] = rate
    byte_array[7] = rate*numchan*2
    byte_array[8] = numchan*2 + (8*2)*0xFFFF
    byte_array[9] = 0x61746164
    byte_array[10] = numsampsc*2

    -- Sine wave
    local data_ptr = ffi.cast('unsigned short*', byte_array)
    local freq = 440
    local phase = 0
    local dt = 1/rate
    for i=0,numsampsc do
        local t = i/rate
        phase = phase + freq*dt
        local ampl = 0.25*0xFFFF*ts:getval(math.fmod(phase,1))
        data_ptr[i+(11*2)] = ampl
    end

    stream = bass.BASS_StreamCreateFile(1, byte_array, 0, numsampsc, 0)
    bass.BASS_Start()
    r = bass.BASS_ChannelPlay(stream, true)
end


function linegraph:rescale(factor)
    self.timescale = self.timescale * factor
    self.ts:init(self.ts.stype, self.timescale)
    self:update_line_attributes()
end

function linegraph:onwheel(x,y)
    local scale = math.pow(2, (1/12))
    if y > 0 then
        self:rescale(scale)
    elseif y < 0 then
        self:rescale(1/scale)
    end

    return true
end

function linegraph:keypressed(key, scancode, action, mods)
    local idx1 = key - 49 + 1
    seriestypes = {
        "uniform ramp",
        "fade in and out",
        "constrained random walk",
        "geiger counter" ,
        "shakespeare play",
        "sin wave",
    }
    local function reinit_line(stype)
        self.ts:init(stype, self.timescale)
        self:update_line_attributes()
    end

    for i=1,#seriestypes do
        if i == idx1 then
            reinit_line(seriestypes[i])
        end
    end

    local scale = math.pow(2, (1/12))
    if key == 61 then -- +
        self:rescale(scale)
    elseif key == 45 then -- -
        self:rescale(1/scale)
    end

    if key == 32 then
        playSoundForNSeconds(self.snd_duration, self.ts)
        self.just_played = true
    end
end

function linegraph:timestep(absTime, dt)
    if self.just_played then
        self.just_played = nil
        self.sound_start = absTime
    end
    if self.sound_start then
        if absTime - self.sound_start > self.snd_duration then
            bass.BASS_Stop()
            self.sound_start = nil
        end
    end
end

return linegraph
