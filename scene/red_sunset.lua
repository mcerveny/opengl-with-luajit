--[[ red_sunset.lua

]]
red_sunset = {}

red_sunset.__index = red_sunset

function red_sunset.new(...)
    local self = setmetatable({}, red_sunset)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function red_sunset:init()
    self.shader = nil
    self.time = 0
end

local ffi = require("ffi")
require("util.fullscreen_shader")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local rm_frag = [[
uniform float iGlobalTime;
uniform vec2 iResolution;

vec3 sunset( in vec2 uv )
{
    vec2 sunpos = vec2(.5, .05);
    vec3 hot = vec3(1.,1.,.65);
    vec3 orange = vec3(1.,.3,0.);
    vec3 red = vec3(.35,0.,0.);
    float d = 2.*length(uv-sunpos);
    float f = clamp(pow(1.9-d,3.),.0,1.);
    return mix(orange, hot, f)
         + mix(red, orange, uv.y-4.*.1);
}

void main()
{
    vec2 ac = vec2(3.*uv.x-1., 2.*uv.y-1.);
    fragColor = vec4(sunset(ac), 1.0);
}
]]

function red_sunset:initGL()
    self.shader = FullscreenShader.new(rm_frag)
    self.shader:initGL()
end

function red_sunset:exitGL()
    self.shader:exitGL()
end

function red_sunset:render_for_one_eye(view, proj)
    local function set_variables(prog)
        local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
        gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
        local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
        gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

        local ut_loc = gl.glGetUniformLocation(prog, "iGlobalTime")
        gl.glUniform1f(ut_loc, self.time)
        local ur_loc = gl.glGetUniformLocation(prog, "iResolution")
        gl.glUniform2f(ur_loc, 1600, 1200)
    end

    gl.glDisable(GL.GL_DEPTH_TEST)
    self.shader:render(view, proj, set_variables)
end

function red_sunset:timestep(absTime, dt)
    self.time = absTime
end

return red_sunset
