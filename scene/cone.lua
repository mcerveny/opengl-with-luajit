--[[ cone.lua

    Generate some simple geometry procedurally.
]]
cone = {}

cone.__index = cone

function cone.new(...)
    local self = setmetatable({}, cone)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function cone:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.time = 0
    self.fractal_depth = 5
end

--local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local Geom_Lib = require("util.geometry_functions")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;
uniform float time;

out vec3 vfColor;

vec3 rotateX(vec3 pt, float q)
{
    mat3 mtx = mat3(
        1., 0.,      0.,
        0., cos(q),  sin(q),
        0., -sin(q), cos(q)
        );
    return mtx * pt;
}

void main()
{
    vec3 pos = vPosition.xyz;
    //pos.z += sin(time);
    pos = rotateX(pos, .5*sin(time) * pos.z);

    vfColor = vColor.xyz;
    gl_Position = prmtx * vmtx * mmtx * vec4(pos,1.);
}
]]


local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.0);
}
]]

local function cheap_normalless_cone()
    local v,n,t,f = Geom_Lib.generate_cylinder(16,16)

    local num = #v/3

    -- Taper cone
    for i=1,num do
        v[3*i-2] = v[3*i-2] * v[3*i]
        v[3*i-1] = v[3*i-1] * v[3*i]
    end

    -- Scale and offset z
    for i=1,num do
        v[3*i] = v[3*i] - 1
        v[3*i] = v[3*i] * 3
    end

    return v,n,t,f
end

function cone:init_attributes()
    local v,n,t,f = cheap_normalless_cone()
    local verts = glFloatv(#v, v)
    local norms = glFloatv(#n, n)
    local texs = glFloatv(#t, t)
    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(texs), texs, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    self.numTris = #f
    local quads = glUintv(#f, f)
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)
end

function cone:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self:init_attributes()
    gl.glBindVertexArray(0)
end

function cone:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function cone:draw_recurse(model, view, proj, depth)
    if depth <= 0 then return end

    gl.glUseProgram(self.prog)
    local um_loc = gl.glGetUniformLocation(self.prog, "mmtx")
    local uv_loc = gl.glGetUniformLocation(self.prog, "vmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    local ut_loc = gl.glGetUniformLocation(self.prog, "time")
    gl.glUniform1f(ut_loc, self.time)

    gl.glBindVertexArray(self.vao)
    gl.glDrawElements(GL.GL_TRIANGLES, self.numTris, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)

    local viewr = {}
    for i=1,16 do viewr[i] = model[i] end
    mm.glh_rotate(viewr, (-180/3.14159)*.5*math.sin(self.time), 1,0,0)

    -- Draw 2 children
    local s = .65
    local r = 55
    local t = {.38, 0, -1.3}
    if depth > 1 then
        local m = {}
        for i=1,16 do m[i] = viewr[i] end
        mm.glh_translate(m, -t[1], t[2], t[3])
        mm.glh_rotate(m, r, 0,1,0)
        mm.glh_scale(m,s,s,s)
        self:draw_recurse(m, view, proj, depth-1)
    end

    if depth > 1 then
        local m = {}
        for i=1,16 do m[i] = viewr[i] end
        mm.glh_translate(m, t[1], t[2], t[3])
        mm.glh_rotate(m, -r, 0,1,0)
        mm.glh_scale(m,s,s,s)
        self:draw_recurse(m, view, proj, depth-1)
    end
end

function cone:renderEye(model, view, proj)
    self:draw_recurse(model, view, proj, self.fractal_depth)
end

function cone:timestep(absTime, dt)
    self.time = .3*absTime
end

function cone:keypressed(key, scancode, action, mods)
    if key == 45 then -- -(minus)
        self.fractal_depth = self.fractal_depth - 1
    elseif key == 61 then -- +
        self.fractal_depth = self.fractal_depth + 1
    end
    self.fractal_depth = math.max(self.fractal_depth, 1)
    self.fractal_depth = math.min(self.fractal_depth, 10)
end

return cone
