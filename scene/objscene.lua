--[[ objscene.lua

    Display obj models using matcap images.
    Depends on objs in data/models and matcaps in data/images.
    Obj loading is in util/objfile. 
]]
objscene = {}

objscene.__index = objscene

function objscene.new(...)
    local self = setmetatable({}, objscene)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end
    return self
end

function objscene:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.data_dir = nil

    self.obj = nil
    self.obj_idx = 0
    self.mat_idx = 0
    self.texID = 0
    self.time = 0
    self.sqrt_instances = 25
    self.lightData = {}
end

require("util.objfile")

--local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv   = ffi.typeof('GLint[?]')
local glUintv  = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

-- Debug display to highlight individual triangles.
local tricolor_vert = [[
#version 310 es

in vec3 vPosition;
in vec3 vNormal;

out vec3 vVertCol;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    int vertID3 = gl_VertexID % 3;

    vec3 col = vec3(1.,0.,0.);
    if (vertID3 == 1)
        col = vec3(0.,1.,0.);
    if (vertID3 == 2)
        col = vec3(0.,0.,1.);

    vVertCol = col;
    vVertCol = normalize(vNormal);

    vec3 pos = vPosition.xyz;
    gl_Position = prmtx * mvmtx * vec4(pos, 1.);
}
]]

local tricolor_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vVertCol;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vVertCol, 1.);
}
]]


local matcap_vert = [[
#version 310 es

in vec3 vPosition;
in vec3 vNormal;

out vec3 vVertCol;
out vec3 vEye;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    int vertID3 = gl_VertexID % 3;

    vec3 col = vec3(1.,0.,0.);
    if (vertID3 == 1)
        col = vec3(0.,1.,0.);
    if (vertID3 == 2)
        col = vec3(0.,0.,1.);

    vVertCol = col;

    vVertCol = normalize((mvmtx * vec4(vNormal,0.)).xyz);
    vEye = (mvmtx * vPosition).xyz;

    vec3 pos = vPosition.xyz;
    gl_Position = prmtx * mvmtx * vec4(pos, 1.);
}
]]

local matcap_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vVertCol;
in vec3 vEye;
out vec4 fragColor;

uniform sampler2D uMatCap;

void main()
{
    vec3 r = reflect( vEye, vVertCol );
    float m = 2. * sqrt(
        pow(r.x, 2.) +
        pow(r.y, 2.) +
        pow(r.z+1., 2.) );
    vec2 vN = r.xy / m + .5;

    vec3 base = texture( uMatCap, vN ).rgb;

    fragColor = vec4(base, 1. );
}
]]


--[[
Calculate normals per-face in geometry shader,
then plug into the matcap frag shader.
NOTE: will not work on OpenGL ES.
]]
local normcalc_vert = [[
#version 330

in vec3 vPosition;

flat out int vgInstanceId;

void main()
{
    vec3 pos = vPosition;
    vgInstanceId = gl_InstanceID;
    gl_Position = vec4(pos, 1.);
}
]]

local normcalc_geom = [[
#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

flat in int vgInstanceId[];
uniform int numInstances;

uniform mat4 modelMtx;
uniform mat4 viewMtx;
uniform mat4 projMtx;

out vec3 vWorldPos;
out vec3 vVertCol;
out vec3 vEye;

// Draw an origin-centered grid
// Returns per-instance offset, scaled by 1.
vec2 getGridOffset(int numInstances, int instanceId)
{
    int side = int(sqrt(numInstances));
    int halfSide = (side-1)/2;
    int row = instanceId % side;
    int col = instanceId / side;

    return vec2(float(row-halfSide), float(col - halfSide));
}

void main()
{
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;
    vec4 p2 = gl_in[2].gl_Position;

    vec3 U = p1.xyz - p0.xyz;
    vec3 V = p2.xyz - p0.xyz;
    vec3 norm = normalize(cross(U, V));

    mat3 normMtx = mat3(modelMtx);
    norm = normMtx * norm;

    mat4 mvMtx = viewMtx * modelMtx;
    mat4 mvpMtx = projMtx * mvMtx;
    mat4 vpMtx = projMtx * viewMtx;

    // Lay out instances in a grid
    float stepSz = 2.;
    vec2 off = stepSz * getGridOffset(numInstances, vgInstanceId[0]);

#if 1
    // Rotate each object about its own origin(position in grid)
    vec4 mp0 = modelMtx * p0;
    mp0.x += off.x;
    mp0.z += off.y;
    gl_Position = vpMtx * mp0;
    vWorldPos = mp0.xyz;
    vVertCol = norm;
    EmitVertex();

    vec4 mp1 = modelMtx * p1;
    mp1.x += off.x;
    mp1.z += off.y;
    gl_Position = vpMtx * mp1;
    vWorldPos = mp1.xyz;
    vVertCol = norm;
    EmitVertex();

    vec4 mp2 = modelMtx * p2;
    mp2.x += off.x;
    mp2.z += off.y;
    gl_Position = vpMtx * mp2;
    vWorldPos = mp2.xyz;
    vVertCol = norm;
    EmitVertex();
#else
    p0.xz += off;
    p1.xz += off;
    p2.xz += off;

    // Rotate the whole grid about the origin
    gl_Position = mvpMtx * p0;
    vVertCol = norm;
    vEye = (mvMtx * p0).xyz;
    EmitVertex();

    gl_Position = mvpMtx * p1;
    vVertCol = norm;
    vEye = (mvMtx * p1).xyz;
    EmitVertex();

    gl_Position = mvpMtx * p2;
    vVertCol = norm;
    vEye = (mvMtx * p2).xyz;
    EmitVertex();
#endif
    EndPrimitive();
}
]]

local lighting_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

const int numberOfLights = 3;
struct SpotLight {
    vec4 pos;
    vec4 dir;
    vec4 diffCol;
    float intensity;
    float cutoff;
};

layout (std140) uniform Light
{
    SpotLight lights[numberOfLights];
} Lgt;

uniform float time;
in vec3 vWorldPos;
in vec3 vVertCol; // normal
out vec4 fragColor;

float shininess = 125.;

vec3 calculateSpotLight(vec3 pos, vec3 normal)
{
    vec3 sumColor = vec3(0.);
    for (int i=0; i<numberOfLights; ++i)
    {
        SpotLight spot = Lgt.lights[i];
        vec3 lightDir = normalize(vec3(spot.pos.xyz - pos));
        vec3 eye = vec3(0.);

        float spec = 0.;

        if (dot(-spot.dir.xyz,lightDir) > spot.cutoff)
        {
            spot.intensity = max(dot(normal,lightDir), 0.);
            if (spot.intensity > 0.)
            {
                vec3 h = normalize(lightDir + eye);
                float intSpec = max(dot(h,normal), 0.);
                spec = pow(intSpec, shininess);
            }
        }
        sumColor += vec3(spot.intensity * spot.diffCol + spec * spot.diffCol);
    }
    return sumColor;
}

void main()
{
    vec3 normal = normalize(vVertCol);
    fragColor = vec4(calculateSpotLight(vWorldPos, normal), 1.);
}
]]


local objlist = {
    "cube",
    "tetrahedron",
    "octahedron",
    "sphere",
    "sphere2",
    "Biplane2",
    "NYERSEY_",
    "Submarine2",
    "hull22",
    --[[
    "bunny",
    "teapot",
    "ahstray",
    "capsule",
    ]]
}

local matlist = {
    {"062-Silver_512.data",512,512},
    {"matcap2_400.raw",400,400},
    {"matcap1_256.raw",256,256},
    {"053-Steel_600.data",600,600},
    {"029-Old_Bronze2_500.data",500,500},
    {"rusty_metal_512.data",512,512},
}

function objscene:switchObj(reverse)
    if reverse then
        self:switchToObj(self.obj_idx - 1)
    else
        self:switchToObj(self.obj_idx + 1)
    end
end

function objscene:switchToObj(idx)
    self.obj_idx = idx
    if self.obj_idx < 1 then self.obj_idx = #objlist end
    if self.obj_idx > #objlist then self.obj_idx = 1 end

    local objname = objlist[self.obj_idx]

    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    self:loadModel("models/"..objname..".obj")
    self:initModelAttributes()
end

function objscene:switchMaterial()
    self.mat_idx = self.mat_idx + 1
    if self.mat_idx > #matlist then self.mat_idx = 1 end
    local mat_params = matlist[self.mat_idx]

    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)
    self.texID = 0

    self:loadTextures("images/"..mat_params[1], mat_params[2], mat_params[3])
end

function objscene:setDataDirectory(dir)
    self.data_dir = dir
end

function objscene:loadModel(filename)
    self.obj = objfile.new()
    if self.data_dir then filename = self.data_dir .. "/" .. filename end
    print("Loading obj "..filename)
    self.obj:loadmodel(filename)
    local str = (#self.obj.vertlist/4).." vertices*3, "
    str = str..(#self.obj.normlist/4).." normals*3, "
    str = str..(#self.obj.texlist/4).." texcoords*3, "
    str = str..(#self.obj.idxlist/4).." ints*3 for triangle indices."
    print(str)

    -- Compute aabb of obj
    if true then
        local nv = #self.obj.vertlist/3
        local aabbmin = {}
        local LARGEVAL = 999999999
        self.obj.aabb = {
            min={LARGEVAL,LARGEVAL,LARGEVAL,},
            max={-LARGEVAL,-LARGEVAL,-LARGEVAL,},
        }
        for i=1,nv-1 do
            local vo = 3*i
            for j=1,3 do
                self.obj.aabb.min[j] = math.min(self.obj.aabb.min[j], self.obj.vertlist[vo+j-1])
                self.obj.aabb.max[j] = math.max(self.obj.aabb.max[j], self.obj.vertlist[vo+j-1])
            end
        end
        print("AABB min: ",
            self.obj.aabb.min[1],
            self.obj.aabb.min[2],
            self.obj.aabb.min[3]
            )
        print("AABB max: ",
            self.obj.aabb.max[1],
            self.obj.aabb.max[2],
            self.obj.aabb.max[3]
            )
        self.objscale = 0
        for j=1,3 do
            self.objscale = math.max(self.objscale, self.obj.aabb.max[j]-self.obj.aabb.min[j])
        end
        print("Obj scale: ",self.objscale)
    end
end

function objscene:loadTextures(texfilename, w, h)
    if self.data_dir then texfilename = self.data_dir .. "/" .. texfilename end
    local inp = assert(io.open(texfilename, "rb"))
    local data = inp:read("*all")
    assert(inp:close())
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    self.texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
    --gl.glTexParameteri( GL.GL_TEXTURE_2D, GL.GL_DEPTH_TEXTURE_MODE, GL.GL_INTENSITY ); --deprecated, out in 3.1
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB,
                  w, h, 0,
                  GL.GL_RGB, GL.GL_UNSIGNED_BYTE, data)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
end


function objscene:initModelAttributes()
    --print("Number of objects: "..#self.obj.olist)
    if #self.obj.olist == 0 then return end

    local v = self.obj.vertlist
    local n = self.obj.normlist
    local i = self.obj.idxlist

    local verts = glFloatv(#v,v)
    local norms = glFloatv(#n,n)

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    self.vbos.originalTris = vvbo

    gl.glEnableVertexAttribArray(vpos_loc)

    local vnorm_loc = gl.glGetAttribLocation(self.prog, "vNormal")
    if vnorm_loc > -1 then
        local nvbo = glIntv(0)
        gl.glGenBuffers(1, nvbo)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, nvbo[0])
        gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(norms), norms, GL.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(vnorm_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
        self.vbos.originalNorms = nvbo

        gl.glEnableVertexAttribArray(vnorm_loc)
    end

    local lines = glUintv(#i,i)
    local ivbo = glIntv(0)
    gl.glGenBuffers(1, ivbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, ivbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(lines), lines, GL.GL_STATIC_DRAW)
    self.vbos.triIndices = ivbo
end

function objscene:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog = sf.make_shader_from_source({
        --vsrc = matcap_vert, fsrc = matcap_frag,
        --vsrc = tricolor_vert, fsrc = tricolor_frag,

        -- Intel complains about mismatched versions for geom shaders(330),
        -- so we swap the 310 es for 330 keeping source intact and valid.
        vsrc = normcalc_vert, gsrc = normcalc_geom,
        fsrc = string.gsub(lighting_frag, "#version 310 es", "#version 330"),
        })

    self:switchObj()
    self:switchMaterial()

    local lv = {}
    local numLights = 3
    local numflts = numLights*(4*4) -- sizeof light struct with padding
    local ubo = glIntv(0)
    gl.glGenBuffers(1, ubo)
    gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, ubo[0])
    gl.glBufferData(GL.GL_UNIFORM_BUFFER, 4*numflts, nil, GL.GL_DYNAMIC_DRAW)
    self.uniformbuf = ubo

    local lightBlock = gl.glGetUniformBlockIndex(self.prog, "Light")
    gl.glUniformBlockBinding(self.prog, lightBlock, 0)
    gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])
    --gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)
    gl.glBindVertexArray(0)
end

function objscene:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    gl.glDeleteBuffers(1,self.uniformbuf)

    local texdel = ffi.new("GLuint[1]", self.texID)
    gl.glDeleteTextures(1,texdel)

    gl.glDeleteProgram(self.prog)

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function objscene:renderEye(model, view, proj)
    local um_loc = gl.glGetUniformLocation(self.prog, "modelMtx")
    local uv_loc = gl.glGetUniformLocation(self.prog, "viewMtx")
    local up_loc = gl.glGetUniformLocation(self.prog, "projMtx")
    gl.glUseProgram(self.prog)

    local m = {}
    for i=1,16 do m[i] = model[i] end
    if self.objscale then
        local sc = 2/self.objscale
        if self.obj_idx == 1 then sc = sc * .5 end -- Why does the cube appear so big?
        mm.glh_scale(m, sc, sc, sc)
    end
    vlowered = {}
    for i=1,16 do vlowered[i] = view[i] end
    mm.glh_translate(vlowered, 0,-4,0)
    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, vlowered))
    gl.glUniformMatrix4fv(up_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    -- Flatten array of light values
    local ldata = {}
    for _,v in pairs(self.lightData) do
        for i=1,#v do
            table.insert(ldata, v[i])
        end
    end
    local lightArr = glFloatv(#ldata, ldata)
    gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, self.uniformbuf[0])
    gl.glBufferSubData(GL.GL_UNIFORM_BUFFER, 0, ffi.sizeof(lightArr), lightArr)
    gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)

    local side = self.sqrt_instances
    local num = side*side
    local unum_loc = gl.glGetUniformLocation(self.prog, "numInstances")
    gl.glUniform1i(unum_loc, num)

    local ut_loc = gl.glGetUniformLocation(self.prog, "time")
    gl.glUniform1f(ut_loc, self.time)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    local stex_loc = gl.glGetUniformLocation(self.prog, "uMatCap")
    gl.glUniform1i(stex_loc, 0)

    gl.glBindVertexArray(self.vao)
    for k,v in pairs(self.obj.olist) do
        if v[1] > 0 then
            gl.glDrawElementsInstanced(GL.GL_TRIANGLES, #self.obj.idxlist, GL.GL_UNSIGNED_INT, nil, num)
        end
    end
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function objscene:render_for_one_eye(mview, proj)
    local model = {}
    mm.make_identity_matrix(model)
    self:renderEye(model, mview, proj)
end

function objscene:timestep(absTime, dt)
    self.time = absTime

    -- Animate lighting data
    local r = 12.5
    local speed = 2
    local time = self.time
    self.lightData = {
        -- Light 1
        {
            -15,15,0,0,
            0,-1,0,0,
            1,0,0,0,
            0,.75,0,0, -- NOTE: std140 layout adds padding to 16-byte boundaries
        },
        -- Light 2
        {
            r*math.sin(speed*time), 15., r*math.cos(speed*time),0,
            0,-1,0,0,
            0,1,0,0,
            0,
            .75 + .05*math.sin(8.3*time),
            0,0,
        },
        -- Light 3
        {
            15,15,0,0,
            0,-1,0,0,
            0,0,1,0,
            0,.75,0,0, -- NOTE: std140 layout adds padding to 16-byte boundaries
        },
    }
end

function objscene:keypressed(key, scancode, action, mods)
    if key == 32 then -- space
        self:switchObj(mods~=0)
    elseif key == 257 then -- enter
        self:switchMaterial()
    elseif key == 45 then -- -(minus)
        self.sqrt_instances = self.sqrt_instances - 1
    elseif key == 61 then -- +
        self.sqrt_instances = self.sqrt_instances + 1
    end
end

function objscene:handleCommand(args)
    local cmd = args[1]
    if cmd == 'obj' then
        local val = tonumber(args[2])
        self:switchToObj(val)
    end
end

-- Return a list of strings(lines) describing commands
function objscene:commandHelp()
    return {
        'objscene - Displays instanced obj files.',
        'Commands: obj <idx>',
    }
end
return objscene
