--[[ touch_shader2.lua

    Draws touch points *indirectly* with a fragment shader.

    There is an intermediate render-to-texture step, where touch points
    are drawn with blending onto an intermediate buffer. Color trails
    can accumulate over multiple frames, leaving trails.
]]
touch_shader2 = {}

--local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions2")
local fbf = require("util.fbofunctions")
local snd = require "util.soundfx"

touch_shader2 = {}
touch_shader2.__index = touch_shader2

function touch_shader2.new(...)
    local self = setmetatable({}, touch_shader2)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local glIntv   = ffi.typeof('GLint[?]')
local glUintv  = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

function touch_shader2:setDataDirectory(dir)
    self.dataDir = dir
    snd.setDataDirectory(dir)
end

function touch_shader2:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.prog_present = 0
    self.winw, winh = 0,0 -- Window size in pixels
    self.pointers = {} -- List of pointer states
    self.fbo = nil
    self.bright = 0
    self.pos = {.5,.5}
    self.vel = {0,0}
    self.buttons = {}
end

local basic_vert = [[
#version 100

attribute vec2 vPosition;
attribute vec2 vColor;

varying vec3 vfColor;

void main()
{
    vfColor = vec3(vColor, 0.);
    gl_Position = vec4(vPosition, 0., 1.);
}
]]

local basic_frag = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfColor;

#define MAX_TOUCH_POINTS 16
uniform vec2 uTouchPts[MAX_TOUCH_POINTS];
uniform int numPts;

void main()
{
    vec3 col = .5*vfColor + vec3(.5);

    float c = 0.;
    for (int i=0; i<numPts; ++i)
    {
        vec2 tp = uTouchPts[i];
        tp.y = 1. - tp.y;
        float d = length(col.xy - tp);
        float soft = 1.-smoothstep(0., .15, d);
        c = max(c, pow(soft,15.));
    }

    gl_FragColor = vec4(col, c);
}
]]


local present_texture_frag = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D sTex;

varying vec3 vfColor;

void main()
{
    vec2 tc = .5*vfColor.xy + vec2(.5);
    vec4 col = texture2D(sTex, tc);
    gl_FragColor = vec4(col.xyz, 1.);
}
]]

function touch_shader2:resizeViewport(w,h)
    self.winw, self.winh = w,h
    self.fbo = fbf.allocate_fbo(self.winw, self.winh)
end

function touch_shader2:init_cube_attributes()
    local verts = glFloatv(4*2, {
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        })

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    local quads = glUintv(6*2, {
        0,1,2,
        0,2,3,
    })
    local qvbo = glIntv(0)
    gl.glGenBuffers(1, qvbo)
    gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, qvbo)
end

function touch_shader2:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog, err = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })
    if err then print(err) end

    self.prog_present, err = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = present_texture_frag,
        })
    if err then print(err) end

    self:init_cube_attributes()
    gl.glBindVertexArray(0)
end

function touch_shader2:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}

    gl.glDeleteProgram(self.prog)
    gl.glDeleteProgram(self.prog_present)

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    fbf.deallocate_fbo(self.fbo)
end


function touch_shader2:render_points()
    local utp_loc = gl.glGetUniformLocation(self.prog, "uTouchPts")
    local unp_loc = gl.glGetUniformLocation(self.prog, "numPts")
    gl.glUseProgram(self.prog)
    
    -- The array may be sparse, so we can't just use #v
    local i = 0
    for k,v in pairs(self.pointers) do
        i = i+1
    end
    -- Assemble array of touch points to pass in as uniform
    pts = {}
    for k,v in pairs(self.pointers) do
        if v and v.x and v.y then
            local x,y = v.x, v.y
            table.insert(pts, x)
            table.insert(pts, y)
        end
    end
    
    local num = i
    gl.glUniform2fv(utp_loc, num, glFloatv(2*num, pts))
    gl.glUniform1i(unp_loc, num)

    gl.glEnable(GL.GL_BLEND)
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
    gl.glBindVertexArray(self.vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 6, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
    gl.glDisable(GL.GL_BLEND)
end

function touch_shader2:clear_buffer()
    -- Set the bound FBO back when done
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)
    fbf.bind_fbo(self.fbo)
    -- Set the viewport back when we're done
    local vpdims = ffi.new("int[4]")
    gl.glGetIntegerv(GL.GL_VIEWPORT, vpdims)
    gl.glViewport(0,0, self.winw, self.winh)

    gl.glClearColor(0,0,0,0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    -- Set the viewport back when we're done
    gl.glViewport(vpdims[0], vpdims[1], vpdims[2], vpdims[3])
    -- Set the FBO back when done
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
end
function touch_shader2:render_to_buffer()
    -- Set the bound FBO back when done
    local boundfbo = ffi.new("int[1]")
    gl.glGetIntegerv(GL.GL_FRAMEBUFFER_BINDING, boundfbo)
    fbf.bind_fbo(self.fbo)

    -- Set the viewport back when we're done
    local vpdims = ffi.new("int[4]")
    gl.glGetIntegerv(GL.GL_VIEWPORT, vpdims)
    gl.glViewport(0,0, self.winw, self.winh)

    self:render_points()

    -- Set the viewport back when we're done
    gl.glViewport(vpdims[0], vpdims[1], vpdims[2], vpdims[3])

    -- Set the FBO back when done
    gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, boundfbo[0])
end



function touch_shader2:render_for_one_eye(view, proj)

    -- Do this here or in touch?
    -- Both should be on the right thread.
    self:render_to_buffer()

    gl.glUseProgram(self.prog_present)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.fbo.tex)
    local ut_loc = gl.glGetUniformLocation(self.prog, "sTex")
    gl.glUniform1i(ut_loc, 0)
    
    gl.glBindVertexArray(self.vao)
    gl.glDrawElements(GL.GL_TRIANGLES, 6, GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function touch_shader2:timestep(absTime, dt)
    for i=1,2 do
        self.pos[i] = self.pos[i] + self.vel[i]

        self.pos[i] = math.max(self.pos[i], -1)
        self.pos[i] = math.min(self.pos[i], 1)
        self.pointers[2] = {x=self.pos[1],y=1-self.pos[2]}
    end
end

function touch_shader2:keypressed(key, scancode, action, mods)
    -- 262-265 rldu
    local s = 0.005
    if key == 262 then self.vel[1] = s end
    if key == 263 then self.vel[1] = -s end
    if key == 264 then self.vel[2] = -s end
    if key == 265 then self.vel[2] = s end
end

function touch_shader2:keyreleased(key, scancode, action, mods)
    if key == 262 then self.vel[1] = 0 end
    if key == 263 then self.vel[1] = 0 end
    if key == 264 then self.vel[2] = 0 end
    if key == 265 then self.vel[2] = 0 end
end

function touch_shader2:onSingleTouch(pointerid, action, x, y)
    self.pointers[pointerid] = {x=x/winw, y=y/winh}

    if action == 1 or action == 6 then
        self.pointers[pointerid] = nil
    end
end

function touch_shader2:onmouse(x, y)
    self.pointers[1] = {x=x,y=y}
end

function touch_shader2:setBrightness(b)
    self.bright = b
end

-- Called on init before initGL and every time the screen rotates
function touch_shader2:setWindowSize(w,h)
    fbf.deallocate_fbo(self.fbo)

    self.winw, self.winh = w,h
    self.fbo = fbf.allocate_fbo(self.winw, self.winh)
    clear_buffer()
end

function touch_shader2:updateJoystick(axes, buttons)
    if not axes then return end

    for k,v in pairs(buttons) do
        if self.buttons[k] ~= v then
            print(k,' changed.',v)
            snd.playSound("pop_drip.wav")
            if k == 12 then -- Start button on a certain controller...
                self:clear_buffer()
            end
        end
        self.buttons[k] = v
    end

    for i=1,2 do
        self.vel[i] = .025 * (axes[i] / 0xffff)
        if i==2 then self.vel[i] = -self.vel[i] end
    end
end

return touch_shader2
