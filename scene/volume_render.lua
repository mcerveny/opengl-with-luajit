--[[ volume_render.lua

]]
volume_render = {}

volume_render.__index = volume_render

function volume_render.new(...)
    local self = setmetatable({}, volume_render)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function volume_render:init()
    self.shader = nil
end

local ffi = require("ffi")
require("util.fullscreen_shader")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local rm_frag = [[
uniform mat4 mvmtx;
uniform mat4 prmtx;

// math
const float PI = 3.14159265359;
const float DEG_TO_RAD = PI / 180.0;

// Returns both t parameters for intersection points
vec2 rayIntersectsSphere( in vec3 ro, in vec3 rd, in vec3 center, float radius )
{
    vec3 oc = ro - center;
    float b = 2.0 * dot( rd, oc );
    float c = dot( oc, oc ) - radius * radius;
    float disc = b * b - 4.0 * c;

    if ( disc < 0.0 )
    {
        return vec2( -9999.0 ); // no hit
    }

    float q;
    if ( b < 0.0 )
        q = ( -b - sqrt(disc) ) / 2.0;
    else
        q = ( -b + sqrt(disc) ) / 2.0;

    float t0 = q;
    float t1 = c / q;

    if ( t0 < t1 )
    {
        // Ensure t0 < t1
        float temp = t0;
        t0 = t1;
        t1 = temp;
    }
    return vec2( t0, t1 );
}

vec3 getSceneColor( in vec3 ro, in vec3 rd, inout float depth )
{
    vec3 center = vec3(0.);
    vec2 hits = rayIntersectsSphere( ro, rd, center, 1.0 );
    if ( hits.x < -9998.0 )
        discard;

    if ( (hits.x < 0.0) && (hits.y < 0.0) )
        discard;

    // Clip off the portion of the volume raymarch path behind the viewpoint.
    // I thought this would have been x to clip, but that's just one more thing that's backwards...
    hits.y = max( 0.0, hits.y );

    vec3 startPt = ro + hits.x * rd;
    vec3 endPt = ro + hits.y * rd;

    // must write to depth for fragment to appear
    depth = 1.;

    ///@todo Raymarch volume

    return (endPt - startPt);
}

///////////////////////////////////////////////////////////////////////////////
// Patch in the Rift's heading to raymarch shader writing out color and depth.
// http://blog.hvidtfeldts.net/

// Translate the origin to the camera's location in world space.
vec3 getEyePoint(mat4 mvmtx)
{
    vec3 ro = -mvmtx[3].xyz;
    return ro;
}

// Construct the usual eye ray frustum oriented down the negative z axis.
// http://antongerdelan.net/opengl/raycasting.html
vec3 getRayDirection(vec2 uv)
{
    vec4 ray_clip = vec4(uv.x, uv.y, -1., 1.);
    vec4 ray_eye = inverse(prmtx) * ray_clip;
    return normalize(vec3(ray_eye.x, ray_eye.y, -1.));
}

void main()
{
    vec2 uv11 = uv * 2.0 - vec2(1.0);
    vec3 ro = getEyePoint(mvmtx);
    vec3 rd = getRayDirection(uv11);

    ro *= mat3(mvmtx);
    rd *= mat3(mvmtx);

    float depth = 9999.0;
    vec3 col = getSceneColor(ro, rd, depth);

    // Write to depth buffer
    vec3 eyeFwd = vec3(0.,0.,-1.) * mat3(mvmtx);
    float eyeHitZ = -depth * dot(rd, eyeFwd);
    float p10 = prmtx[2].z;
    float p11 = prmtx[3].z;
    // A little bit of algebra...
    float ndcDepth = -p10 + -p11 / eyeHitZ;
    float dep = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;

    gl_FragDepth = dep;
    fragColor = vec4(col, 1.0);
}
]]

function volume_render:initGL()
    self.shader = FullscreenShader.new(rm_frag)
    self.shader:initGL()
end

function volume_render:exitGL()
    self.shader:exitGL()
end

function volume_render:render_for_one_eye(view, proj)
    local function set_variables(prog)
        local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
        gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
        local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
        gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))
    end

    self.shader:render(view, proj, set_variables)
end

return volume_render
