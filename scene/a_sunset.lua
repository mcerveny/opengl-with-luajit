--[[ a_sunset.lua

]]
a_sunset = {}

a_sunset.__index = a_sunset

function a_sunset.new(...)
    local self = setmetatable({}, a_sunset)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function a_sunset:init()
    -- Object-internal state: hold a list of VBOs for deletion on exitGL
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.time = 0
end

--local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")
local mm = require("util.matrixmath")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local sun_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    vec2 center = vec2(0.5);
    vec2 tc = vfColor.xy;
    if (length(tc-center) > 0.5)
        discard;

    vec3 sunColor = vec3(250./255., 235./255., 141./255.);
    fragColor = vec4(sunColor, 1.0);
}
]]

local wave_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform float time;

in vec3 vfColor;
out vec4 fragColor;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 438.5453);
}

void main()
{
    vec3 sunColor = vec3(250./255., 235./255., 141./255.);
    vec3 waveColor = vec3(170./255., 56./255., 0./255.);

    vec2 tc = vfColor.xy;
    tc.x += 0.0001*time;
    tc = fract(tc);
    vec3 noise = vec3(rand(tc));
    vec3 highlightCol = mix(waveColor, sunColor, noise);
    fragColor = vec4(
        mix(waveColor, highlightCol, smoothstep(0.9, 1.0, tc.y)),
        1.0);
}
]]

function a_sunset:init_buffers()
    local verts = glFloatv(4*3, {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
        })

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    table.insert(self.vbos, cvbo)
end

function a_sunset:init_attributes(prog)
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[1][0])
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)

    local vcol_loc = gl.glGetAttribLocation(prog, "vColor")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbos[2][0])
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function a_sunset:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.prog_sun = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = sun_frag,
        })
    self.prog_wave = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = wave_frag,
        })

    self:init_buffers()
    self:init_attributes(self.prog_sun)
    self:init_attributes(self.prog_wave)
    gl.glBindVertexArray(0)
end

function a_sunset:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    gl.glDeleteProgram(self.prog_sun)
    gl.glDeleteProgram(self.prog_wave)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function a_sunset:draw_wave_quad(v, p, time)
    gl.glUseProgram(self.prog_wave)
    local ut_loc = gl.glGetUniformLocation(self.prog_wave, "time")
    gl.glUniform1f(ut_loc, time)

    local umv_loc = gl.glGetUniformLocation(self.prog_wave, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog_wave, "prmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, v))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, p))
    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function a_sunset:render_for_one_eye(view, proj)
    gl.glClearColor(238/255, 89/255, 0, 0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    -- Sun quad
    local p = {}
    mm.make_identity_matrix(p)
    local v = {}
    mm.make_identity_matrix(v)
    local s = 1.5
    mm.glh_scale(v,s,s,s)
    mm.glh_translate(v, -.5,-.5,0)

    gl.glUseProgram(self.prog_sun)
    local umv_loc = gl.glGetUniformLocation(self.prog_sun, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog_sun, "prmtx")
    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, v))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, p))
    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)


    -- Wave quad
    local p = {}
    mm.make_identity_matrix(p)
    local v = {}
    mm.make_identity_matrix(v)
    mm.glh_translate(v, 0,0,-.1)
    local s = 2
    mm.glh_scale(v,s,s,s)
    mm.glh_translate(v, -.5,-1,0)
    self:draw_wave_quad(v, p, 0)

    mm.glh_translate(v, 0,-.07, -.1)
    self:draw_wave_quad(v, p, self.time)
end

function a_sunset:timestep(absTime, dt)
    self.time = absTime
end

return a_sunset
