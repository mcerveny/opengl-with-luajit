--[[ gridcube.lua

    Create subdivided cube geometry using compute shaders.

    Generates the six faces of a cube, each subdivided into subdivs*subdivs
    squares(2 triangles each). Initialization is very fast as no CPU loops
    are used to create geometry, only compute shaders which assign positions
    and connectivity by index in the list.

    Also includes display shader with basic diffuse and specular lighting
    and a gray material.
]]
gridcube = {}

gridcube.__index = gridcube

function gridcube.new(...)
    local self = setmetatable({}, gridcube)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function gridcube:init(subdivs, numlights)
    self.cubemesh = nil
    self.mesh_subdivs = subdivs or 64
    self.vao = 0
    self.progs = {}
    self.time = 0
    self.lightData = {}
    self.numLights = numlights or 2
    self.texID = 0
end

require("util.cubemesh")
--local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv   = ffi.typeof('GLint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

--[[
    Display the mesh with diffuse and specular lighting.
]]
local gridcube_spotlight_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vNormal;
in vec2 vTexCoord;

out vec3 vfWorldPos;
out vec3 vfNormal;
out vec2 vfTexCoord;

uniform mat4 modelmtx;
uniform mat4 viewmtx;
uniform mat4 projmtx;

void main()
{
    mat4 mvmtx = viewmtx * modelmtx;
    vfWorldPos = (modelmtx * vPosition).xyz;
    vfNormal = normalize(mat3(modelmtx) * vNormal.xyz);
    vfTexCoord = vTexCoord;
    gl_Position = projmtx *  mvmtx * vPosition;
}
]]

local gridcube_spotlight_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfWorldPos;
in vec3 vfNormal;
in vec2 vfTexCoord;
out vec4 fragColor;

float shininess = 125.;

struct SpotLight {
    vec4 pos;
    vec4 dir;
    vec4 diffCol;
    float intensity;
    float cutoff;
};

const int numberOfLights = NUMLIGHTS;
layout (std140) uniform Light
{
    SpotLight lights[numberOfLights];
} Lgt;

uniform sampler2D sTex;

vec3 calculateSpotLight(vec3 pos, vec3 normal)
{
    vec4 texCol = texture(sTex, vfTexCoord.xy);
    // Little hack to bring up blacks
    texCol = max(texCol, vec4(.2f));

    vec3 sumColor = vec3(0.);
    for (int i=0; i<numberOfLights; ++i)
    {
        SpotLight spot = Lgt.lights[i];
        vec3 lightDir = normalize(vec3(spot.pos.xyz - pos));
        vec3 eye = vec3(0.);

        float spec = 0.;

        if (dot(-spot.dir.xyz,lightDir) > spot.cutoff)
        {
            spot.intensity = max(dot(normal,lightDir), 0.);
            if (spot.intensity > 0.)
            {
                vec3 h = normalize(lightDir + eye);
                float intSpec = max(dot(h,normal), 0.);
                spec = pow(intSpec, shininess);
            }
        }
        sumColor += vec3(
            spot.intensity * spot.diffCol * texCol
            + spec * spot.diffCol);
    }
    return sumColor;
}

void main()
{
    //fragColor = vec4(normalize(abs(vfNormal)), 1.0);
    fragColor = vec4(calculateSpotLight(vfWorldPos, vfNormal), 1.);
}
]]

-- Init display shader attributes
function gridcube:init_gridmesh_display_attribs(prog)
    local vpos_loc = gl.glGetAttribLocation(prog, "vPosition")
    local vnorm_loc = gl.glGetAttribLocation(prog, "vNormal")
    local vtex_loc = gl.glGetAttribLocation(prog, "vTexCoord")
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.cubemesh.vbos.vertices[0])
    gl.glVertexAttribPointer(vpos_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.cubemesh.vbos.normals[0])
    gl.glVertexAttribPointer(vnorm_loc, 4, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, self.cubemesh.vbos.texs[0])
    gl.glVertexAttribPointer(vtex_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vnorm_loc)
    gl.glEnableVertexAttribArray(vtex_loc)
end


-- GL_INVALID_VALUE is generated if any of num_groups_x, num_groups_y,
-- or num_groups_z is greater than or equal to the maximum work-group
-- count for the corresponding dimension.
local function check_compute_group_size()
    print("GL_MAX_COMPUTE_WORK_GROUP_COUNT:")
    local int_buffer = ffi.new("GLint[1]")
    for i=0,2 do
        gl.glGetIntegeri_v(GL.GL_MAX_COMPUTE_WORK_GROUP_COUNT, i, int_buffer)
        print(' ',i,int_buffer[0])
    end
end

function gridcube:initGL()
    check_compute_group_size()

    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    self.cubemesh = CubeMesh.new(self.mesh_subdivs)
    self.cubemesh:initGL()

    -- TODO: make this step optional? A parameter?
    self.progs.meshvsfs = sf.make_shader_from_source({
        vsrc = gridcube_spotlight_vert,
        fsrc = string.gsub(gridcube_spotlight_frag, "NUMLIGHTS", self.numLights),
        })
    self:init_gridmesh_display_attribs(self.progs.meshvsfs)

    local lv = {}
    local numflts = self.numLights*(4*4) -- sizeof light struct with padding
    local ubo = glIntv(0)
    gl.glGenBuffers(1, ubo)
    gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, ubo[0])
    gl.glBufferData(GL.GL_UNIFORM_BUFFER, 4*numflts, nil, GL.GL_DYNAMIC_DRAW)
    self.uniformbuf = ubo

    local lightBlock = gl.glGetUniformBlockIndex(self.progs.meshvsfs, "Light")
    print("lb:",lightBlock)
    gl.glUniformBlockBinding(self.progs.meshvsfs, lightBlock, 0)
    gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])
    --gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)

    gl.glBindVertexArray(0)
end

function gridcube:exitGL()
    gl.glBindVertexArray(self.vao)

    for _,p in pairs(self.progs) do
        gl.glDeleteProgram(p)
    end
    progs = {}

    self.cubemesh:exitGL()

    gl.glDeleteBuffers(1,self.uniformbuf)

    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
end

function gridcube:renderEye(model, view, proj)
    gl.glDisable(GL.GL_CULL_FACE)

    local prog = self.progs.meshvsfs
    local um_loc = gl.glGetUniformLocation(prog, "modelmtx")
    local uv_loc = gl.glGetUniformLocation(prog, "viewmtx")
    local up_loc = gl.glGetUniformLocation(prog, "projmtx")
    gl.glUseProgram(prog)
    --gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(up_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    -- Flatten array of light values
    local ldata = {}
    for _,v in pairs(self.lightData) do
        for i=1,#v do
            table.insert(ldata, v[i])
        end
    end
    local lightArr = glFloatv(#ldata, ldata)
    gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, self.uniformbuf[0])
    gl.glBufferSubData(GL.GL_UNIFORM_BUFFER, 0, ffi.sizeof(lightArr), lightArr)
    gl.glBindBuffer(GL.GL_UNIFORM_BUFFER, 0)

    local lightBlock = gl.glGetUniformBlockIndex(self.progs.meshvsfs, "Light")
    gl.glUniformBlockBinding(self.progs.meshvsfs, lightBlock, 0)
    gl.glBindBufferBase(GL.GL_UNIFORM_BUFFER, 0, self.uniformbuf[0])

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
    local stex_loc = gl.glGetUniformLocation(prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    local m = {}
    mm.make_identity_matrix(m)
    for i=1,16 do m[i] = model[i] end
    local s = .5
    --mm.glh_scale(m,s,s,s)
    --mm.glh_rotate(m, 10*self.time , .1,.7,.3)
    mm.glh_translate(m, -.5, -.5, -.5)
    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, m))
    gl.glBindVertexArray(self.vao)
    --gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_LINE)
    gl.glDrawElements(GL.GL_TRIANGLES,
        self.cubemesh.num_tri_idxs,
        GL.GL_UNSIGNED_INT, nil)
    gl.glBindVertexArray(0)

    gl.glUseProgram(0)
end

function gridcube:timestep(absTime, dt)
    self.time = absTime

    --[[ Animate lighting data
    local r = 12.5
    local speed = 2
    local time = self.time
    local x,y,z = r*math.sin(speed*time),20,r*math.cos(speed*time)
    local dir = {-x,-y,-z}
    mm.normalize(dir)
    --print(dir[1], dir[2], dir[3])
    self.lightData = {
        -- Light 1
        {
            x,y,z,0,
            dir[1], dir[2], dir[3],0,
            1,1,0,0,
            0,.9,0,0, -- NOTE: std140 layout adds padding to 16-byte boundaries
        },
    }]]
end

function gridcube:keypressed(ch)
    if ch == 'f' then return end
    if ch == 'l' then return end

    if ch == 'n' then
        self.cubemesh:recalculate_normals()
    end
end

function gridcube:get_cubemesh() return self.cubemesh end
function gridcube:get_vertices_vbo() return self.cubemesh.vbos.vertices end
function gridcube:get_num_verts() return self.cubemesh.num_verts end
function gridcube:recalc_normals() self.cubemesh:recalculate_normals() end

return gridcube
