-- shader_editor.lua
-- Holds a subject scene and an interface to edit its source.
-- Edits to the shader text are passed along to the included scene so
-- effects can be seen immediately on edit.
-- TODO: Maybe push editor to another module?

local ffi = require "ffi"
local mm = require "util.matrixmath"
local sf2 = require "util.shaderfunctions2"
local EditorLibrary = require "scene.stringedit_scene"
require "util.glfont"

shader_editor = {}
shader_editor.__index = shader_editor

function shader_editor.new(...)
    local self = setmetatable({}, shader_editor)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function shader_editor:init()
    self.vao = 0
    self.vbos = {}
    self.time = 0

    self.glfont = nil
    self.Editor = nil -- gets created and deleted every toggle
    self.win_w = 400
    self.win_h = 300
    self.update_every_key = true
    self.src_filename = ""
    self.sceneName = 'gs'
    self.sceneNames = {
        "vs",
        "boids",
        "gs",
        "life",
        "fs",
    }
    self.SceneLibraries = {}
    for _,v in pairs(self.sceneNames) do
        local mod = 'scene.'..v
        self.SceneLibraries[v] = require(mod)
    end
    self.points_scene = self.SceneLibraries[self.sceneName].new()
    self.src_string = self.points_scene.src or ""
end

function shader_editor:setDataDirectory(dir)
    self.data_dir = dir
    if self.points_scene.setDataDirectory then
        self.points_scene:setDataDirectory(dir)
    end
end

-- Recompile the current shader, updating error messages.
function shader_editor:buildShader()
    if self.Editor then
        self.Editor.error_msgs = {}
    end

    local err = self.points_scene:buildShader(self.src_string)
    if err then
        self:pushGlslErrorMessages(err)
    end
end

function shader_editor:initGL()
    self.points_scene:initGL()

    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]

    local dir = "fonts"
    if self.data_dir then dir = self.data_dir .. "/" .. dir end
    self.glfont = GLFont.new('courier_512.fnt', 'courier_512_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function shader_editor:exitGL()
    self.points_scene:exitGL()
    self.glfont:exitGL()
    gl.glBindVertexArray(self.vao)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function shader_editor:renderEye(model, view, proj)
    if self.points_scene.renderEye then
        self.points_scene:renderEye(model, view, proj)
    else
        self.points_scene:render_for_one_eye(view, proj)
    end

    if self.Editor then
        self:renderEditor(view, proj)
    end
end

function shader_editor:render_for_one_eye(view, proj)
    self.points_scene:render_for_one_eye(view, proj)

    if self.Editor then
        self:renderEditor(view, proj)
    end
end

function shader_editor:timestep(absTime, dt)
    self.time = absTime
    self.points_scene:timestep(absTime, dt)
end

function shader_editor:resizeViewport(w,h)
    self.win_w, self.win_h = w, h
end

--
-- Editor Concerns below
--

function shader_editor:onmouse(x,y)
    if self.Editor then
        -- TODO move into editor
        local rows_per_window = self.Editor.visible_lines
        local cols_per_window = 94 * (rows_per_window/26)
        local col = -1+math.floor(x * cols_per_window)
        local row = 1+math.floor(y * rows_per_window)
        col = col - 4 -- line nums
        col = math.max(0,col)
        row = row + self.Editor.scroll
        self.Editor.editbuf:setCursorPosition(col, row)
    end
end

-- http://lua-users.org/wiki/SplitJoin
function split_into_lines(str)
    local t = {}
    local function helper(line) table.insert(t, line) return "" end
    helper((str:gsub("(.-)\r?\n", helper)))
    return t
end

-- Catch, parse, and display this list of error messages
-- overlaid on the editor's code under their respective lines.
function shader_editor:pushGlslErrorMessages(err)
    -- Sample errors:
    -- Intel: [ERROR: 0:4: 'vec2' : syntax error syntax error]
    if not self.Editor then return end
    local lines = split_into_lines(err)
    if #lines == 0 then return end

    for _,v in pairs(lines) do
        if #v > 1 then
            -- Strip off digit, non-digit, digit, non-digit
            local linestr = string.match(v, "%d+[^%d]?%d+[^%d]?")
            if linestr then
                -- Get last digit sequence in that string
                for match in string.gmatch(linestr, "%d+") do
                    linestr = match
                end

                local linenum = tonumber(linestr)
                -- TODO concatenate or stack lines
                -- TODO wrap error lines in display
                self.Editor.error_msgs[tonumber(linenum)] = v
            end
        end
    end
    -- Lua out of memory errors?
    collectgarbage()
end

function shader_editor:renderEditor(view, proj)
    local id = {}
    mm.make_identity_matrix(id)
    local mv = {}
    local aspect = self.win_w / self.win_h
    mm.make_scale_matrix(mv,1/aspect,1,1)
    local s = .9
    mm.glh_scale(mv,s,s,s)

    gl.glDisable(GL.GL_DEPTH_TEST)
    self.Editor:render_for_one_eye(mv,id)
    gl.glEnable(GL.GL_DEPTH_TEST)
end

-- Parse the cmd line string into args and handle any that have handlers here.
function shader_editor:handleCommand(args)
    --for k,v in pairs(args) do print(k,v) end
    if not args then return end
    if #args == 0 then return end

    -- The editor/shell environment gets first crack at cmd args.
    if args[1]:lower() == 'load' and args[2] then
        local fn = self.sceneName..'/'..args[2]
        self.src_filename = fn

        self.Editor = EditorLibrary.new({
            filename = "shaders/"..fn,
            data_dir = self.data_dir
            })

        if self.Editor then
            self.Editor:initGL()
            self:reloadShader()
        end
        return {"loaded ",fn, self.data_dir}
    elseif args[1]:lower() == 'save' then
        if args[2] then self.src_filename = args[2] end
        local fn = self.data_dir..'/shaders/'..self.src_filename
        if self.Editor then
            self.Editor.editbuf:saveToFile(fn)
        end
        return {"saved ",fn, self.data_dir}
    elseif args[1]:lower() == 'new' then
        if self.points_scene.getNewShader then
            local src = self.points_scene:getNewShader()
            self.src_string = src
            if self.Editor then
                self.Editor.editbuf:loadFromString(src)
            end
            self:reloadShader()
            self.src_filename = 'new'
            return {'new shader'}
        end
    else
        local scn = args[1]:lower()
        local mod = self.SceneLibraries[scn]
        if mod then
            self.sceneName = scn
            local restore_editor = self.Editor ~= nil
            if self.Editor then
                self:toggleEditor()
                -- TODO: keep editor and repopulate with new source
            end
            self.points_scene = mod.new()
            self.points_scene:initGL()
            self.src_string = self.points_scene.src or ""
            if restore_editor then self:toggleEditor() end
        else
            -- Not built in command, try scene's command handlers
            if self.points_scene and self.points_scene.handleCommand then
                return self.points_scene:handleCommand(args)
            end
        end
    end
end

-- Return a list of strings(lines) describing commands
function shader_editor:commandHelp()
    local lines = {
        'shader_editor Allows live editing of multiple shader types.',
        'Commands: load save new',
        '          vs gs fs boids life',
    }
    for _,v in pairs(self.points_scene:commandHelp()) do
        table.insert(lines, v)
    end
    return lines
end

function shader_editor:keypressed(key, scancode, action, mods)
    if self.points_scene.keypressed then
        self.points_scene:keypressed(key, scancode, action, mods)
    end

    if key == 96 then
        self:toggleEditor()
        return true
    end

    if self.Editor then
        if mods ~= 0 then
            if action == 1 then -- press
                if key == 257 then -- Enter
                    self:reloadShader()
                    return true
                end
            end
        end

        -- On save, reload the scene
        if mods == 2 then -- ctrl held
            if action == 1 then -- press
                local fn = self.data_dir..'/'.."shaders/"..self.src_filename
                if key == string.byte('S') then
                    print("SAVE: ", self.src_filename, self.data_dir)
                    self.Editor.editbuf:saveToFile(fn)
                    return true
                elseif key == string.byte('L') then
                    print("LOAD: ", self.src_filename, self.data_dir)
                    self.Editor.editbuf:loadFromFile(fn)
                    return true
                end
            end
        end

        self.Editor:keypressed(key, scancode, action, mods)
        -- TODO: do we need this twice with glfw's charkeypressed?
        if self.update_every_key then
            self:reloadShader()
        end

        return true
    end
end

function shader_editor:charkeypressed(ch)
    if string.byte(ch) == 96 then
        self:toggleEditor()
        return true
    end

    if self.Editor then
        self.Editor:charkeypressed(ch)
        -- TODO: do we need this twice with glfw's keypressed?
        if self.update_every_key then
            self:reloadShader()
        end
    else
        if self.points_scene.charkeypressed then
            self.points_scene:charkeypressed(ch)
        end
    end

    return true
end

-- TODO: pass along mouse events

function shader_editor:onwheel(x,y)
    if self.Editor then
        self.Editor:onwheel(x,y)
        return true
    end
end

function shader_editor:toggleEditor()
    if self.Editor == nil then
        self.Editor = EditorLibrary.new({
            contents = self.src_string
            })
        if self.Editor then
            if self.Editor.setDataDirectory then self.Editor:setDataDirectory(self.data_dir) end
            self.Editor:initGL()
        end    
    else
        self.Editor:exitGL()
        self.Editor = nil
    end
end

function shader_editor:reloadShader()
    if not self.Editor then return end

    self.src_string = self.Editor.editbuf:saveToString()
    self:buildShader()
    -- TODO: keep an undo stack of working shader,string tuples

    if self.points_scene.clearInstanceBuffers then
        self.points_scene:clearInstanceBuffers()
    end
end

function shader_editor:saveShader()
    if not self.Editor then return end
    self.Editor.editbuf:saveToFile(self.src_filename)
end

return shader_editor
